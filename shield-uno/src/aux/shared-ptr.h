////////////////////////////////////////////////////////////////////////////////////////////
//
// shared-ptr.h
//
// Copyright © 2018 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "debug-assert.h"

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

class SharedPtrBase
{
   struct Wrapped
   {
      uint8_t m_refcnt = 1 ;

      Wrapped (void) {}
      virtual ~Wrapped (void) {}

      Wrapped (const Wrapped&) = delete ;
      Wrapped& operator= (const Wrapped&) = delete ;
   } ;

protected:
   SharedPtrBase (Wrapped *wrapped) : m_value{ wrapped } {}

   Wrapped* wrapped (void) const { debug_assert( m_value, PSTR("SHAREDPTRVAL") ) ; return m_value ; }

public:
   SharedPtrBase (void) : m_value{ nullptr } {}
   ~SharedPtrBase (void) { _unreference (m_value) ; }

protected:
   SharedPtrBase (const SharedPtrBase& org) :
      m_value{ org.m_value }
   {
      _reference (m_value) ;
   }

   SharedPtrBase (SharedPtrBase&& rhs) :
      m_value{ rhs.m_value }
   {
      rhs.m_value = nullptr ;
   }

   SharedPtrBase& operator= (const SharedPtrBase& rhs)
   {
      _reference (rhs.m_value) ;
      _unreference (m_value) ;
      m_value = rhs.m_value ;
      return *this ;
   }

   SharedPtrBase& operator= (SharedPtrBase&& rhs)
   {
      _unreference (m_value) ;

      m_value = rhs.m_value ;
      rhs.m_value = nullptr ;

      return *this ;
   }

public:
   void reset (void)
   {
      _unreference (m_value) ;
      m_value = nullptr ;
   }

   explicit operator bool (void) const { return m_value != nullptr ; }

   friend inline bool operator== (const SharedPtrBase& lhs, const SharedPtrBase& rhs) { return lhs.m_value == rhs.m_value ; }
   friend inline bool operator!= (const SharedPtrBase& lhs, const SharedPtrBase& rhs) { return lhs.m_value != rhs.m_value ; }
   friend inline bool operator< (const SharedPtrBase& lhs, const SharedPtrBase& rhs) { return lhs.m_value < rhs.m_value ; }
   friend inline bool operator> (const SharedPtrBase& lhs, const SharedPtrBase& rhs) { return lhs.m_value > rhs.m_value ; }
   friend inline bool operator<= (const SharedPtrBase& lhs, const SharedPtrBase& rhs) { return lhs.m_value <= rhs.m_value ; }
   friend inline bool operator>= (const SharedPtrBase& lhs, const SharedPtrBase& rhs) { return lhs.m_value >= rhs.m_value ; }

private:
   void _reference (Wrapped *value)
   {
      if (value)
         ++value->m_refcnt ;
   }

   void _unreference (Wrapped *value)
   {
      if (value && !--value->m_refcnt)
         delete value ;
   }

private:
   Wrapped *m_value ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

template<class _Tp>
   class shared_ptr : public SharedPtrBase
   {
      typedef SharedPtrBase base_class ;

   public:
      typedef _Tp value_type ;
      typedef _Tp& reference ;
      typedef _Tp* pointer ;

   private:
      struct MyWrapped : Wrapped
      {
         value_type m_value ;

         template<typename ... _Args>
            MyWrapped (_Args ... args) :
               m_value{ args... }
            {
            }
      } ;

      shared_ptr (MyWrapped *wrapped) : base_class{ wrapped } {}

   public:
      shared_ptr (void) {}

      template<typename _Org>
         shared_ptr (const shared_ptr<_Org>& rhs) :
            base_class{ rhs }
         {
            static_cast<value_type*> ((_Org*)nullptr) ;
         }

      template<typename _Org>
         shared_ptr (shared_ptr<_Org>&& rhs) :
            base_class{ aux::forward (rhs) }
         {
            static_cast<value_type*> ((_Org*)nullptr) ;
         }

      template<typename _Org>
         shared_ptr& operator= (const shared_ptr<_Org>& rhs)
         {
            static_cast<value_type*> ((_Org*)nullptr) ;
            base_class::operator= (rhs) ;
            return *this ;
         }

      template<typename _Org>
         shared_ptr& operator= (shared_ptr<_Org>&& rhs)
         {
            static_cast<value_type*> ((_Org*)nullptr) ;
            base_class::operator= (aux::forward (rhs)) ;
            return *this ;
         }

      pointer operator-> (void) const { return reinterpret_cast<pointer> (wrapped() + 1) ; }
      reference operator* (void) const { return *operator->() ; }

      template<typename ... _Args>
         static shared_ptr create (_Args ... args)
         {
            return shared_ptr{ new MyWrapped{ args... } } ;
         }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Tp>
   struct Ref
   {
      _Tp& m_value ;
      explicit Ref (_Tp& val) : m_value{ val } {}
      operator _Tp& (void) const { return m_value ; }
   } ;

template<typename _Tp>
   inline Ref<_Tp> ref (_Tp& val) { return Ref<_Tp>{ val } ; }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
