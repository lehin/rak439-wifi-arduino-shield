////////////////////////////////////////////////////////////////////////////////////////////
//
// scheduler.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "list.h"
#include "ring-buffer.h"
#include "singleton.h"
#include "aux/function.h"
#include <avr/interrupt.h>

ISR (TIMER1_COMPA_vect) ;
ISR (TIMER1_COMPB_vect) ;

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

class Scheduler
{
   SINGLETON_DECL (Scheduler) ;
   typedef aux::function<void(void)> Slot ;

   struct Event
   {
      uint16_t m_id ;
      uint16_t m_time ;
      bool m_cr ;
      Slot m_slot ;

      Event (uint16_t id, uint16_t delay_ms, Slot&& slot) ;
      Event (void) {}

      friend inline bool operator< (const Event& lhs, const Event& rhs)
      {
         if (lhs.m_cr == rhs.m_cr)
            return lhs.m_time < rhs.m_time ;

         return lhs.m_cr < rhs.m_cr ;
      }
   } ;

   typedef List<Event> Events ;

public:
   Scheduler (void) ;

   static uint16_t schedule (Slot&& slot, uint16_t delay_ms) ;
   static void cancel (uint16_t schedule) ;

private:
   static void _call_handlers (Events&& evts) ;
   static void _check_events (void) ;
   static Events _check_events_prepare (void) ;
   static void _handle_overflow (void) ;
   static Events _handle_overflow_prepare (void) ;
   static void _schedule_check_events (void) ;
   static uint16_t _next_id (void) ;


   friend void ::TIMER1_COMPA_vect (void) ;
   friend void ::TIMER1_COMPB_vect (void) ;

   static void _on_overflow (void) ;

private:

   static Events m_schedule ;
   static uint16_t m_schedule_id ;
   static volatile bool m_check_events_scheduled ;
   static volatile bool m_overflow_scheduled ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
