////////////////////////////////////////////////////////////////////////////////////////////
//
// ll-scheduler.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "ll-scheduler.h"
#include <avr/sleep.h>
#include <util/atomic.h>

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

LowLatencySched::Queue LowLatencySched::s_pending ;

void LowLatencySched::process (void)
{
   Slot slot ;

   while (true)
   {
      cli() ;
      
      if (!s_pending.empty())
         slot = s_pending.pop_front() ;
      else
      {
         sleep_enable() ;
         sei() ;
         sleep_cpu() ;
         sleep_disable() ;
         
         return ;
      }
      
      sei() ;
      slot() ;
   }
} ;

void LowLatencySched::process_no_sleep (void)
{
   Slot slot ;

   while (true)
   {
      ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
      {
         if (s_pending.empty())
            return ;
            
         slot = s_pending.pop_front() ;
      }
      
      slot() ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
