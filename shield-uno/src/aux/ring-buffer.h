////////////////////////////////////////////////////////////////////////////////////////////
//
// ring-buffer.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stddef.h"
#include "stdint.h"
#include "forward.h"

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace impl
{
   template<bool _large>
      struct SelectSizeTypeImpl
      {
         typedef size_t type ;
      } ;

   template<>
      struct SelectSizeTypeImpl<false>
      {
         typedef uint8_t type ;
      } ;

   template<size_t _sz>
      struct SelectSizeType
      {
         static constexpr bool _large = _sz > 255 ;
         typedef typename SelectSizeTypeImpl<_large>::type type ;
      } ;
}

template<size_t _sz, typename _Tp>
   class RingBuffer
   {
      RingBuffer (const RingBuffer&) = delete ;
      int operator= (const RingBuffer&) = delete ;

      typedef typename impl::SelectSizeType<_sz>::type size_type ;

      static constexpr size_type BUFSIZE = _sz ;
      typedef _Tp Data[BUFSIZE] ;

   public:
      typedef _Tp value_type ;

      static constexpr size_type capacity (void) { return BUFSIZE - 1 ; }

      class iterator
      {
         friend class RingBuffer ;
         iterator (value_type *data, size_type idx) : m_data { data }, m_idx { idx } {}

      public:
         iterator (void) {}

         iterator& operator++ (void) { m_idx = _next (m_idx) ; return *this ; }
         iterator operator++ (int) { iterator res = *this ; m_idx = _next (m_idx) ; return res ; }

         iterator operator+ (size_type offset) const
         {
            size_type idx = (m_idx + offset) % BUFSIZE ;
            return { m_data, idx } ;
         }

         value_type& operator* (void) const { return m_data[m_idx] ; }
         value_type* operator-> (void) const { return m_data + m_idx ; }

         friend inline bool operator== (const iterator& lhs, const iterator& rhs)
            { return lhs.m_idx == rhs.m_idx ; }

         friend inline bool operator!= (const iterator& lhs, const iterator& rhs)
            { return lhs.m_idx != rhs.m_idx ; }

         friend inline size_type operator- (const iterator& lhs, const iterator& rhs)
         {
            if (lhs.m_idx < rhs.m_idx)
               return lhs.m_idx + BUFSIZE - rhs.m_idx ;

            return lhs.m_idx - rhs.m_idx ;
         }

      private:
         value_type *m_data ;
         size_type m_idx ;
      } ;

   public:
      RingBuffer (void) :
         m_begin { 0 },
         m_end { 0 },
         m_size { 0 }
      {
      }

      iterator begin (void) { return { m_data, m_begin } ; }
      iterator end (void) { return iterator { m_data, m_end } ; }

      bool empty (void)
      {
         return m_size == 0 ;
      }

      bool full (void)
      {
         return m_size == capacity() ;
      }

      size_type size (void)
      {
         return m_size ;
      }

      void clear (void)
      {
         m_end = m_begin ;
         m_size = 0 ;
      }

      bool push_back (const value_type& val)
      {
         if (full())
            return false ;

         ++m_size ;
         m_data[m_end] = val ;
         m_end = _next (m_end) ;

         return true ;
      }

      bool push_back (value_type&& val)
      {
         if (full())
            return false ;

         ++m_size ;
         m_data[m_end] = aux::forward (val) ;
         m_end = _next (m_end) ;

         return true ;
      }

      value_type pop_front (void)
      {
         auto sve = m_begin ;

         m_begin = _next (m_begin) ;
         --m_size ;

         return forward (m_data[sve]) ;
      }

      void truncate_front (size_type sz)
      {
         m_begin += sz ;
         m_begin %= BUFSIZE ;
      }

      value_type& at (size_type idx)
      {
         return m_data [(m_begin + idx) % BUFSIZE] ;
      }

      value_type& operator[] (size_type idx) { return at (idx) ; }

   private:
      static size_type _next (size_type idx)
      {
         return ++idx % BUFSIZE ;
      }

   private:
      Data m_data ;
      size_type m_begin, m_end, m_size ;
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
