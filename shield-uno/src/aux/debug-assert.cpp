////////////////////////////////////////////////////////////////////////////////////////////
//
// debug-assert.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "debug-assert.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <string.h>
#include "hw/gpio.h"
#include "util/uart-stream.h"
#include <util/delay.h>

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEBUG_ASSERT

static DebugAssertInfo* const DEBUG_ASSERT_EEPROM = (DebugAssertInfo*)(0x1000 - sizeof (DebugAssertInfo)) ;

DebugAssertInfo read_debug_assert_info (void)
{
   DebugAssertInfo res ;
   eeprom_read_block (&res, DEBUG_ASSERT_EEPROM, sizeof (res)) ;
   return res ;
}

#endif

////////////////////////////////////////////////////////////////////////////////////////////

void debug_assert (bool result, const char *ctx)
{
   if (result)
      return ;

   #ifdef DEBUG_ASSERT

   util::UartStream{} << util::endl{} << util::pstr{} << ctx << util::endl{} ;

   DebugAssertInfo info ;
   strncpy_P (info.m_ctx, ctx, sizeof (info.m_ctx)) ;
   eeprom_write_block (&info, DEBUG_ASSERT_EEPROM, sizeof (info)) ;

   #endif

   cli() ;

   for (;;)
   {
      hw::gpio::Stat::turn() ;
      _delay_ms (50) ;
      hw::gpio::Stat::turn() ;
      _delay_ms (50) ;
   }
}


////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
