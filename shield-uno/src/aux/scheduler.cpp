////////////////////////////////////////////////////////////////////////////////////////////
//
// scheduler.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "scheduler.h"
#include "ll-scheduler.h"
#include <avr/io.h>
#include <avr/power.h>
#include <util/atomic.h>
#include <alloca.h>

////////////////////////////////////////////////////////////////////////////////////////////

ISR (TIMER1_COMPA_vect)
{
   aux::Scheduler::_on_overflow() ;
}

ISR (TIMER1_COMPB_vect)
{
   aux::Scheduler::_schedule_check_events() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

SINGLETON_IMPL (Scheduler) ;

Scheduler::Event::Event (uint16_t id, uint16_t delay_ms, Slot&& slot) :
   m_id { id },
   m_slot { forward (slot) }
{
   // calculation are based on selected timer clock: 20MHz / 1024 (prescaler)
   // it gives 65527 ticks in 3355 ms and fits the 16-bit integer range

   auto cur = TCNT1 ;
   uint16_t tm = (cur + (uint32_t)delay_ms * 65527 / 3355) % 65527 ;

   m_time = tm ;
   m_cr = (tm < cur) || (!(cur & 0x8000) && m_overflow_scheduled) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

List<Scheduler::Event> Scheduler::m_schedule ;
uint16_t Scheduler::m_schedule_id ;

volatile bool Scheduler::m_check_events_scheduled ;
volatile bool Scheduler::m_overflow_scheduled ;

Scheduler::Scheduler (void)
{
   power_timer1_enable() ;

   // prescaler = clk / 1024, timer mode = CTC
   TCNT1 = 0 ;
   TCCR1A = 0 ;
   TCCR1B = _BV(CS10) | _BV (CS12) | _BV (WGM12) ;

   OCR1A = 65527 ;
   TIMSK1 = _BV(OCIE1A) ;   // включить только прерывание по переполнению (OCR1A) таймера 1
}

uint16_t Scheduler::schedule (Slot&& slot, uint16_t delay_ms)
{
   Event e { _next_id(), delay_ms, forward (slot) } ;

   auto ins = m_schedule.begin() ;
   for (; ins != m_schedule.end() && *ins < e; ++ins) ;

   auto it = m_schedule.insert (ins, forward (e)) ;
   if (it == m_schedule.begin())
      _schedule_check_events() ;

   return e.m_id ;
}

void Scheduler::cancel (uint16_t schedule)
{
   bool first = false ;

   for (auto del = m_schedule.begin(); del != m_schedule.end(); ++del)
      if (del->m_id == schedule)
      {
         first = (del == m_schedule.begin()) ;
         m_schedule.erase (del) ;
         break ;
      }

   if (first)
      _schedule_check_events() ;
}

void Scheduler::_call_handlers (Events&& evts)
{
   for (auto& e : evts)
      e.m_slot() ;
}

void Scheduler::_check_events (void)
{
   _call_handlers (_check_events_prepare()) ;
}

Scheduler::Events Scheduler::_check_events_prepare (void)
{
   m_check_events_scheduled = false ;

   auto last = m_schedule.begin() ;

   for (; last != m_schedule.end() && !last->m_cr; ++last)
   {
      if (last->m_time > TCNT1)
      {
         OCR1B = last->m_time ;
         TIFR1 = _BV(OCF1B) ;
      }

      if (last->m_time > TCNT1)
      {
         TIMSK1 = _BV(OCIE1A) | _BV(OCIE1B) ;
         break ;
      }
   }

   auto res = m_schedule.pull (m_schedule.begin(), last) ;

   if (m_schedule.empty())
      TIMSK1 = _BV(OCIE1A) ;

   return res ;
}

void Scheduler::_handle_overflow (void)
{
   m_overflow_scheduled = false ;
   _call_handlers (_handle_overflow_prepare()) ;
}

Scheduler::Events Scheduler::_handle_overflow_prepare (void)
{
   auto it = m_schedule.begin() ;
   for (; it != m_schedule.end() && !it->m_cr; ++it) ;

   auto res = m_schedule.pull (m_schedule.begin(), it) ;

   for (auto& e : m_schedule)
      e.m_cr = false ;

   _schedule_check_events() ;

   return res ;
}

inline
   void Scheduler::_schedule_check_events (void)
   {
      ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
      {
         if (m_check_events_scheduled)
            return ;

         m_check_events_scheduled = true ;
         TIMSK1 = _BV(OCIE1A) ;
      }

      LowLatencySched::schedule (&Scheduler::_check_events) ;
   }

inline
   uint16_t Scheduler::_next_id (void)
   {
      while (!++m_schedule_id) ;
      return m_schedule_id ;
   }

inline
   void __attribute__ ((always_inline)) Scheduler::_on_overflow (void)
   {
      m_overflow_scheduled = true ;
      LowLatencySched::schedule (&Scheduler::_handle_overflow) ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
