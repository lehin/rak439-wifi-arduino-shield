////////////////////////////////////////////////////////////////////////////////////////////
//
// array.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "forward.h"
#include <stdint.h>

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Tp, typename _SizeType, _SizeType _sz>
   class ArrayImpl
   {
      ArrayImpl (const ArrayImpl&) = delete ;
      int operator= (const ArrayImpl&) = delete ;

      typedef _SizeType size_type ;

      static constexpr size_type BUFSIZE = _sz ;
      typedef _Tp Buffer[BUFSIZE] ;

   public:
      typedef _Tp value_type ;
      typedef _Tp* iterator ;
      typedef const _Tp* const_iterator ;

   public:
      ArrayImpl (void) : m_end { 0 } {}

      iterator begin (void) { return m_buffer ; }
      iterator end (void) { return m_buffer + m_end ; }

      const_iterator begin (void) const { return m_buffer ; }
      const_iterator end (void) const { return m_buffer + m_end ; }

      bool empty (void) const { return m_end == 0 ; }
      bool full (void) const { return m_end == capacity() ; }
      size_type size (void) const { return m_end ; }
      void clear (void) { m_end = 0 ; }

      void resize (size_type newsize)
      {
         if (newsize <= capacity())
            m_end = newsize ;
      }

      bool push_back (const value_type& val)
      {
         if (full())
            return false ;

         m_buffer[m_end++] = val ;

         return true ;
      }

      bool push_back (value_type&& val)
      {
         if (full())
            return false ;

         m_buffer[m_end++] = aux::forward (val) ;

         return true ;
      }

      value_type& at (size_type idx)
         { return m_buffer[idx] ; }

      const value_type& at (size_type idx) const
         { return m_buffer[idx] ; }

      value_type& operator[] (size_type idx) { return at (idx) ; }
      const value_type& operator[] (size_type idx) const { return at (idx) ; }


      static constexpr size_type capacity (void) { return BUFSIZE ; }

      template<typename ... _Args>
         void setup (_Args ... args)
         {
            resize (_args_size (args...)) ;
            _fill_bytes (0, args...) ;
         }

   private:
      template<typename ... _Args>
         static constexpr size_type _args_size (uint8_t, _Args ... args)
         {
            return _args_size (args...) + 1 ;
         }

      static constexpr size_type _args_size (void) { return 0 ; }

      template<typename ... _Args>
         void _fill_bytes (size_type n, uint8_t byte, _Args ... args)
         {
            at(n) = byte ;
            _fill_bytes (n+1, args...) ;
         }

      void _fill_bytes (size_type) {}

   private:
      Buffer m_buffer ;
      size_type m_end ;
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Tp, uint8_t _sz> using Array = ArrayImpl<_Tp, uint8_t, _sz> ;
template<typename _Tp, uint16_t _sz> using Array16 = ArrayImpl<_Tp, uint16_t, _sz> ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
