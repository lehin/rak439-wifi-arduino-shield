////////////////////////////////////////////////////////////////////////////////////////////
//
// cmd-codes.h
//
// Copyright © 2018 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdint.h>

namespace host
{

////////////////////////////////////////////////////////////////////////////////////////////

enum class CmdCode : uint8_t
{
   SetNet = 0x10,
   SetPassphrase = 0x11,
   SetKey = 0x12,
   Test = 0x13,
   SetIpConfig = 0x14,
   SetDnsConfig = 0x15,

   GetConnStatus = 0x20,
   GetIpaddr = 0x21,
   GetMacaddr = 0x22,
   GetCurrSsid = 0x23,
   GetCurrBssid = 0x24,
   GetCurrRssi = 0x25,
   GetCurrEnct = 0x26,
   ScanNetworks = 0x27,
   StartServerTcp= 0x28,
   GetStateTcp = 0x29,
   DataSentTcp = 0x2a,
   AvailDataTcp = 0x2b,
   GetDataTcp = 0x2c,
   StartClientTcp= 0x2d,
   StopClientTcp = 0x2e,
   GetClientStateTcp= 0x2f,
   Disconnect = 0x30,
   GetIdxSsid = 0x31,
   GetIdxRssi = 0x32,
   GetIdxEnct = 0x33,
   ReqHostByName= 0x34,
   GetHostByName= 0x35,
   StartScanNetworks = 0x36,
   GetFwVersion = 0x37,
   GetTest = 0x38,
   SendDataUdp = 0x39,
   GetRemoteData = 0x3a,

   // all command with data_flag 0x40 send a 16bit len

   SendDataTcp = 0x44,
   GetDatabufTcp = 0x45,
   InsertDatabuf = 0x46,

   _Count
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace host
