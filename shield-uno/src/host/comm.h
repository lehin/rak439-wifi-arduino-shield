////////////////////////////////////////////////////////////////////////////////////////////
//
// comm.h
//
// Copyright © 2018 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "cmd-codes.h"
#include "marshall.h"
#include "hw/spi-slave.h"

namespace host
{

////////////////////////////////////////////////////////////////////////////////////////////

class Comm
{
   typedef bool (*PktHandler) (MarshallRecBuf& buf, hw::SpiSlave::Tx& tx) ;

public:
   struct Registrar
   {
      Registrar (CmdCode cmd, PktHandler fn) { _register_handler (cmd, fn) ; }
      void dummy (void) const {}
   } ;

public:
   static void initialize (void) ;

private:
   static void _on_spi_command (hw::SpiSlave::Received pkt) ;
   static void _register_handler (CmdCode cmd, PktHandler fn) ;

private:
   static PktHandler m_pkt_handlers[] ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

template<CmdCode _cmd, typename _Result>
   class CommHandler
   {
   protected:
      CommHandler (void) { s_registrar.dummy() ; }

   private:
      static bool _handler (MarshallRecBuf& buf, hw::SpiSlave::Tx& tx)
      {
         return unmarshall (_Result::on_command, buf, tx) ;
      }

   private:
      struct Registrar : Comm::Registrar
      {
         Registrar (void) : Comm::Registrar{ _cmd, _handler } {}
      } ;

      static Registrar s_registrar ;
   } ;


template<CmdCode _cmd, typename _Result>
   typename CommHandler<_cmd,_Result>::Registrar CommHandler<_cmd,_Result>::s_registrar ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace host
