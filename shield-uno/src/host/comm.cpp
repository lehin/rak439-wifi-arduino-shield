////////////////////////////////////////////////////////////////////////////////////////////
//
// comm.h
//
// Copyright © 2018 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "comm.h"
#include "cmd-codes.h"
#include "util/uart-dbg.h"

namespace host
{

////////////////////////////////////////////////////////////////////////////////////////////

Comm::PktHandler Comm::m_pkt_handlers[(size_t)CmdCode::_Count] ;

void Comm::initialize (void)
{
   hw::SpiSlave::initialize (_on_spi_command) ;
}

void Comm::_on_spi_command (hw::SpiSlave::Received pkt)
{
/*   if (util::UartDbg::enabled())
   {
      util::UartDbg{} << util::pstr{} << PSTR("SPI: ") ;

      for (auto byte : pkt)
         util::UartDbg{} << util::hex{} << byte << ' ' ;

      util::UartDbg{} << util::endl() ;
   }*/

   if (pkt.size() < 2)
   {
      if (util::UartDbg::enabled())
         util::UartDbg{} << util::pstr{} << PSTR("SPI: cmd packet is too small") << util::endl{} ;

      return ;
   }

   uint8_t cmd = *pkt.begin() ;
   if (cmd >= sizeof (m_pkt_handlers) / sizeof (*m_pkt_handlers))
   {
      if (util::UartDbg::enabled())
         util::UartDbg{} << util::pstr{} << PSTR("SPI: cmd value too big") << util::endl{} ;

      return ;
   }

   auto h = m_pkt_handlers[cmd] ;
   if (!h)
   {
      if (util::UartDbg::enabled())
         util::UartDbg{} << util::pstr{} << PSTR("SPI: cmd handler is not found") << util::endl{} ;

      return ;
   }

   MarshallRecBuf buf{ aux::range (pkt.begin() + 1, pkt.end()), (cmd & 0x40) != 0 } ;

   // these 2 lines fill exactly two first bytes (cmd code and number of params, at receiving)
   // of shared rx/tx buffer
   auto tx = hw::SpiSlave::tx() ;
   tx << (uint8_t) (cmd | 0x80) ;

   if (!h (buf, tx) && util::UartDbg::enabled())
      util::UartDbg{} << util::pstr{} << PSTR("SPI: cmd handler exec error") << util::endl{} ;
}

void Comm::_register_handler (CmdCode cmd, PktHandler fn)
{
   const uint8_t idx = (size_t)cmd ;

   aux::debug_assert( idx < sizeof (m_pkt_handlers) / sizeof (*m_pkt_handlers), PSTR("SPIPKTHANDLERSSIZE") ) ;
   aux::debug_assert( !m_pkt_handlers[idx], PSTR("SPIPKTHANDLERDUP") ) ;

   m_pkt_handlers[idx] = fn ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace host
