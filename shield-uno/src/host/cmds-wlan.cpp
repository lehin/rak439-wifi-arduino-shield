////////////////////////////////////////////////////////////////////////////////////////////
//
// cmds-wlan.cpp
//
// Copyright © 2018 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "comm.h"
#include "lan/lan-service.h"
#include "lan/lan-socket.h"
#include "lan/dn-service.h"

namespace host
{

////////////////////////////////////////////////////////////////////////////////////////////

struct GetConnStatus : CommHandler<CmdCode::GetConnStatus, GetConnStatus>
{
   GetConnStatus (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx)
   {
      marshall (tx, lan::Service::connection_state()) ;
      return true ;
   }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

struct SetNet : CommHandler<CmdCode::SetNet, SetNet>
{
   SetNet (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, aux::StringRef ssid)
   {
      bool res = true ;

      if (!ssid.empty())
         lan::Service::connect_unecrypted (ssid) ;
      else
         res = false ;

      marshall (tx, res) ;
      return true ;
   }
} ;

struct SetPassphrase : CommHandler<CmdCode::SetPassphrase, SetPassphrase>
{
   SetPassphrase (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, aux::StringRef ssid, aux::StringRef pass)
   {
      bool res = true ;

      if (!ssid.empty() && !pass.empty())
         lan::Service::connect_wpa (ssid, pass) ;
      else
         res = false ;

      marshall (tx, res) ;
      return true ;
   }
} ;

struct SetKey : CommHandler<CmdCode::SetKey, SetKey> // WEP encryption is not implemented
{
   SetKey (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, aux::StringRef, uint8_t, aux::Range<uint8_t*>)
    { return false ; }
} ;

struct Disconnect : CommHandler<CmdCode::Disconnect, Disconnect>
{
   Disconnect (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t)
   {
      lan::Service::disconnect() ;
      marshall (tx, true) ;
      return true ;
   }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

struct SetIpConfig : CommHandler<CmdCode::SetIpConfig, SetIpConfig>
{
   SetIpConfig (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t valid_params, uint32_t local_ip, uint32_t gateway, uint32_t subnet)
   {
      bool res = true ;

      switch (valid_params)
      {
         case 0: res = false ; break ;
         case 1: gateway = (local_ip & 0x00FFFFFFul) | 0x01000000ul ;
         case 2: subnet = 0x00FFFFFFul ;
         default:;
      }

      lan::Service::ip_config ({ ntohl (local_ip), ntohl (subnet), ntohl (gateway), 0 })   ;

      marshall (tx, res) ;
      return true ;
   }
} ;

struct SetDnsConfig : CommHandler<CmdCode::SetDnsConfig, SetDnsConfig>
{
   SetDnsConfig (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t valid_params, uint32_t dns1, uint32_t)
   {
      bool res = true ;

      switch (valid_params)
      {
         case 0: res = false ; break ;
         default: lan::Service::set_dns ({ ntohl (dns1) }) ;
      }

      marshall (tx, true) ;
      return true ;
   }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

struct GetCurrSsid : CommHandler<CmdCode::GetCurrSsid, GetCurrSsid>
{
   GetCurrSsid (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t)
   {
      auto ssid = lan::Service::ssid() ;
      marshall (tx, aux::StringRef{ ssid, ssid + strlen (ssid) + 1 }) ;
      return true ;
   }
} ;

namespace
{
   static void _marshall_mac (hw::SpiSlave::Tx& tx, const uint8_t *mac)
   {
      uint8_t out[6] ;

      for (uint8_t n = 0; n < 6; ++n)
         out[5-n] = mac[n] ;

      marshall (tx, aux::range (out + 0, out + 6)) ;
   }
}

struct GetCurrBssid : CommHandler<CmdCode::GetCurrBssid, GetCurrBssid>
{
   GetCurrBssid (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t)
   {
      _marshall_mac (tx, lan::Service::bssid()) ;
      return true ;
   }
} ;

struct GetCurrRssi : CommHandler<CmdCode::GetCurrRssi, GetCurrRssi>
{
   GetCurrRssi (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t)
   {
      marshall (tx, hw::rak439::Wmi::get_rssi()) ;
      return true ;
   }
} ;

struct GetCurrEnct : CommHandler<CmdCode::GetCurrEnct, GetCurrEnct>
{
   GetCurrEnct (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t)
   {
      marshall (tx, lan::Service::encryption_type()) ;
      return true ;
   }
} ;

struct GetIpaddr : CommHandler<CmdCode::GetIpaddr, GetIpaddr>
{
   GetIpaddr (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t)
   {
      auto& ip = lan::Service::ip_config() ;
      marshall (tx, ntohl (ip.m_addr), ntohl (ip.m_mask), ntohl (ip.m_gateway)) ;
      return true ;
   }
} ;

struct GetMacaddr : CommHandler<CmdCode::GetMacaddr, GetMacaddr>
{
   GetMacaddr (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t)
   {
      _marshall_mac (tx, lan::Service::mac()) ;
      return true ;
   }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static uint32_t _host_by_name_ip ;
}

struct ReqHostByName : CommHandler<CmdCode::ReqHostByName, ReqHostByName>
{
   ReqHostByName (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, aux::StringRef name)
   {
      _host_by_name_ip = 0xFFFFFFFF ;
      lan::DnService::request (name, [] (uint16_t, uint32_t ip) { _host_by_name_ip = ip ; }) ;

      marshall (tx, true) ;
      return true ;
   }
} ;

struct GetHostByName : CommHandler<CmdCode::GetHostByName, GetHostByName>
{
   GetHostByName (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx)
   {
      marshall (tx, htonl (_host_by_name_ip)) ;
      return true ;
   }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

struct StartScanNetworks : CommHandler<CmdCode::StartScanNetworks, StartScanNetworks>
{
   StartScanNetworks (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx)
   {
      lan::Service::scan_start() ;
      marshall (tx, true) ;
      return true ;
   }
} ;

struct ScanNetworks : CommHandler<CmdCode::ScanNetworks, ScanNetworks>
{
   ScanNetworks (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx)
   {
      tx << lan::Service::scan_get_count() ;
      lan::Service::scan_for_each ([&] (auto& info) { marshall_push (tx, info.m_ssid) ; }) ;
      return true ;
   }
} ;

struct GetIdxRssi : CommHandler<CmdCode::GetIdxRssi, GetIdxRssi>
{
   GetIdxRssi (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t idx)
   {
      uint8_t rssi = 0 ;
      uint8_t n = 0 ;

      lan::Service::scan_for_each ([&] (auto& info) { if (n++ == idx) rssi = info.m_rssi ; }) ;

      marshall (tx, rssi) ;
      return true ;
   }
} ;

struct GetIdxEnct : CommHandler<CmdCode::GetIdxEnct, GetIdxEnct>
{
   GetIdxEnct (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t idx)
   {
      lan::Service::EncryptionType enc = lan::Service::EncryptionType::Unencrypted ;
      uint8_t n = 0 ;

      lan::Service::scan_for_each ([&] (auto& info) { if (n++ == idx) enc = info.m_encryption ; }) ;

      marshall (tx, enc) ;
      return true ;
   }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace host
