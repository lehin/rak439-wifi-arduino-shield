////////////////////////////////////////////////////////////////////////////////////////////
//
// unwind.h
//
// Copyright © 2018 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "hw/spi-slave.h"
#include "aux/string.h"

namespace host
{

////////////////////////////////////////////////////////////////////////////////////////////

class MarshallRecBuf ;

struct MarshallExtractorBase
{
   MarshallRecBuf& m_buf ;
   MarshallExtractorBase (MarshallRecBuf& buf) : m_buf{ buf } {}
} ;

template<typename _Tp>
   struct MarshallExtractor ;

////////////////////////////////////////////////////////////////////////////////////////////

class MarshallRecBuf
{
   template<typename _Tp> friend struct MarshallExtractor ;

   uint8_t m_parms ;
   bool m_len16bit ;
   hw::SpiSlave::Received m_data ;

public:
   explicit MarshallRecBuf (hw::SpiSlave::Received data, bool len16bit) :
      m_parms{ *data.begin() },
      m_len16bit{ len16bit },
      m_data{ aux::range (data.begin() + 1, data.end()) }
   {
   }

   template<typename _Tp>
      _Tp extract (void) { return MarshallExtractor<_Tp>{ *this } . extract() ; }

   bool ok (void) const { return !m_parms ; }
   uint8_t parms (void) const { return m_parms ; }
   bool len16bit (void) const { return m_len16bit ; }

private:
   bool _check_parms_counter (void)
   {
      if (!m_parms)
         m_parms = (uint8_t)-1 ;

      if (m_parms == (uint8_t)-1)
         return false ;

      --m_parms ;
      return true ;
   }

   bool _check_parm_size (size_t sz)
   {
      if (_pop_length() != sz)
         return (m_parms = (uint8_t)-1, false) ;

      if (m_data.size() < sz)
         return (m_parms = (uint8_t)-1, false) ;

      return true ;
   }

   size_t _pop_length (void)
   {
      size_t read ;
      if (!m_len16bit)
         read = m_data[0] ;
      else
         read = ((uint16_t)m_data[0] << 8) | m_data[1] ;

      m_data = aux::range (m_data.begin() + 1 + m_len16bit, m_data.end()) ;

      return read ;
   }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Tp>
   struct MarshallExtractor : MarshallExtractorBase
   {
      MarshallExtractor (MarshallRecBuf& buf) : MarshallExtractorBase{ buf } {}

      _Tp extract (void) const
      {
         if (!m_buf._check_parms_counter() || !m_buf._check_parm_size (sizeof (_Tp)))
            return _Tp{} ;

         _Tp res = *(_Tp*)m_buf.m_data.begin() ;

         m_buf.m_data = aux::range (m_buf.m_data.begin() + sizeof (_Tp), m_buf.m_data.end()) ;

         return res ;
      }
   } ;

template<>
   struct MarshallExtractor<aux::StringRef> : MarshallExtractorBase
   {
      MarshallExtractor (MarshallRecBuf& buf) : MarshallExtractorBase{ buf } {}

      aux::StringRef extract (void) const
      {
         if (!m_buf._check_parms_counter())
            return { nullptr, nullptr } ;

         size_t sz = m_buf._pop_length() ;

         if (m_buf.m_data.size() < sz)
            return (m_buf.m_parms = (uint8_t)-1, aux::StringRef{ nullptr, nullptr }) ;

         aux::StringRef res{ (const char*)m_buf.m_data.begin(), (const char*)m_buf.m_data.begin() + sz } ;

         m_buf.m_data = aux::range (m_buf.m_data.begin() + sz, m_buf.m_data.end()) ;

         return res ;
      }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _P, typename... _Tail>
   struct UnmarshallImpl ;

template<typename... _Args>
   struct Unmarshall : UnmarshallImpl<_Args...>
   {
      Unmarshall (MarshallRecBuf& buf) : UnmarshallImpl<_Args...>{ buf } {}
   } ;

template<>
   struct Unmarshall<>
   {
      Unmarshall (MarshallRecBuf& buf) {}

      template<typename _Fn, typename... _Args>
         bool operator() (_Fn fn, hw::SpiSlave::Tx& tx, _Args... args) const
            { return fn (tx, args...) ; }
   } ;

template<typename _P, typename... _Tail>
   struct UnmarshallImpl
   {
      _P m_p ;
      Unmarshall<_Tail...> m_tail ;

      UnmarshallImpl (MarshallRecBuf& buf) : m_p{ buf.extract<_P>() }, m_tail{ buf } {}

      template<typename _Fn, typename... _Args>
         bool operator() (_Fn fn, hw::SpiSlave::Tx& tx, _Args... args) const
            { return m_tail (fn, tx, args..., m_p) ; }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _SizeType>
   struct MarshallPushSize
   {
      static void push (hw::SpiSlave::Tx& tx, size_t sz)
      {
         tx << (uint8_t)sz ;
      }
   } ;

template<>
   struct MarshallPushSize<uint16_t>
   {
      static void push (hw::SpiSlave::Tx& tx, size_t sz)
      {
         tx << (uint8_t)(sz >> 8) << (uint8_t)sz ;
      }
   } ;

template<typename _SizeType, typename _Tp>
   struct MarshallPush
   {
      static void push (hw::SpiSlave::Tx& tx, _Tp p)
      {
         MarshallPushSize<_SizeType>::push (tx, sizeof (_Tp)) ;
         tx << p ;
      }
   } ;

template<typename _SizeType, typename _It>
   struct MarshallPush<_SizeType, aux::Range<_It> >
   {
      static void push (hw::SpiSlave::Tx& tx, aux::Range<_It> r)
      {
         MarshallPushSize<_SizeType>::push (tx, r.size() * sizeof (typename aux::Range<_It>::value_type)) ;
         tx << r ;
      }
   } ;

template<typename _SizeType>
   struct MarshallPush<_SizeType, aux::String>
   {
      static void push (hw::SpiSlave::Tx& tx, const aux::String& s)
      {
         MarshallPushSize<_SizeType>::push (tx, s.length()) ;
         tx << s.as_range() ;
      }
   } ;

template<typename _SizeType>
   struct MarshallPush<_SizeType, const char*>
   {
      static void push (hw::SpiSlave::Tx& tx, const char *str)
      {
         auto sz = strlen (str) ;
         MarshallPushSize<_SizeType>::push (tx, sz) ;
         tx << aux::range (str, str + sz) ;
      }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _SizeType, typename... _Args>
   struct Marshall ;

template<typename _SizeType, typename _P, typename... _Tail>
   struct MarshallImpl
   {
      static uint8_t count (void) { return Marshall<_SizeType,_Tail...>::count() + 1 ; }
      static void push (hw::SpiSlave::Tx& tx, _P p, _Tail... tail)
      {
         MarshallPush<_SizeType,_P>::push (tx, p) ;
         Marshall<_SizeType,_Tail...>::push (tx, tail...) ;
      }
   } ;

template<typename _SizeType, typename... _Args>
   struct Marshall : MarshallImpl<_SizeType, _Args...>
   {
   } ;

template<typename _SizeType>
   struct Marshall<_SizeType>
   {
      static uint8_t count (void) { return 0 ; }
      static void push (hw::SpiSlave::Tx&) {}
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename... _Args>
   inline bool unmarshall (bool (*fn) (hw::SpiSlave::Tx& tx, _Args...), MarshallRecBuf& buf, hw::SpiSlave::Tx& tx)
   {
      Unmarshall<_Args...> u { buf } ;

      if (!buf.ok())
         return false ;

      return u (fn, tx) ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

template<typename... _Args>
   inline void marshall (hw::SpiSlave::Tx& tx, _Args... args)
   {
      typedef Marshall<uint8_t,_Args...> TheMarshall ;

      tx << TheMarshall::count() ;
      TheMarshall::push (tx, args...) ;
   }

template<typename... _Args>
   inline void marshall16 (hw::SpiSlave::Tx& tx, _Args... args)
   {
      typedef Marshall<uint16_t,_Args...> TheMarshall ;

      tx << TheMarshall::count() ;
      TheMarshall::push (tx, args...) ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Tp>
   inline void marshall_push (hw::SpiSlave::Tx& tx, _Tp p)
   {
      typedef MarshallPush<uint8_t,_Tp> Push ;
      Push::push (tx, p) ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace host
