#include "comm.h"
#include "lan/lan-socket.h"
#include "hw/rak439/rxstream.h"
#include "aux/ring-buffer.h"
#include "util/uart-dbg.h"

namespace host
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static constexpr uint8_t SLOTSNUM = 4 ;
   static constexpr size_t RECBUF_SIZE = XRAMEND > 0x4000 ? 2048 : (XRAMEND > 0x2000 ? 1024 : 384) ;

   struct Slot
   {
      lan::Socket m_server,
                  m_client ;

      bool m_listening = false ;

      uint32_t m_peer_ip = 0 ;
      uint16_t m_peer_port = 0 ;

      typedef aux::RingBuffer<RECBUF_SIZE,uint8_t> DataBuf ;
      DataBuf* m_buf = nullptr ;
   } ;

   static Slot _slots[SLOTSNUM] ;

   enum class Proto : uint8_t{ Tcp, Udp } ;
   enum class TcpState : uint8_t { Closed, Listen, SynSent, SynRcvd, Established, FinWait1, FinWait2, CloseWait, Closing, LastAck, TimeWait } ;

   void _on_data_received (uint8_t slot, const lan::Socket::ReceiveData& data)
   {
      auto& sl = _slots[slot] ;
      sl.m_peer_ip = data.m_addr.m_addr ;
      sl.m_peer_port = data.m_addr.m_port ;

      if (!data.m_rx.unread())
         return ;

      auto& buf = sl.m_buf ;
      if (!buf)
         buf = new Slot::DataBuf ;

      while (data.m_rx.unread())
         buf->push_back (data.m_rx.read()) ;
   }

   void _delete_rec_data (uint8_t slot)
   {
      if (util::UartDbg::enabled())
         util::UartDbg{} << util::pstr{} << PSTR("Socket at slot ") << slot << util::pstr{} << PSTR(" closed") << util::endl{} ;

      if (_slots[slot].m_buf)
      {
         delete _slots[slot].m_buf ;
         _slots[slot].m_buf = nullptr ;
      }
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

struct StartServerTcp : CommHandler<CmdCode::StartServerTcp, StartServerTcp>
{
   StartServerTcp (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint16_t port, uint8_t slot, Proto proto)
   {
      if (slot >= SLOTSNUM)
         return false ;

      auto& sl = _slots[slot] ;
      auto& sock = sl.m_server ;

      if (!sock.is_valid())
      {
         port = ntohs (port) ;

         sock.open (proto == Proto::Tcp ? lan::Socket::Type::Tcp : lan::Socket::Type::Udp) ;
         sock.bind (port) ;

         if (util::UartDbg::enabled())
            util::UartDbg{} << util::pstr{} << PSTR("Opening slot ") << slot << util::pstr{} << PSTR(" at port ") << port << util::endl{} ;
      }

      if (proto == Proto::Udp)
      {
         sock.on_receive ([slot] (const lan::Socket::ReceiveData& rx) { _on_data_received (slot, rx) ; }) ;
         sock.on_close ([slot] { _delete_rec_data (slot) ; }) ;
      }
      else
      {
         if (util::UartDbg::enabled())
            util::UartDbg{} << util::pstr{} << PSTR("Socket at slot ") << slot << util::pstr{} << PSTR(" is listening") << util::endl{} ;

         sock.listen (1) ;
         sl.m_listening = true ;
         sock.on_incoming
            (
               [slot]
               {
                  if (util::UartDbg::enabled())
                     util::UartDbg{} << util::pstr{} << PSTR("Incoming connection at slot ") << slot << util::pstr{} << PSTR("...") << util::endl{} ;

                  auto& s = _slots[slot] ;
                  s.m_client = s.m_server.accept() ;
                  s.m_client.on_receive ([slot] (const lan::Socket::ReceiveData& rx) { _on_data_received (slot, rx) ; }) ;
                  s.m_client.on_close ([slot] { _delete_rec_data (slot) ; }) ;
                  s.m_listening = false ;
               }
            ) ;

         sock.on_close ([slot] { _slots[slot].m_listening = false ; }) ;
      }

      marshall (tx, true) ;
      return true ;
   }
} ;

struct GetStateTcp : CommHandler<CmdCode::GetStateTcp, GetStateTcp>
{
   GetStateTcp (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t slot)
   {
      marshall (tx, slot < SLOTSNUM && _slots[slot].m_listening) ;
      return true ;
   }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

struct StartClientTcp : CommHandler<CmdCode::StartClientTcp, StartClientTcp>
{
   StartClientTcp (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint32_t ip, uint16_t port, uint8_t slot, Proto proto)
   {
      if (slot >= SLOTSNUM)
         return false ;

      auto& sl = _slots[slot] ;
      auto& cli = sl.m_client ;
      cli.close() ;
      cli.open (proto == Proto::Tcp ? lan::Socket::Type::Tcp : lan::Socket::Type::Udp) ;

      if (proto == Proto::Tcp)
      {
         if (util::UartDbg::enabled())
            util::UartDbg{} << util::pstr{} << PSTR("Connecting socket at slot ") << slot << util::pstr{} << PSTR("...") << util::endl{} ;

         cli.connect ({ ntohl (ip), ntohs (port) }) ;
         cli.on_receive ([slot] (const lan::Socket::ReceiveData& rx) { _on_data_received (slot, rx) ; }) ;
         cli.on_close ([slot] { _delete_rec_data (slot) ; }) ;
      }
      else
      {
         sl.m_peer_ip = ntohl (ip) ;
         sl.m_peer_port = ntohs (port) ;

         if (sl.m_buf)
         {
            delete sl.m_buf ;
            sl.m_buf = nullptr ;
         }
      }

      marshall (tx, true) ;
      return true ;
   }
} ;

struct StopClientTcp : CommHandler<CmdCode::StopClientTcp, StopClientTcp>
{
   StopClientTcp (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t slot)
   {
      if (slot >= SLOTSNUM)
         return false ;

      auto& sl = _slots[slot] ;

      sl.m_client.close() ;

      if (sl.m_server.is_valid() && sl.m_server.type() == lan::Socket::Type::Udp)
         sl.m_server.close() ;

      marshall (tx, true) ;
      return true ;
   }
} ;

struct GetClientStateTcp : CommHandler<CmdCode::GetClientStateTcp, GetClientStateTcp>
{
   GetClientStateTcp (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t slot)
   {
      auto ret = TcpState::Closed ;

      if (slot < SLOTSNUM && _slots[slot].m_client.is_valid())
         ret = TcpState::Established ;

      marshall (tx, ret) ;
      return true ;
   }
} ;

struct AvailDataTcp : CommHandler<CmdCode::AvailDataTcp, AvailDataTcp>
{
   AvailDataTcp (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t slot)
   {
      if (slot >= SLOTSNUM)
         marshall (tx, (uint16_t)0) ;
      else
      {
         auto& buf = _slots[slot].m_buf ;
         if (buf)
            marshall (tx, (uint16_t)buf->size()) ;
         else
            marshall (tx, (uint16_t)0) ;
      }

      return true ;
   }
} ;

struct GetDataTcp : CommHandler<CmdCode::GetDataTcp, GetDataTcp>
{
   GetDataTcp (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t slot, uint16_t peek)
   {
      if (slot >= SLOTSNUM)
         return false ;

      auto& buf = _slots[slot].m_buf ;
      if (!buf || buf->empty())
         return false ;

      marshall (tx, *buf->begin()) ;

      if (!peek)
         buf->pop_front() ;

      return true ;
   }
} ;

struct GetDatabufTcp : CommHandler<CmdCode::GetDatabufTcp, GetDatabufTcp>
{
   GetDatabufTcp (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t slot)
   {
      if (slot >= SLOTSNUM)
         return false ;

      auto& buf = _slots[slot].m_buf ;

      if (!buf)
         marshall16 (tx, aux::range ((char*)0, (char*)0)) ;
      else
      {
         marshall16 (tx, aux::range (buf->begin(), buf->end())) ;
         buf->clear() ;
      }

      return true ;
   }
} ;

struct SendDataTcp : CommHandler<CmdCode::SendDataTcp, SendDataTcp>
{
   SendDataTcp (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t slot,  aux::StringRef data)
   {
      if (slot >= SLOTSNUM)
         return false ;

      auto& s = _slots[slot].m_client ;
      if (!s.is_valid())
         return false ;

      s.send (data.begin(), data.size()) ;
      marshall (tx, true) ;
      return true ;
   }
} ;

struct DataSentTcp : CommHandler<CmdCode::DataSentTcp, DataSentTcp>
{
   DataSentTcp (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t)
   {
      marshall (tx, true) ;
      return true ;
   }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

struct SendDataUdp : CommHandler<CmdCode::SendDataUdp, SendDataUdp>
{
   SendDataUdp (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t slot)
   {
      if (slot >= SLOTSNUM)
         return false ;

      auto& sl = _slots[slot] ;
      if (!sl.m_peer_ip || !sl.m_peer_port)
         return false ;

      auto& buf = sl.m_buf ;
      if (buf && buf->size())
      {
         sl.m_server.sendto (buf->begin().operator->(), buf->size(), { sl.m_peer_ip, sl.m_peer_port }) ;
         buf->clear() ;
      }

      marshall (tx, true) ;
      return true ;
   }
} ;

struct InsertDatabuf : CommHandler<CmdCode::InsertDatabuf, InsertDatabuf>
{
   InsertDatabuf (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t slot, aux::StringRef data)
   {
      if (slot >= SLOTSNUM)
         return false ;

      auto& buf = _slots[slot].m_buf ;
      if (!buf)
         buf = new Slot::DataBuf ;

      for (auto byte : data)
         buf->push_back (byte) ;

      marshall (tx, true) ;
      return true ;
   }
} ;

struct GetRemoteData : CommHandler<CmdCode::GetRemoteData, GetRemoteData>
{
   GetRemoteData (void) {}

   static bool on_command (hw::SpiSlave::Tx& tx, uint8_t slot)
   {
      if (slot >= SLOTSNUM)
         return false ;

      auto& sl = _slots[slot] ;

      marshall (tx, htonl (sl.m_peer_ip), htons (sl.m_peer_port)) ;
      return true ;
   }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace host
