////////////////////////////////////////////////////////////////////////////////////////////
//
// lan-service.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/list.h"
#include "aux/ring-buffer.h"
#include "aux/string.h"
#include "hw/rak439/sock.h"
#include "hw/rak439/wmi-events.h"

namespace lan
{

////////////////////////////////////////////////////////////////////////////////////////////

class Socket ;

class Service
{
   static constexpr uint8_t MAX_SSID_LEN = 32 ;
   static constexpr uint8_t MAX_PSK_LEN = 64 ;

   enum class State : uint8_t{ Undefined, Initialized, HwReady, Connected, Configured } ;

public:
   enum class ConnectionState : uint8_t{ Idle, NoSsidAvail, ScanCompleted, Connected, ConnectFailed, ConnectionLost, Disconnected } ;
   enum class EncryptionType : uint8_t{ Unencrypted, Wep, Wpa } ;

   struct IpConfig
   {
      uint32_t m_addr ;
      uint32_t m_mask ;
      uint32_t m_gateway ;
      uint32_t m_dns ;
   } ;

   struct ScanInfo
   {
      aux::String m_ssid ;
      EncryptionType m_encryption = EncryptionType::Unencrypted ;
      uint8_t m_rssi = 255 ;
   } ;

public:
   Service (void) ;

   static void initialize (void) ;
   static void shutdown (void) ;

   static const uint8_t* mac (void) ;
   static ConnectionState connection_state (void) { return m_connection_state ; }
   static EncryptionType encryption_type (void) { return m_encryption_type ; }

   static const char* ssid (void) { return m_ssid ; }
   static const uint8_t* bssid (void) { return m_bssid ; }

   static const IpConfig& ip_config (void) { return m_ip_config ; }
   static void ip_config (const IpConfig& ipcfg) ;
   static void set_dns (uint32_t dns) { m_ip_config.m_dns = dns ; }

   static void connect_unecrypted (const aux::StringRef& ssid) ;
   static void connect_wpa (const aux::StringRef& ssid, const aux::StringRef& passwd) ;
   static bool is_connected (void) { return m_state >= State::Connected ; }
   static void disconnect(void) ;

   static void scan_start (void) ;
   static uint8_t scan_get_count (void) ;

   template<typename _Cb>
      static void scan_for_each (_Cb cb)
      {
         for (auto& info : m_scan_results)
            cb (info) ;
      }

   static void register_socket (Socket *s) ;
   static void unregister_socket (Socket *s) ;

private:
   using ReceiveData = hw::rak439::Sock::ReceiveData ;

   static void _check_hw_ready (void) ;
   static bool _set_ssid (const aux::StringRef& ssid) ;
   static void _initialize_wlan (void) ;
   static void _on_wmi_ready (void) ;
   static void _on_connected (void) ;
   static void _on_disconnected (State dst_state) ;
   static void _on_configured (void) ;
   static void _on_incoming_connection (uint32_t sock) ;
   static void _on_sock_close (uint32_t sock) ;
   static void _on_sock_receive (const ReceiveData& rcv) ;

   static void _dhcp_query (void) ;
   static void _static_ip (void) ;

   template<typename _Fn>
      static bool _set_state (State state, _Fn fn)
      {
         if (!fn())
            return false ;

         return _set_state (state) ;
      }

   static bool _set_state (State state) ;

   static void _on_bss_info_event (const hw::rak439::evts::BssInfo& bi) ;

private:
   static State m_state ;

   typedef aux::List<Socket*> Sockets ;
   static Sockets m_sockets ;

   static uint8_t m_mac[] ;
   static uint8_t m_bssid[] ;
   static char m_ssid[] ;
   static char m_passwd[] ;

   enum class IpConfigStatus : uint8_t{ NeedDhcp, NeedStatic, WaitDhcp, WaitStatic, Dhcp, Static } ;
   static IpConfigStatus m_ip_config_status ;
   static IpConfig m_ip_config ;

   static ConnectionState m_connection_state ;
   static EncryptionType m_encryption_type ;

   static bool m_scan_in_progress ;

   typedef aux::List<ScanInfo> ScanResults ;
   static ScanResults m_scan_results ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace lan
