////////////////////////////////////////////////////////////////////////////////////////////
//
// lan-socket.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/function.h"
#include "hw/rak439/sock.h"
#include <stdint.h>

namespace lan
{

////////////////////////////////////////////////////////////////////////////////////////////

#define htonl(A)    ((((uint32_t)(A) & 0xff000000) >> 24) | \
                     (((uint32_t)(A) & 0x00ff0000) >> 8) | \
                     (((uint32_t)(A) & 0x0000ff00) << 8) | \
                     (((uint32_t)(A) & 0x000000ff) << 24))

#define ntohl                   htonl

//Use in case of Big Endian only
#define htons(A)     ((((uint16_t)(A) & 0xff00) >> 8) | \
                      (((uint16_t)(A) & 0x00ff) << 8))


#define ntohs                   htons

////////////////////////////////////////////////////////////////////////////////////////////

using Address = hw::rak439::SockAddr ;
class Service ;

////////////////////////////////////////////////////////////////////////////////////////////

class Socket
{
public:
   using Type = hw::rak439::SockType ;
   using RxStream = hw::rak439::RxStream ;
   using ReceiveData = hw::rak439::Sock::ReceiveData ;

private:
   friend class Service ;

   Socket (const Socket&) = delete ;
   int operator= (const Socket&) = delete ;

   Socket (Type type, uint32_t sock) : m_type{ type }, m_sock{ sock } {}

public:
   Socket (Type type) ;
   Socket (void) ;
   ~Socket (void) ;

   Socket (Socket&& rhs) ;
   Socket& operator= (Socket&& rhs) ;

   bool is_valid (void) const { return m_sock != 0 && m_sock != (uint32_t)-1 ; }
   Type type (void) const { return m_type ; }

   Socket accept (void) ;

   void open (Type type) ;
   bool bind (uint16_t port) ;
   bool listen (uint8_t backlog) ;
   bool connect (const Address& addr) ;
   void close (void) ;

   void send (const void *buf, uint16_t len) ;
   void sendto (const void *buf, uint16_t len, const Address& addr) ;

   uint32_t raw_handle (void) const { return m_sock ; }

   typedef aux::function<void(void)> GenericCallback ;
   void on_incoming (GenericCallback&& cb) { m_on_incoming = aux::forward (cb) ; }
   void on_close (GenericCallback&& cb) { m_on_close = aux::forward (cb) ; }

   typedef aux::function<void (const ReceiveData&)> ReceiveCallback ;
   void on_receive (ReceiveCallback&& cb) { m_on_receive = aux::forward (cb) ; }

private:
   void handle_incoming_connection (void) ;
   void handle_close (void) ;
   void handle_receive (const hw::rak439::Sock::ReceiveData& rcv) ;

private:
   Type m_type ;
   uint32_t m_sock ;

   GenericCallback m_on_incoming,
                   m_on_close ;

   ReceiveCallback m_on_receive ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace lan
