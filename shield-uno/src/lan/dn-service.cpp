////////////////////////////////////////////////////////////////////////////////////////////
//
// dn-service.cpp
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "dn-service.h"
#include "aux/ll-scheduler.h"
#include "lan/lan-service.h"
#include "hw/rak439/rxstream.h"

namespace lan
{

////////////////////////////////////////////////////////////////////////////////////////////

DnService::Queue DnService::m_queue ;
bool DnService::m_ready ;
uint32_t DnService::m_result ;
Socket *DnService::m_sock ;

uint16_t DnService::request (aux::String name, Callback&& cb)
{
   bool first = m_queue.empty() ;
   auto itnew = m_queue.insert (m_queue.end(), { name, aux::forward (cb) }) ;

   if (first)
      aux::LowLatencySched::schedule (_handle_pendings) ;

   return itnew->m_id ;
}

void DnService::cancel (uint16_t request_id)
{
   auto it = m_queue.find_if ([request_id] (auto& req) { return req.m_id == request_id ; }) ;
   if (it == m_queue.end())
      return ;

   if (it != m_queue.begin())
      m_queue.erase (it) ;
   else if (!m_ready)
   {
      m_ready = true ;
      aux::LowLatencySched::schedule (_deliver_reply) ;
   }

}

namespace
{
   static constexpr uint16_t DNS_TRNS_ID = htons (0x56) ;

   struct RequestHdr
   {
      uint16_t id = DNS_TRNS_ID ;
      uint8_t flags1 = 0x01,
              flags2 = 0 ;
      uint16_t numquestions = htons (1) ;
      uint16_t numanswers = 0 ;
      uint16_t numauthrr = 0 ;
      uint16_t numextrarr = 0 ;
   } ;
}

void DnService::_handle_pendings (void)
{
   if (m_queue.empty())
      return ;

   m_result = 0 ;

   lan::Address addr{ lan::Service::ip_config().m_dns, 53 } ;
   if (!lan::Service::is_connected() || !addr.m_addr)
   {
      _deliver_reply() ;
      return ;
   }

   m_sock = new Socket{ Socket::Type::Udp } ;
   m_sock->connect (addr) ;

   const auto& req = m_queue.front() ;
   const auto namelen = req.m_name.length() ;

   static const uint8_t s_endquery[] PROGMEM { 0, 0, 1, 0, 1 } ;

   const auto pktsize = sizeof(RequestHdr) + namelen + sizeof (s_endquery) + 1 ;

   uint8_t buf[pktsize] ;

   *reinterpret_cast<RequestHdr*> (buf) = RequestHdr{} ;

   uint8_t *dst = buf + sizeof (RequestHdr) ;
   uint8_t *cnt = dst++ ;
   *cnt = 0 ;

   for (uint8_t i = 0; i < namelen; ++i,++dst)
      if ((*dst = req.m_name[i]) == '.')
      {
         cnt = dst ;
         *cnt = 0 ;
      }
      else
         ++*cnt ;

   memcpy_P (buf + sizeof (RequestHdr) + namelen + 1, s_endquery, sizeof (s_endquery)) ;

   m_sock->sendto (buf, pktsize, addr) ;

   m_sock->on_receive
      (
         [id = req.m_id] (auto& rcv)
         {
            if (m_queue.empty() || m_queue.front().m_id != id)
               return ;

            _handle_reply (rcv) ;

            if (m_ready)
               return ;

            m_ready = true ;
            aux::LowLatencySched::schedule (_deliver_reply) ;
         }
      ) ;
}

namespace
{
   void _skip_name (hw::rak439::RxStream& rx)
   {
      uint8_t len ;
      while ((len = rx.read()))
         rx.skip (len) ;
   }
}

void DnService::_handle_reply (const hw::rak439::Sock::ReceiveData& rcv)
{
   if (rcv.m_rx.unread() < sizeof (RequestHdr))
      return ;

   RequestHdr hdr = rcv.m_rx.read() ;
   if (hdr.id != DNS_TRNS_ID || hdr.flags2 & 0x0f)
      return ;

   _skip_name (rcv.m_rx) ;
   rcv.m_rx.skip (4) ;

   for (auto i = 0; i < ntohs (hdr.numanswers); ++i)
   {
      uint8_t namelen = rcv.m_rx.read() ;
      if (namelen & 0xc0)
         rcv.m_rx.skip (1) ;
      else
         _skip_name (rcv.m_rx) ;

      struct AnswerHdr
      {
          uint16_t type ;
          uint16_t uclass ;
          uint16_t ttl[2] ;
          uint16_t len ;
      } ;

      AnswerHdr h = rcv.m_rx.read() ;
      if (h.type == htons (1) && h.uclass == htons (1) && h.len == htons (sizeof (uint32_t)))
      {
         m_result = rcv.m_rx.read() ;
         m_result = ntohl (m_result) ;

         break ;
      }

      rcv.m_rx.skip (ntohs (h.len)) ;
   }
}

void DnService::_deliver_reply (void)
{
   if (!m_ready || m_queue.empty())
      return ;

   m_ready = false ;
   delete m_sock ;

   auto& request = m_queue.front() ;

   request.m_cb (request.m_id, m_result) ;
   m_queue.erase (m_queue.begin()) ;

   if (!m_queue.empty())
      aux::LowLatencySched::schedule (_handle_pendings) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

uint16_t DnService::Request::_generate_id (void)
{
   static uint16_t s_id = 0 ;
   return ++s_id ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace lan
