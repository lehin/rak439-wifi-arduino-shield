////////////////////////////////////////////////////////////////////////////////////////////
//
// lan-service.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "lan-service.h"
#include "lan-socket.h"
#include "aux/scheduler.h"
#include "aux/ll-scheduler.h"
#include "aux/minmax.h"
#include "hw/stat.h"
#include "hw/watchdog.h"

#include "hw/rak439/hci.h"
#include "hw/rak439/wmi.h"
#include "hw/rak439/wmi-commands.h"
#include "hw/rak439/wmi-events.h"

#include "util/eeprom.h"
#include "util/uart-dbg.h"

namespace lan
{

////////////////////////////////////////////////////////////////////////////////////////////

Service::State Service::m_state ;
Service::Sockets Service::m_sockets ;

uint8_t Service::m_mac[6] ;
uint8_t Service::m_bssid[6] ;
char Service::m_ssid[MAX_SSID_LEN] ;
char Service::m_passwd[MAX_PSK_LEN] ;
Service::IpConfigStatus Service::m_ip_config_status ;
Service::IpConfig Service::m_ip_config ;

Service::ConnectionState Service::m_connection_state ;
Service::EncryptionType Service::m_encryption_type ;

bool Service::m_scan_in_progress ;
Service::ScanResults Service::m_scan_results ;

void Service::ip_config (const IpConfig& ipcfg)
{
   m_ip_config = ipcfg ;

   if (_set_state (State::Connected, [] { return m_state == State::Configured ; }))
      _static_ip() ;
   else
      m_ip_config_status = IpConfigStatus::NeedStatic ;
}

void Service::initialize (void)
{
   if (util::UartDbg::enabled())
      util::UartDbg{} << util::pstr{} << PSTR("RAK439 initializing...") << util::endl{} ;

   _initialize_wlan() ;
   hw::rak439::Hci::initialize() ;
   _set_state (State::Initialized) ;
}

void Service::shutdown (void)
{
   if (!_set_state (State::Initialized, [] { return m_state > State::Initialized ; }))
      return ;

   m_connection_state = ConnectionState::Idle ;
   m_scan_in_progress = false ;

   hw::rak439::Hci::shutdown() ;
}

const uint8_t* Service::mac (void)
{
   _check_hw_ready() ;
   return m_mac ;
}

void Service::connect_unecrypted (const aux::StringRef& ssid)
{
   _check_hw_ready() ;

   if (!_set_ssid (ssid))
      return ;

   if (util::UartDbg::enabled())
      util::UartDbg{} << util::pstr{} << PSTR("Connecting unecrypted...") << util::endl{} ;

   m_encryption_type = EncryptionType::Unencrypted ;

   hw::rak439::Wmi::command (hw::rak439::cmds::ConnectOpen{ m_ssid }) ;
}

void Service::connect_wpa (const aux::StringRef& ssid, const aux::StringRef& passwd)
{
   _check_hw_ready() ;

   if (!_set_ssid (ssid))
      return ;

   if (passwd.empty() || !*passwd.begin())
   {
      m_connection_state = ConnectionState::ConnectFailed ;
      return ;
   }

   memset (m_passwd, 0, sizeof (m_passwd)) ;
   memcpy (m_passwd, passwd.begin(), passwd.size()) ;

   if (util::UartDbg::enabled())
      util::UartDbg{} << util::pstr{} << PSTR("Connecting WPA...") << util::endl{} ;

   m_encryption_type = EncryptionType::Wpa ;

   hw::rak439::Wmi::command (hw::rak439::cmds::SetPassword{ m_ssid, m_passwd }) ;
   hw::rak439::Wmi::command (hw::rak439::cmds::ConnectWpa2{ m_ssid }) ;
}

void Service::disconnect (void)
{
   _check_hw_ready() ;

   if (util::UartDbg::enabled())
      util::UartDbg{} << util::pstr{} << PSTR("Diconnecting...") << util::endl{} ;

   hw::rak439::Wmi::command (hw::rak439::cmds::Disconnect{}) ;
}

void Service::scan_start (void)
{
   _check_hw_ready() ;

   if (util::UartDbg::enabled())
      util::UartDbg{} << util::pstr{} << PSTR("Starting networks scan...") << util::endl{} ;

   m_scan_in_progress = true ;
   hw::rak439::Wmi::command (hw::rak439::cmds::BssFilter{}) ;
   hw::rak439::Wmi::command (hw::rak439::cmds::Scan{}) ;
}

uint8_t Service::scan_get_count (void)
{
   aux::wait ([] { return !m_scan_in_progress ; }) ;
   return m_scan_results.size() ;
}

void Service::register_socket (Socket *s)
{
   m_sockets.push_back (s) ;
}

void Service::unregister_socket (Socket *s)
{
   m_sockets.erase (m_sockets.find (s)) ;
}

void Service::_check_hw_ready (void)
{
   aux::wait ([] { return m_state >= State::HwReady ; }) ;
   m_scan_results.clear() ;
}

bool Service::_set_ssid (const aux::StringRef& ssid)
{
   if (ssid.empty() || !*ssid.begin())
      return (m_connection_state = ConnectionState::ConnectFailed, false) ;

   memset (m_ssid, 0, sizeof (m_ssid)) ;
   memcpy (m_ssid, ssid.begin(), ssid.size()) ;

   return true ;
}

void Service::_initialize_wlan (void)
{
   if (!_set_state (State::Initialized, [] { return m_state < State::Initialized ; }))
      return ;

   hw::rak439::Wmi::on_event<hw::rak439::evts::Ready>
      (
         [] (const auto& evt)
         {
            memcpy (m_mac, evt.m_macaddr, sizeof (m_mac)) ;
            aux::LowLatencySched::schedule (_on_wmi_ready) ;
         }
      ) ;

   hw::rak439::Wmi::on_event<hw::rak439::evts::Connect>
      (
         [] (const auto& evt)
         {
            memcpy (m_bssid, evt.m_bssid, sizeof (m_bssid)) ;
            aux::LowLatencySched::schedule (_on_connected) ;
         }
      ) ;

   hw::rak439::Wmi::on_event<hw::rak439::evts::Disconnect>
      (
         [] (const auto&)
         {
            aux::LowLatencySched::schedule
               (
                  []
                  {
                     if (util::UartDbg::enabled())
                        util::UartDbg{} << util::pstr{} << PSTR("RAK439 WLAN disconnected") << util::endl{} ;

                     _on_disconnected (State::HwReady) ;
                  }
               ) ;
         }
      ) ;

   hw::rak439::Wmi::on_event<hw::rak439::evts::ScanComplete>
      (
         [] (const auto& res)
         {
            if (util::UartDbg::enabled())
               util::UartDbg{} << util::pstr{} << PSTR("RAK439 Scan completed: ") << res.m_status << util::endl{} ;

            m_scan_in_progress = false ;
         }
      ) ;
   hw::rak439::Wmi::on_event<hw::rak439::evts::BssInfo> (_on_bss_info_event) ;

   hw::rak439::Sock::set_ip_callback
      (
         [] (bool ok)
         {
            if (!ok)
               return ;

            aux::LowLatencySched::schedule (_on_configured) ;
         }
      ) ;

   hw::rak439::Sock::listen_callback
      (
         [] (uint32_t sock)
         {
            static uint32_t _sock ;
            _sock = sock ;
            aux::LowLatencySched::schedule ([] { _on_incoming_connection (_sock) ; }) ;
         }
      ) ;
   hw::rak439::Sock::close_callback (_on_sock_close) ;
   hw::rak439::Sock::receive_callback (_on_sock_receive) ;
}

namespace
{
   void _normalize_eep_struct (uint8_t *data, size_t size)
   {
      for (; size; ++data, --size)
         if (*data == 255)
            *data = 0 ;
   }

   template<typename _Tp> inline
      void _normalize_eep_struct (_Tp& p)
         { _normalize_eep_struct (reinterpret_cast<uint8_t*> (&p), sizeof (_Tp)) ; }
}

void Service::_on_wmi_ready (void)
{
   if (util::UartDbg::enabled())
      util::UartDbg{} << util::pstr{} << PSTR("RAK439 WMI ready") << util::endl{} ;

   hw::rak439::Sock::init_stack_offload() ;

   _set_state (State::HwReady) ;
}

void Service::_on_connected (void)
{
   if (util::UartDbg::enabled())
      util::UartDbg{} << util::pstr{} << PSTR("RAK439 WLAN connected") << util::endl{} ;

   _on_disconnected (State::Connected) ;
   _set_state (State::Connected) ;

   m_connection_state = ConnectionState::Connected ;

   switch (m_ip_config_status)
   {
      case IpConfigStatus::NeedDhcp:
      case IpConfigStatus::WaitDhcp:
      case IpConfigStatus::Dhcp:
         _dhcp_query() ;
         break ;

      case IpConfigStatus::NeedStatic:
      case IpConfigStatus::WaitStatic:
      case IpConfigStatus::Static:
         _static_ip() ;
         break ;
   }
}

void Service::_on_disconnected (State dst_state)
{
   if (!_set_state (dst_state, [dst_state] { return m_state > dst_state ; }))
      return ;

   m_connection_state = ConnectionState::Disconnected ;
}

void Service::_on_configured (void)
{
   if (!_set_state (State::Configured))
      return ;

   switch (m_ip_config_status)
   {
      case IpConfigStatus::WaitDhcp:
      {
         hw::rak439::IpConfigResult ip ;
         if (hw::rak439::Sock::ip_config_query (ip) != -1)
            m_ip_config = { ip.m_ipv4, ip.m_subnet_mask, ip.m_gateway4, ip.m_dnsrv1 } ;

         m_ip_config_status = IpConfigStatus::Dhcp ;
         break ;
      }

      case IpConfigStatus::WaitStatic:
         m_ip_config_status = IpConfigStatus::Static ;
         break ;

      case IpConfigStatus::NeedDhcp:
         _set_state (State::Connected) ;
         _dhcp_query() ;
         return ;

      case IpConfigStatus::NeedStatic:
         _set_state (State::Connected) ;
         _static_ip() ;
         return ;

      case IpConfigStatus::Static:
      case IpConfigStatus::Dhcp:
         break ;
   }

   if (util::UartDbg::enabled())
   {
      char buf[16] ;

      util::UartDbg{}
         << util::pstr{} << PSTR("IP: ") << aux::range (buf + 0, util::iptostr (m_ip_config.m_addr, buf))
         << util::pstr{} << PSTR(" / ") << aux::range (buf + 0, util::iptostr (m_ip_config.m_mask, buf)) << util::endl{}
         << util::pstr{} << PSTR("GW: ") << aux::range (buf + 0, util::iptostr (m_ip_config.m_gateway, buf)) << util::endl{}
         << util::pstr{} << PSTR("DNS: ") << aux::range (buf + 0, util::iptostr (m_ip_config.m_dns, buf)) << util::endl{} ;
   }
}

void Service::_on_incoming_connection (uint32_t sock)
{
   for (auto s : m_sockets)
      if (sock == s->raw_handle())
      {
         s->handle_incoming_connection() ;
         break ;
      }
}

void Service::_on_sock_close (uint32_t sock)
{
   for (auto s : m_sockets)
      if (sock == s->raw_handle())
      {
         s->handle_close () ;
         break ;
      }
}

void Service::_on_sock_receive (const ReceiveData& rcv)
{
   for (auto s : m_sockets)
      if (rcv.m_sock == s->raw_handle())
      {
         s->handle_receive (rcv) ;
         break ;
      }
}

void Service::_dhcp_query (void)
{
   if (util::UartDbg::enabled())
      util::UartDbg{} << util::pstr{} << PSTR("Querying DHCP...") << util::endl{} ;

   m_ip_config_status = IpConfigStatus::WaitDhcp ;

   aux::String hostname = *util::Eeprom::host_name() ;
   if (hostname.empty())
      hostname = aux::String::from_progmem (PSTR("rak439")) ;

   hw::rak439::Sock::ip_config_dhcp (hostname.c_str()) ;
}

void Service::_static_ip (void)
{
   if (util::UartDbg::enabled())
      util::UartDbg{} << util::pstr{} << PSTR("Setting IP...") << util::endl{} ;

   m_ip_config_status = IpConfigStatus::WaitStatic ;
   hw::rak439::Sock::ip_config_static (m_ip_config.m_addr, m_ip_config.m_mask, m_ip_config.m_gateway) ;
}

bool Service::_set_state (State state)
{
   if (m_state == state)
      return false ;

   m_state = state ;

   switch (m_state)
   {
      case State::Undefined:
      case State::Initialized:
         hw::Stat::off() ;
         break ;

      case State::HwReady:
         hw::Stat::blink (50, 950) ;
         break ;

      case State::Connected:
         hw::Stat::blink (50, 150) ;
         break ;

      case State::Configured:
         hw::Stat::blink (50, 2950) ;
         break ;
   }

   static char const _undef[] PROGMEM = "UNDEFINED" ;
   static char const _init[] PROGMEM = "INITIALIZED" ;
   static char const _hwready[] PROGMEM = "HWREADY" ;
   static char const _connected[] PROGMEM = "CONNECTED" ;
   static char const _configured[] PROGMEM = "CONFIGURED" ;
   static const char* const _status[] PROGMEM = { _undef, _init, _hwready, _connected, _configured } ;

   if (util::UartDbg::enabled())
      util::UartDbg{}
         << util::pstr{} << PSTR("WLAN state: ")
         << util::pstr{} << (const char*)pgm_read_ptr (_status + (uint8_t)m_state) << util::endl{} ;

   return true ;
}

void Service::_on_bss_info_event (const hw::rak439::evts::BssInfo& bi)
{
   if (util::UartDbg::enabled())
   {
      char mac[17] ;
      util::UartDbg{}
         << util::pstr{} << PSTR("RAK439 BSS info: ") << bi.m_ssid.c_str()
         << util::pstr{} << PSTR(" / ") << aux::range (mac + 0, util::mactostr (bi.m_hdr.m_bssid, mac)) << util::endl{} ;
   }

   if (bi.m_ssid.empty())
      return ;

   ScanResults::iterator it = m_scan_results.begin() ;
   for ( ; it != m_scan_results.end(); ++it)
      if (it->m_ssid == bi.m_ssid)
         break ;

   if (it == m_scan_results.end())
      it = m_scan_results.insert (m_scan_results.end(), { bi.m_ssid }) ;

   auto rssi = 95 - bi.m_hdr.m_snr ;
   if (it->m_rssi < rssi)
      return ;

   it->m_rssi = rssi ;
   it->m_encryption = EncryptionType::Unencrypted ;

   using hw::rak439::CryptoType ;
   using hw::rak439::AuthMode ;

   if ((bi.m_rsn_cipher & CryptoType::Wep) && (bi.m_rsn_auth & AuthMode::None))
      it->m_encryption = EncryptionType::Wep ;

   if (
         (
            bi.m_rsn_cipher & CryptoType::Tkip ||
            bi.m_rsn_cipher & CryptoType::Aes
         ) &&
         (
            bi.m_rsn_auth & AuthMode::Wpa2Psk ||
            bi.m_rsn_auth & AuthMode::WpaPsk
         )
      )
      it->m_encryption = EncryptionType::Wpa ;

   if (
         (
            bi.m_wpa_cipher & CryptoType::Tkip ||
            bi.m_wpa_cipher & CryptoType::Aes
         ) &&
         (
            bi.m_wpa_auth & AuthMode::Wpa2Psk ||
            bi.m_wpa_auth & AuthMode::WpaPsk
         )
      )
      it->m_encryption = EncryptionType::Wpa ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace lan
