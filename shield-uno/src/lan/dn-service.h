////////////////////////////////////////////////////////////////////////////////////////////
//
// dn-service.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "lan-socket.h"
#include "aux/string.h"
#include "aux/list.h"
#include "aux/forward.h"
#include "aux/function.h"

namespace lan
{

////////////////////////////////////////////////////////////////////////////////////////////

class DnService
{
public:
   typedef aux::function<void (uint16_t id, uint32_t addr)> Callback ;
   static uint16_t request (aux::String name, Callback&& cb) ;
   static void cancel (uint16_t request_id) ;

private:
   static void _handle_pendings (void) ;
   static void _handle_reply (const hw::rak439::Sock::ReceiveData& rcv) ;
   static void _deliver_reply (void) ;

private:
   struct Request
   {
      aux::String m_name ;
      Callback m_cb ;
      uint16_t m_id ;

      Request (aux::String name = {}, Callback&& cb = {}) :
         m_name{ name }, m_cb{ aux::forward (cb) }, m_id{ _generate_id() }
      {
      }

      static uint16_t _generate_id (void) ;
   } ;

   typedef aux::List<Request> Queue ;

   static Queue m_queue ;

   static uint32_t m_result ;
   static bool m_ready ;
   static Socket *m_sock ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace lan
