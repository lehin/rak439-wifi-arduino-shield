////////////////////////////////////////////////////////////////////////////////////////////
//
// stat.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "stat.h"
#include "aux/scheduler.h"
#include <util/atomic.h>

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

uint16_t Stat::m_schedule = 0 ;
uint16_t Stat::m_on_time = 0,
         Stat::m_off_time = 0 ;

void Stat::blink (uint16_t on, uint16_t off)
{
   _cancel() ;

   m_on_time = on ;
   m_off_time = off ;

   _switch() ;
}

void Stat::_cancel (void)
{
   if (!m_schedule)
      return ;

   aux::Scheduler::cancel (m_schedule) ;
   m_schedule = m_on_time = m_off_time = 0 ;
}

void Stat::_switch (void)
{
   if (!m_on_time || !m_off_time)
      return ;

   ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
      gpio::Stat::turn() ;

   m_schedule = aux::Scheduler::schedule (_switch, gpio::Stat::stat() ? m_on_time : m_off_time) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
