////////////////////////////////////////////////////////////////////////////////////////////
//
// uart.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/singleton.h"
#include "aux/ring-buffer.h"
#include "aux/range.h"
#include "aux/buf-guard.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

ISR (USART0_RX_vect) ;
ISR (USART0_UDRE_vect) ;

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

class Uart
{
   SINGLETON_DECL(Uart) ;

   static constexpr uint8_t RECBUFSIZE = 8 ;
   static constexpr uint8_t SNDBUFSIZE = 16 ;

   typedef void (*EvtHandler) (void) ;
   typedef aux::RingBuffer<RECBUFSIZE,uint8_t> ReceiveBuffer ;
   typedef aux::RingBuffer<SNDBUFSIZE,uint8_t> SendBuffer ;

public:
   typedef aux::BufferGuard<ReceiveBuffer> RecBufAccessor ;

public:
   Uart (void) ;

   static void send (uint8_t byte) { _push (byte) ; }

   static void send_str (const char *s) ;
   static void send_pstr (const char *s) ;

   template<typename _Tp>
      static void send (const _Tp& data)
      {
         send_range (aux::range ((const uint8_t*)&data, (const uint8_t*)(&data + 1))) ;
      }

   template<typename _Tp, typename... _Args>
      static void send_multiple (const _Tp& data, const _Args& ... args)
      {
         send (data) ;
         send_multiple (args...) ;
      }

   template<typename _Range>
      static void send_range (const _Range& range)
      {
         for (auto& val : range)
            send (val) ;
      }

   static void receive_handler (EvtHandler handler)
   {
      m_receive_handler = handler ;
   }

   static RecBufAccessor buffer (void) { return m_recbuf ; }

   static void flush (void) ;

private:
   static void send_multiple (void) {}
   static void _push (uint8_t byte) ;

   friend void ::USART0_RX_vect (void) ;
   friend void ::USART0_UDRE_vect (void) ;

   static void _handle_received (void) ;
   static void _on_receive (void) ;
   static void _on_buffer_empty (void) ;

private:
   static ReceiveBuffer m_recbuf ;
   static SendBuffer m_sendbuf ;
   static EvtHandler m_receive_handler ;
   static volatile bool m_transmitter_active ;
   static volatile bool m_receive_handle_scheduled ;
   static volatile bool m_data_received ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
