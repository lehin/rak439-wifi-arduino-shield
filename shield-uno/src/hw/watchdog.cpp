////////////////////////////////////////////////////////////////////////////////////////////
//
// watchdog.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "watchdog.h"
#include "uart.h"
#include "aux/ll-scheduler.h"
#include <avr/interrupt.h>
#include <avr/wdt.h>

////////////////////////////////////////////////////////////////////////////////////////////

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

void Wdt::enable (void)
{
   wdt_enable (WDTO_4S) ;
}

void Wdt::disable (void)
{
   wdt_disable() ;
}

void Wdt::reset (void)
{
   wdt_reset() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

void reboot (void)
{
   hw::Uart::send_pstr (PSTR("Rebooting...\r\n")) ;
   hw::Uart::flush() ;

   wdt_enable (WDTO_15MS) ;

   cli() ;
   while (true) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
