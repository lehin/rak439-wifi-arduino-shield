////////////////////////////////////////////////////////////////////////////////////////////
//
// uart.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "uart.h"
#include "aux/ll-scheduler.h"
#include <avr/power.h>
#include <avr/pgmspace.h>

#define BAUDRATE 115200

////////////////////////////////////////////////////////////////////////////////////////////

ISR (USART0_RX_vect)
{
   hw::Uart::_on_receive() ;
}

ISR (USART0_UDRE_vect)
{
   hw::Uart::_on_buffer_empty() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

SINGLETON_IMPL(Uart) ;

Uart::ReceiveBuffer Uart::m_recbuf ;
Uart::SendBuffer Uart::m_sendbuf ;
Uart::EvtHandler Uart::m_receive_handler = nullptr ;

volatile bool Uart::m_transmitter_active = false ;
volatile bool Uart::m_receive_handle_scheduled = false ;
volatile bool Uart::m_data_received = false ;

Uart::Uart (void)
{
   UBRR0 = F_CPU / BAUDRATE / 8 - 1 ;
   UCSR0A = _BV(U2X0) ;
   UCSR0B = _BV(RXEN0) | _BV(TXEN0) | _BV(RXCIE0) ;
   UCSR0C = _BV(UCSZ00) | _BV(UCSZ01) ;
}

void Uart::send_str (const char *s)
{
   while (*s)
      send (*s++) ;
}

void Uart::send_pstr (const char *s)
{
   while (char ch = pgm_read_byte (s++))
      send (ch) ;
}

void Uart::flush (void)
{
   while (m_transmitter_active) ;
}

void Uart::_push (uint8_t byte)
{
   bool full ;
   do
   {
      ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
         full = m_sendbuf.full() ;

   } while (full) ;

   ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
   {
      if (m_transmitter_active)
         m_sendbuf.push_back (byte) ;
      else
      {
         m_transmitter_active = true ;
         UDR0 = byte ;
         UCSR0B |= _BV(UDRIE0) ;
      }
   }
}

void Uart::_handle_received (void)
{
   ATOMIC_BLOCK (ATOMIC_FORCEON)
   {
      while (m_data_received)
      {
         m_data_received = false ;

         sei() ;

         if (m_receive_handler)
            m_receive_handler() ;

         cli() ;
      } ;

      m_receive_handle_scheduled = false ;
   }
}

inline
   void __attribute__ ((always_inline)) Uart::_on_receive (void)
   {
      while ((UCSR0A & _BV(RXC0)) != 0)
      {
         uint8_t byte = UDR0 ;
         m_recbuf.push_back (byte) ;
         m_data_received = true ;
      }

      if (!m_receive_handle_scheduled)
      {
         m_receive_handle_scheduled = true ;
         aux::LowLatencySched::schedule (&Uart::_handle_received) ;
      }
   }

inline
   void __attribute__ ((always_inline)) Uart::_on_buffer_empty (void)
   {
      if (m_sendbuf.empty())
      {
         m_transmitter_active = false ;
         UCSR0B &= ~_BV(UDRIE0) ;
      }
      else
         UDR0 = m_sendbuf.pop_front() ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
