////////////////////////////////////////////////////////////////////////////////////////////
//
// spi-slave.cpp
//
// Copyright © 2018 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "spi-slave.h"
#include "aux/ll-scheduler.h"

#include "gpio.h"
#include <string.h>

////////////////////////////////////////////////////////////////////////////////////////////

ISR (SPI_STC_vect)
{
   hw::SpiSlave::_on_interrupt() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

SpiSlave::PktHandler SpiSlave::m_pkt_handler ;
SpiSlave::Buffer SpiSlave::m_buffer ;
SpiSlave::State SpiSlave::m_state = State::Idle ;
bool SpiSlave::m_parm_len_16bit ;
uint8_t SpiSlave::m_parm_cnt ;
uint16_t SpiSlave::m_parm_len ;
SpiSlave::Buffer::iterator SpiSlave::m_send_it ;

const SpiSlave::ByteHandler SpiSlave::m_handlers[] =
{
   _on_byte_idle,
   _on_byte_cmd,
   _on_byte_nparm,
   _on_byte_parmlen_16high,
   _on_byte_parmlen_16low,
   _on_byte_parmlen_8,
   _on_byte_parmdata,
   _on_byte_end,
   _on_byte_send
} ;

void SpiSlave::initialize (PktHandler fn)
{
   m_pkt_handler = fn ;

   OCR2A = 195 ;
   TCCR2A = _BV(WGM21) ;
   TCCR2B = _BV(CS20) | _BV(CS21) | _BV(CS22) ;

   SPCR = _BV(SPIE) | _BV(SPE) ;

   _set_ready() ;
}

SpiSlave::Tx SpiSlave::tx (void)
{
   return Tx{ &m_buffer } ;
}

void SpiSlave::send (void)
{
   m_send_it = m_buffer.begin() ;
   m_state = State::Send ;
}

inline __attribute__ ((always_inline))
   void SpiSlave::_on_interrupt (void)
   {
      TCNT2 = 0 ;
      TIFR2 = _BV(OCF2A) ;

      while (true)
      {
         uint8_t byte = SPDR ;

         if (m_handlers[(uint8_t)m_state] (byte))
            m_buffer.push_back (byte) ;

         if (m_state == State::Idle)
            break ;

         TCNT2 = 0 ;
         while (!(SPSR & _BV(SPIF)))
            if ((TIFR2 & _BV(OCF2A)))
            {
               m_state = State::Idle ;
               _set_ready() ;
               break ;
            }
      } ;
   }

bool SpiSlave::_on_byte_idle (uint8_t byte)
{
   if (byte != 0xE0)
      return false ;

   m_state = State::Cmd ;

   hw::gpio::Act::set() ;
   hw::gpio::Int::set() ;

   m_buffer.clear() ;

   return false ;
}

bool SpiSlave::_on_byte_cmd (uint8_t byte)
{
   m_state = State::Nparm ;
   m_parm_len_16bit = (byte & 0x40) != 0 ;
   return true ;
}

SpiSlave::State SpiSlave::_parmlen_state (void)
{
   return m_parm_len_16bit ? State::ParmLen16High : State::ParmLen8 ;
}

bool SpiSlave::_on_byte_nparm (uint8_t byte)
{
   m_parm_cnt = byte ;
   m_state = m_parm_cnt ? _parmlen_state() : State::End ;
   return true ;
}

bool SpiSlave::_on_byte_parmlen_16high (uint8_t byte)
{
   m_parm_len = (uint16_t)byte << 8 ;
   m_state = State::ParmLen16Low ;
   return true ;
}

bool SpiSlave::_on_byte_parmlen_16low (uint8_t byte)
{
   if ((m_parm_len |= byte))
      m_state = State::ParmData ;
   else
      m_state = --m_parm_cnt ? _parmlen_state() : State::End ;

   return true ;
}

bool SpiSlave::_on_byte_parmlen_8 (uint8_t byte)
{
   if ((m_parm_len = byte))
      m_state = State::ParmData ;
   else
      m_state = --m_parm_cnt ? _parmlen_state() : State::End ;

   return true ;
}

bool SpiSlave::_on_byte_parmdata (uint8_t byte)
{
   if (--m_parm_len)
      return true ;

   m_state = --m_parm_cnt ? _parmlen_state() : State::End ;

   return true ;
}

bool SpiSlave::_on_byte_end (uint8_t byte)
{
   if (byte != 0xEE)
      return false ;

   aux::LowLatencySched::schedule
      (
         []
         {
            if (m_pkt_handler)
               m_pkt_handler (aux::range (m_buffer.begin(), m_buffer.end())) ;

            _set_ready() ;
         }
      ) ;

   m_state = State::Idle ;

   return false ;
}

bool SpiSlave::_on_byte_send (uint8_t)
{
   hw::gpio::Act::set() ;

   SPDR = *m_send_it ;

   if (++m_send_it != m_buffer.end())
      return false ;

   m_state = State::Idle ;
   hw::gpio::Act::reset() ;

   return false ;
}

void SpiSlave::_set_ready (void)
{
   hw::gpio::Int::reset() ;
   hw::gpio::Act::reset() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

SpiSlave::Tx::Tx (Buffer* buf) :
   m_buf { buf }
{
   m_buf->clear() ;
   m_buf->push_back (0xE0) ;
}

SpiSlave::Tx::~Tx (void)
{
   if (!m_buf)
      return ;

   m_buf->push_back (0xEE) ;
   SpiSlave::send() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
