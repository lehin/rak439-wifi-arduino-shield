////////////////////////////////////////////////////////////////////////////////////////////
//
// spi-slave.h
//
// Copyright © 2018 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/array.h"
#include "aux/range.h"
#include "aux/debug-assert.h"
#include <avr/interrupt.h>

ISR (SPI_STC_vect) ;

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

class SpiSlave
{
   typedef uint16_t BUFFER_SIZE_TYPE ;
   static constexpr BUFFER_SIZE_TYPE BUFFER_SIZE = XRAMEND > 0x4000 ? 2560 : (XRAMEND > 0x2000 ? 1536 : 512) ;

   typedef aux::ArrayImpl<uint8_t, BUFFER_SIZE_TYPE, BUFFER_SIZE> Buffer ;

   enum class State : uint8_t{ Idle, Cmd, Nparm, ParmLen16High, ParmLen16Low, ParmLen8, ParmData, End, Send } ;

public:
   typedef aux::Range<const uint8_t*> Received ;
   typedef void (*PktHandler) (Received pkt) ;

   static void initialize (PktHandler fn) ;

   class Tx ;
   static Tx tx (void) ;
   static void send (void) ;

   static Buffer& recbuf (void) { return m_buffer ; }

private:
   friend void ::SPI_STC_vect (void) ;
   static void _on_interrupt (void) ;

   static bool _on_byte_idle (uint8_t byte) ;
   static bool _on_byte_cmd (uint8_t byte) ;
   static State _parmlen_state (void) ;
   static bool _on_byte_nparm (uint8_t byte) ;
   static bool _on_byte_parmlen_16high (uint8_t byte) ;
   static bool _on_byte_parmlen_16low (uint8_t byte) ;
   static bool _on_byte_parmlen_8 (uint8_t byte) ;
   static bool _on_byte_parmdata (uint8_t byte) ;
   static bool _on_byte_end (uint8_t byte) ;
   static bool _on_byte_send (uint8_t) ;

   static void _set_ready (void) ;

private:
   static PktHandler m_pkt_handler ;

   static Buffer m_buffer ;

   static State m_state ;
   static bool m_parm_len_16bit ;
   static uint8_t m_parm_cnt ;
   static uint16_t m_parm_len ;
   static Buffer::iterator m_send_it ;

   typedef bool (*ByteHandler) (uint8_t byte) ;
   static const ByteHandler m_handlers[] ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class SpiSlave::Tx
{
   friend class SpiSlave ;

   Tx (const Tx&) = delete ;
   Tx& operator= (const Tx&) = delete ;

   Tx (Buffer* buf) ;

public:
   ~Tx (void) ;

   Tx (Tx&& org) : m_buf{ org.m_buf } { org.m_buf = nullptr ; }
   Tx& operator= (Tx&& rhs)
   {
      m_buf = rhs.m_buf ;
      rhs.m_buf = nullptr ;
   }

   template<typename  _Tp>
      Tx& operator<< (const _Tp& p) ;

   template<typename  _It>
      Tx& operator<< (const aux::Range<_It>& r) ;

   template<typename _Tp, typename ... _Args>
      void write (_Tp& p, _Args& ... tail) ;

   void write (void) {}

private:
   Buffer* m_buf ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

namespace impl
{
   template<typename _Buf, typename _Tp, uint8_t _Sz>
      struct BufferPush
      {
         static void do_push (_Buf& buf, const _Tp& p)
         {
            aux::debug_assert( sizeof (_Tp) <= buf.capacity() - buf.size(), PSTR("SPI TX buf") ) ;

            reinterpret_cast<_Tp&> (*buf.end()) = p ;
            buf.resize (buf.size() + _Sz) ;
         }
      } ;

   template<typename _Buf, typename _Tp>
      struct BufferPush<_Buf,_Tp,1>
      {
         static void do_push (_Buf& buf, const _Tp& p)
         {
            aux::debug_assert( !buf.full(), PSTR("SPI TX buf") ) ;
            buf.push_back ((uint8_t)p) ;
         }
      } ;
}

////////////////////////////////////////////////////////////////////////////////////////////

template<typename  _Tp> inline
   SpiSlave::Tx& SpiSlave::Tx::operator<< (const _Tp& p)
   {
      impl::BufferPush<Buffer,_Tp,sizeof(_Tp)>::do_push (*m_buf, p) ;
      return *this ;
   }

template<typename  _It> inline
   SpiSlave::Tx& SpiSlave::Tx::operator<< (const aux::Range<_It>& r)
   {
      for (auto& item : r)
         operator<< (item) ;

      return *this ;
   }

template<typename _Tp, typename ... _Args> inline
   void SpiSlave::Tx::write (_Tp& p, _Args& ... tail)
   {
      operator<< (p) ;
      write (tail...) ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
