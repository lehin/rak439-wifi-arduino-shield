////////////////////////////////////////////////////////////////////////////////////////////
//
// gpio.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/singleton.h"
#include <avr/io.h>

namespace hw
{
   namespace gpio
   {

////////////////////////////////////////////////////////////////////////////////////////////

struct Init
{
   SINGLETON_DECL (Init) ;
   Init (void) ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

#define PORTDEF(name) \
   struct Portdef##name \
   { \
      static volatile uint8_t& in (void) { return PIN##name ; } \
      static volatile uint8_t& out (void) { return PORT##name ; } \
      static volatile uint8_t& ddr (void) { return DDR##name ; } \
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

enum class Dir { In, Out } ;

template<int _num, Dir _dir = Dir::In, int _init = 1>
   struct PinD
   {
      static constexpr uint8_t mask (void) { return _BV(_num) ; }
      static constexpr uint8_t ddr (void) { return _dir == Dir::Out ? mask() : 0 ; }
      static constexpr uint8_t init (void) { return _init ? mask() : 0 ; }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _PinD = void, typename ... _Tail>
   struct PortPins : PortPins<_Tail...>
   {
      typedef PortPins<_Tail...> base_class ;

      static constexpr uint8_t ddr (void)
      {
         return (base_class::ddr() & ~_PinD::mask()) | _PinD::ddr() ;
      }

      static constexpr uint8_t init (void)
      {
         return (base_class::init() & ~_PinD::mask()) | _PinD::init() ;
      }
   } ;

template<>
   struct PortPins<void>
   {
      static constexpr uint8_t ddr (void) { return 0 ; }
      static constexpr uint8_t init (void) { return 0xFF ; }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template< typename _PortDef, typename ... _PinsDef >
   class Port : public _PortDef
   {
   public:
      using _PortDef::in ;
      using _PortDef::out ;
      using _PortDef::ddr ;

      static void init (void)
      {
         out() = PortPins<_PinsDef...>::init() ;
         ddr() = PortPins<_PinsDef...>::ddr() ;
      }

      template<int _num>
         struct Pin
         {
            typedef Port Owner ;
            static constexpr int num = _num ;
            static constexpr uint8_t mask = _BV(num) ;

            template<int _set> __attribute__ ((always_inline))
               static void set (void)
               {
                  if (_set)
                     Owner::out() |= mask ;
                  else
                     Owner::out() &= ~mask ;
               }

            __attribute__ ((always_inline))
               static void set (void) { set<1>() ; }

            __attribute__ ((always_inline))
               static void reset (void) { set<0>() ; }

            __attribute__ ((always_inline))
               static void turn (void) { Owner::out() ^= _BV(num) ; }

            __attribute__ ((always_inline))
               static bool read (void) { return (Owner::in() & _BV(num)) != 0 ; }

            __attribute__ ((always_inline))
               static bool stat (void) { return (Owner::out() & _BV(num)) != 0 ; }

            __attribute__ ((always_inline))
               static void as_input (void) { Owner::ddr() &= ~PinD<num>::mask() ; }

            __attribute__ ((always_inline))
               static void as_output (void) { Owner::ddr() |= PinD<num>::mask() ; }
         } ;
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

PORTDEF (A) ;
PORTDEF (B) ;
PORTDEF (C) ;
PORTDEF (D) ;

struct PortA :
   Port
      <
         PortdefA,
         PinD<0, Dir::Out, 0>,
         PinD<1, Dir::Out, 0>,
         PinD<2, Dir::Out, 0>
      >
{
} ;

struct PortB :
   Port
      <
         PortdefB,
         PinD<0, Dir::Out, 1>,
         PinD<4, Dir::In, 1>,
         PinD<5, Dir::In, 1>,
         PinD<6, Dir::Out, 1>,
         PinD<7, Dir::In, 1>
      >
{
} ;

struct PortC :
   Port
      <
         PortdefC
      >
{
} ;

struct PortD :
   Port
      <
         PortdefD,
         PinD<0, Dir::In, 1>,
         PinD<1, Dir::Out, 1>,
         PinD<2, Dir::In, 0>,
         PinD<3, Dir::Out, 1>,
         PinD<4, Dir::Out, 1>,
         PinD<5, Dir::Out, 1>,
         PinD<6, Dir::Out, 1>,
         PinD<7, Dir::In, 0>
      >
{
} ;

////////////////////////////////////////////////////////////////////////////////////////////

typedef PortA::Pin<0> Stat ;
typedef PortA::Pin<1> Pwr ;
typedef PortA::Pin<2> Act ;

typedef PortB::Pin<0> Int ;
typedef PortB::Pin<4> Ss ;
typedef PortB::Pin<5> Mosi ;
typedef PortB::Pin<6> Miso ;
typedef PortB::Pin<7> Sck ;

typedef PortD::Pin<0> Rxd ;
typedef PortD::Pin<1> Txd ;
typedef PortD::Pin<2> Miso1 ;
typedef PortD::Pin<3> Mosi1 ;
typedef PortD::Pin<4> Sck1 ;
typedef PortD::Pin<5> Wlrst ;
typedef PortD::Pin<6> Wlcs ;
typedef PortD::Pin<7> Wirq ;

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace gpio
} // namespace hw
