////////////////////////////////////////////////////////////////////////////////////////////
//
// stat.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "gpio.h"

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

class Stat
{
public:
   static void on (bool on = true)
   {
      _cancel() ;
      _on_off (on) ;
   }

   static void off (void) { on (false) ; }

   static void blink (uint16_t on, uint16_t off) ;

private:
   static void _on_off (bool on)
   {
      if (on)
         gpio::Stat::set() ;
      else
         gpio::Stat::reset() ;
   }

   static void _cancel (void) ;
   static void _switch (void) ;

private:
   static uint16_t m_schedule ;
   static uint16_t m_on_time, m_off_time ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
