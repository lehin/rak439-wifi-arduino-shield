////////////////////////////////////////////////////////////////////////////////////////////
//
// gpio.cpp
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "gpio.h"

namespace hw
{
   namespace gpio
   {

////////////////////////////////////////////////////////////////////////////////////////////

SINGLETON_IMPL (Init) ;

Init::Init (void)
{
   PortA::init() ;
   PortB::init() ;
   PortC::init() ;
   PortD::init() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace gpio
} // namespace hw
