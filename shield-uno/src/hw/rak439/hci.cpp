////////////////////////////////////////////////////////////////////////////////////////////
//
// hci.cpp
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "hci.h"
#include "device.h"
#include "aux/scheduler.h"
#include "aux/ll-scheduler.h"
#include "hw/gpio.h"
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <string.h>

namespace hw
{
   namespace rak439
   {

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static constexpr uint8_t ENDPOINTS_COUNT = 3 ;
}

Hci::WmiPktHandler Hci::m_wmi_pkt_handler ;
Hci::DataPktHandler Hci::m_data_pkt_handler ;
Hci::Credit Hci::m_credits[ENDPOINTS_COUNT] ;
uint8_t Hci::m_total_credits ;
uint16_t Hci::m_write_buffer_space ;
bool Hci::m_request_is_pending ;

void Hci::initialize (void)
{
   Device::disable_interrupts() ;
   Device::reset ([] { aux::Scheduler::schedule (_init_impl, 2) ; }) ;
}

void Hci::shutdown (void)
{
   Device::disable_interrupts() ;

   static constexpr uint16_t SPI_RESET = (1 << 15) ;
   ConfigReg{} = SPI_RESET ;
}

namespace
{
   static constexpr uint16_t HCI_INTREG_FLAG = 1 << 14 ;
   static constexpr uint16_t HCI_READ_FLAG = 1 << 15 ;
}

void Hci::set_internal_register (uint16_t addr, uint16_t val)
{
   Device::Cs _cs ;

   Spi::send_multiple (Spi::msb{}, addr | HCI_INTREG_FLAG, val) ;
}

uint16_t Hci::get_internal_register (uint16_t addr)
{
   Device::Cs _cs ;

   Spi::send (Spi::msb{}, addr | HCI_INTREG_FLAG | HCI_READ_FLAG) ;

   uint16_t res ;
   Spi::receive (Spi::msb{}, res) ;

   return res ;
}

namespace
{
   static constexpr uint16_t SPI_HOST_CTRL_SIZE_FIXED_ADDR = 1 << 6 ;
   static constexpr uint16_t SPI_HOST_CTRL_CONFIG_ENABLE = 1 << 15 ;
   static constexpr uint16_t SPI_HOST_CTRL_CONFIG_DIR_WRITE = 1 << 14 ;
   static constexpr uint16_t SPI_STATUS_HOST_ACCESS_DONE = 1 << 0 ;
}

void Hci::external_read (uint16_t addr, bool incaddr, uint8_t size, void* buf)
{
   HostCtrlSizeReg{} = (uint16_t)size | (incaddr ? 0 : SPI_HOST_CTRL_SIZE_FIXED_ADDR) ;
   HostCtrlCfgReg{} = addr | SPI_HOST_CTRL_CONFIG_ENABLE ;

   for (uint8_t err = 0; !(StatusReg{} & SPI_STATUS_HOST_ACCESS_DONE); ++err)
   {
      aux::debug_assert( err < 200, PSTR("EXT READ") ) ;
      _delay_ms (1) ;
   }

   {
      Device::Cs _cs ;

      Spi::send (Spi::msb{}, HostCtrlRdPortReg::address | HCI_INTREG_FLAG | HCI_READ_FLAG) ;

      auto bytes = static_cast<uint8_t*> (buf) ;
      Spi::receive_range (Spi::lsb{}, aux::range (bytes, bytes + size)) ;
   }

   Interrupts::clear (Interrupts::HostCtrl) ;
}

void Hci::external_write (uint16_t addr, bool incaddr, uint8_t size, const void* buf)
{
   HostCtrlSizeReg{} = (uint16_t)size | (incaddr ? 0 : SPI_HOST_CTRL_SIZE_FIXED_ADDR) ;

   {
      Device::Cs _cs ;

      Spi::send (Spi::msb{}, HostCtrlWrPortReg::address  | HCI_INTREG_FLAG) ;

      auto *bytes = static_cast<const uint8_t*> (buf) ;
      Spi::send_range (Spi::lsb{}, aux::range (bytes, bytes + size)) ;
   }

   HostCtrlCfgReg{} = addr | SPI_HOST_CTRL_CONFIG_ENABLE | SPI_HOST_CTRL_CONFIG_DIR_WRITE ;

   for (uint8_t err = 0; !(StatusReg{} & SPI_STATUS_HOST_ACCESS_DONE); ++err)
   {
      aux::debug_assert( err < 200, PSTR("EXT WRITE") ) ;
      _delay_ms (1) ;
   }

   Interrupts::clear (Interrupts::HostCtrl) ;
}

uint32_t Hci::read_diag_window (uint32_t addr)
{
   WindowRdAddrReg{} = addr ;
   return WindowDataPort{} ;
}

void Hci::write_diag_window (uint32_t addr, uint32_t value)
{
   WindowDataPort{} = value ;
   WindowWrAddrReg{} = addr ;
}

TxStream Hci::send_packet (uint16_t size, uint8_t ep)
{
   _check_wait_credits (ep) ;

   uint16_t txlen = (size + sizeof (FrameHdr) + 31) & ~31 ;
   _check_wait_write_buffer_space (txlen) ;

   Device::Cs _cs ;

   SpiDmaSizeReg{} = txlen ;

   uint16_t addr = 0x800 + 0x800 - txlen ;
   Spi::send (Spi::msb{}, addr) ;

   TxStream tx{ txlen } ;

   static constexpr uint8_t HTC_FLAGS_NEED_CREDIT_UPDATE = 1 ;

   uint8_t flags = 0 ;

   if (!--m_credits[ep].m_available)
      flags |= HTC_FLAGS_NEED_CREDIT_UPDATE ;

   tx << FrameHdr{ ep, flags, size, { 0, 0 } } ;

   return aux::forward (tx) ;
}

namespace
{
   static constexpr uint16_t MAX_WRITE_BUFFER_SPACE = 3163 ;
}

void Hci::_init_impl (void)
{
   ConfigReg{} = 1 << 7 ;

   m_write_buffer_space = WrBufSpaceReg{} ;
   aux::debug_assert( m_write_buffer_space == MAX_WRITE_BUFFER_SPACE, PSTR("HCI INIT") ) ;

   _reset_wrbuf_below_watermark() ;

   Interrupts::enable (Interrupts::AllErrors) ;
   Interrupts::enable (Interrupts::LocalCpuIntr) ;

   Interrupts::inhibit() ;
   Device::enable_interrupts (_interrupt_handler) ;
   Interrupts::restore() ;

   int8_t pass = 0 ;

   static constexpr uint32_t DEFAULT_REFCLK_HZ = 26000000 ;

   do
   {
      for (int i = 0; HostInterest::refclk_hz() != DEFAULT_REFCLK_HZ; ++i)
         aux::debug_assert( i < 1000, PSTR("RAK439BAD") ) ;

      HostInterest::flash_is_present() = 0x102 ;
   } while (!pass++ && !_target_reset()) ;

   _init_credits() ;
   Interrupts::enable (Interrupts::PktAvail) ;
}

bool Hci::_target_reset (void)
{
   while (DiagSample{} != 0x001E0104) ;

   if (DiagCfgFound{})
      return true ;

   DiagCfgFound{} = 0xFFFFFFFF ;
   HostInterest::reset_flag() = 0x08 ;
   HostInterest::reset_flag_valid() = 0x12345678 ;
   HostInterest::refclk_hz() = 0 ;
   RtcControl{} = 0x80 ;
   _delay_ms (1) ;

   return false ;
}

void Hci::_reset_wrbuf_below_watermark (void)
{
   WrBufWatermarkReg{} = 0 ;
   Interrupts::clear (Interrupts::WrBufBelowWatermark) ;
}

namespace
{
   bool _check_reset_flag (Interrupts::Flags& ints, Interrupts::Flags theflag)
   {
      if (!(ints & theflag))
         return false ;

      ints = static_cast<Interrupts::Flags> (ints & ~theflag) ;
      return true ;
   }
}

void Hci::_interrupt_handler (void)
{
   if (gpio::Wirq::read())
      return ;

   auto flags = Interrupts::current() ;
   aux::debug_assert( flags, PSTR("RAK439 INT LINE") ) ;

   static constexpr auto HOST_IRQS = Interrupts::AllErrors | Interrupts::HostCtrl | Interrupts::WrBufBelowWatermark ;

   auto host_irqs = flags & HOST_IRQS ;
   if (host_irqs)
      Interrupts::clear (host_irqs) ;

   aux::debug_assert( !(flags & Interrupts::AllErrors), PSTR("RAK439 FATAL") ) ;

   if (_check_reset_flag (flags, Interrupts::HostCtrl))
      Interrupts::disable (Interrupts::HostCtrl) ;

   if (_check_reset_flag (flags, Interrupts::WrBufBelowWatermark))
   {
      m_write_buffer_space = WrBufSpaceReg{} ;
      _reset_wrbuf_below_watermark() ;
   }

   if (_check_reset_flag (flags, Interrupts::LocalCpuIntr))
      _cpu_interrupt() ;

   if (!m_request_is_pending && _check_reset_flag (flags, Interrupts::PktAvail))
   {
      Interrupts::disable (Interrupts::PktAvail) ;
      Interrupts::clear (Interrupts::PktAvail) ;

      aux::LowLatencySched::schedule
         (
            []
            {
               _read_packet() ;
               Interrupts::enable (Interrupts::PktAvail) ;
            }
         ) ;
   }

   if (flags)
      aux::LowLatencySched::schedule (_interrupt_handler) ;
}

void Hci::_cpu_interrupt (void)
{
   CpuIntStatusReg{} = 0xFF ;

   static bool s_init = false ;
   if (s_init)
      return ;

   s_init = true ;

   m_total_credits = TotalCreditsReg{}.get()[3] ;
   _init_credits() ;
}

void Hci::_read_packet (void)
{
   auto hdr = _query_look_ahead() ;
   if (!hdr.m_length)
      return ;

   uint16_t readlen = hdr.m_length + sizeof (FrameHdr) ;
   readlen = (readlen + 31) & ~31 ;

   SpiDmaSizeReg{} = readlen ;

   Device::Cs _cs ;

   uint16_t addr = 0x800 + 0x800 - readlen ;
   Spi::send (Spi::msb{}, addr | HCI_READ_FLAG) ;

   FrameHdr h ;
   Spi::receive (Spi::lsb{}, h) ;

   aux::debug_assert( h.m_length == hdr.m_length, PSTR("LOOKAHEAD") ) ;

   uint16_t datalen = h.m_length,
            trailer = 0 ;

   static constexpr uint8_t RECV_TRAILER = 1 << 1 ;
   if (h.m_flags & RECV_TRAILER)
   {
      trailer = h.m_control[0] ;
      datalen -= trailer ;
   }

   {
      RxStream rx{ datalen } ;

      switch (h.m_ep)
      {
         case 0: break ;
         case 1: m_wmi_pkt_handler (rx) ; break ;
         default: m_data_pkt_handler (rx) ; break ;
      }
   }

   if (trailer)
      _read_packet_trailer (RxStream{ trailer }) ;

   for (uint8_t n = 0; n < readlen - datalen - trailer - sizeof (FrameHdr); ++n)
      Spi::receive() ;
}

void Hci::_read_packet_trailer (RxStream&& rx)
{
   struct Hdr
   {
      enum class Id : uint8_t { Credits = 1, Lookahead, LookaheadBundle } ;

      Id m_id ;
      uint8_t m_len ;
   } ;

   struct CredRep
   {
      uint8_t m_ep ;
      uint8_t m_count ;
   } ;

   while (rx.unread())
   {
      Hdr hdr = rx.read() ;

      switch (hdr.m_id)
      {
         case Hdr::Id::Credits:
            for (uint8_t n = 0; n < hdr.m_len / sizeof (CredRep); ++n)
            {
               CredRep cr = rx.read() ;
               auto& ep = m_credits[cr.m_ep] ;

               ep.m_reported += cr.m_count ;

               if (ep.m_not_reported < cr.m_count)
               {
                  cr.m_count -= ep.m_not_reported ;
                  ep.m_not_reported = 0 ;
               }
               else
               {
                  ep.m_not_reported -= cr.m_count ;
                  cr.m_count = 0 ;
               }

               ep.m_available += cr.m_count ;
            }

            break ;

         default:
            rx.skip (hdr.m_len) ;
      }
   }
}

namespace
{
   uint16_t _swab (uint16_t val)
   {
      return (val << 8) | (val >> 8) ;
   }
}

Hci::FrameHdr Hci::_query_look_ahead (void)
{
   static constexpr FrameHdr NULL_FRAME { 0, 0, 0, { 0, 0 } } ;

   uint16_t avail = RdBufSpaceReg{} ;
   if (avail < 4)
      return NULL_FRAME ;

   union
   {
      uint16_t bytes[2] ;
      FrameHdr hdr ;
   } cvt ;

   cvt.bytes[0] = _swab (RdBufLookahead1Reg{}) ;
   cvt.bytes[1] = _swab (RdBufLookahead2Reg{}) ;

   if (avail < sizeof (FrameHdr) + cvt.hdr.m_length)
      return NULL_FRAME ;

   return cvt.hdr ;
}

void Hci::_check_wait_write_buffer_space (uint16_t required)
{
   required += 5 ; // SPI header size (as it specified in Atheros driver)

   if (m_write_buffer_space < required)
   {
      m_write_buffer_space = WrBufSpaceReg{} ;

      if (m_write_buffer_space < required)
      {
         const auto level = MAX_WRITE_BUFFER_SPACE - required ;
         WrBufWatermarkReg{} = level ;

         if ((StatusReg{} & 6) == 4)
         {
            _delay_ms (1) ;
            if ((StatusReg{} & 6) == 4)
            {
               m_write_buffer_space = WrBufSpaceReg{} ;
               _reset_wrbuf_below_watermark() ;

               aux::debug_assert( m_write_buffer_space >= required, PSTR("WRITE BUF") );
            }
         }

         m_request_is_pending = true ;
         aux::wait ([required] { return m_write_buffer_space >= required ; }) ;
         m_request_is_pending = false ;
      }
   }

   m_write_buffer_space -= required ;
}

void Hci::_check_wait_credits (uint8_t ep)
{
   auto& c = m_credits[ep] ;

   while (!c.m_available)
   {
      aux::debug_assert( c.m_not_reported < 200, PSTR("CREDSLARGE") ) ;

      uint8_t num = CreditCountersReg{}.get()[ep] - c.m_reported ;

      aux::debug_assert( num >= c.m_not_reported, PSTR("CREDSCOUNTERS") ) ;

      c.m_available += num - c.m_not_reported ;
      c.m_not_reported = num ;

      if (!c.m_available)
         _delay_ms (1) ;
   }
}

void Hci::_init_credits (void)
{
   memset (m_credits, 0, sizeof (m_credits)) ;

   m_credits[0].m_available = m_credits[1].m_available = 1 ;
   m_credits[2].m_available = m_total_credits - 2 ;
}

////////////////////////////////////////////////////////////////////////////////////////////

Interrupts::ShadowMask Interrupts::m_shadow_mask ;

void Interrupts::enable (Flags flags)
{
   m_shadow_mask.m_value |= flags ;

   if (!m_shadow_mask.m_inhibited)
      IntEnableReg{} = m_shadow_mask.m_value ;
}

void Interrupts::disable (Flags flags)
{
   m_shadow_mask.m_value &= ~flags ;

   if (!m_shadow_mask.m_inhibited)
      IntEnableReg{} = m_shadow_mask.m_value ;
}

void Interrupts::inhibit (void)
{
   if (m_shadow_mask.m_inhibited)
      return ;

   m_shadow_mask.m_inhibited = 1 ;
   IntEnableReg{} = 0 ;
}

void Interrupts::restore (void)
{
   if (!m_shadow_mask.m_inhibited)
      return ;

   m_shadow_mask.m_inhibited = 0 ;
   IntEnableReg{} = m_shadow_mask.m_value ;
}

Interrupts::Flags Interrupts::current (void)
{
   return static_cast<Flags> (IntCauseReg{} & m_shadow_mask.m_value) ;
}

void Interrupts::clear (Flags flags)
{
   IntCauseReg{} = flags ;
}

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace rak439
} // namespace hw
