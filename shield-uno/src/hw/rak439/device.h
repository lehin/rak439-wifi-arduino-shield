////////////////////////////////////////////////////////////////////////////////////////////
//
// device.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/singleton.h"
#include <avr/interrupt.h>

ISR (PCINT3_vect) ;

namespace hw
{
   namespace rak439
   {

////////////////////////////////////////////////////////////////////////////////////////////

class Device
{
   SINGLETON_DECL (Device) ;

public:
   struct Cs
   {
      Cs (const Cs&) = delete ;
      Cs& operator= (const Cs&) = delete ;

      Cs (void) ;
      ~Cs (void) ;

      static uint8_t s_locks ;
   } ;

public:
   Device (void) ;

   typedef void (*VoidFn) (void) ;
   static void reset (VoidFn completion) ;

   static void enable_interrupts (VoidFn interrupt_handler) ;
   static void disable_interrupts (void) ;

private:
   friend void ::PCINT3_vect (void) ;
   static void _on_wlirq (void) ;

private:
   static VoidFn m_completion ;
   static VoidFn m_interrupt_handler ;
   static bool m_interrupt_pending ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace rak439
} // namespace hw
