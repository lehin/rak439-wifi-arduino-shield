////////////////////////////////////////////////////////////////////////////////////////////
//
// wmi-events.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "wmi.h"
#include "aux/string.h"

namespace hw
{
   namespace rak439
   {
      namespace evts
      {

////////////////////////////////////////////////////////////////////////////////////////////

struct ReadyData
{
   uint32_t m_sw_version ;
   uint32_t m_abi_version ;
   uint8_t m_macaddr[6] ;
   uint8_t m_phyCapability ;
} ;

struct ConnectData
{
   uint16_t m_channel ;
   uint8_t m_bssid[6] ;
   uint16_t m_listenInterval ;
   uint16_t m_beaconInterval ;
   uint32_t m_networkType ;
   uint8_t m_beaconIeLen ;
   uint8_t m_assocReqLen ;
   uint8_t m_assocRespLen ;
} ;

struct DisconnectData
{
    uint16_t m_proto_reason ;
    uint8_t m_bssid[6] ;
    uint8_t m_reason ;
    uint8_t m_response_len ;
} ;

struct CmdErrorData
{
   uint16_t m_cmd ;
   uint8_t m_error ;
} ;

struct EasyConfigData
{
   char m_bssid[48] ;
   char m_easydata[128] ;
   uint8_t m_result ;
   uint8_t m_reserved[3] ;
} ;

struct ScanCompleteData
{
   int32_t m_status ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////
// Reporting statistics

struct TxStats
{
   uint32_t m_tx_packets ;
   uint32_t m_tx_bytes ;
   uint32_t m_tx_unicast_pkts ;
   uint32_t m_tx_unicast_bytes ;
   uint32_t m_tx_multicast_pkts ;
   uint32_t m_tx_multicast_bytes ;
   uint32_t m_tx_broadcast_pkts ;
   uint32_t m_tx_broadcast_bytes ;
   uint32_t m_tx_rts_success_cnt ;
   uint32_t m_tx_packet_per_ac[4] ;
   uint32_t m_tx_errors_per_ac[4] ;

   uint32_t m_tx_errors ;
   uint32_t m_tx_failed_cnt ;
   uint32_t m_tx_retry_cnt ;
   uint32_t m_tx_mult_retry_cnt ;
   uint32_t m_tx_rts_fail_cnt ;
   int32_t m_tx_unicast_rate ;
} ;

struct RxStats
{
   uint32_t m_rx_packets ;
   uint32_t m_rx_bytes ;
   uint32_t m_rx_unicast_pkts ;
   uint32_t m_rx_unicast_bytes ;
   uint32_t m_rx_multicast_pkts ;
   uint32_t m_rx_multicast_bytes ;
   uint32_t m_rx_broadcast_pkts ;
   uint32_t m_rx_broadcast_bytes ;
   uint32_t m_rx_fragment_pkt ;

   uint32_t m_rx_errors ;
   uint32_t m_rx_crcerr ;
   uint32_t m_rx_key_cache_miss ;
   uint32_t m_rx_decrypt_err ;
   uint32_t m_rx_duplicate_frames ;
   int32_t m_rx_unicast_rate ;
} ;

struct TkipCcmpStats
{
   uint32_t m_tkip_local_mic_failure ;
   uint32_t m_tkip_counter_measures_invoked ;
   uint32_t m_tkip_replays ;
   uint32_t m_tkip_format_errors ;
   uint32_t m_ccmp_format_errors ;
   uint32_t m_ccmp_replays ;
} ;

struct PmStats
{
   uint32_t m_power_save_failure_cnt ;
   uint16_t m_stop_tx_failure_cnt ;
   uint16_t m_atim_tx_failure_cnt ;
   uint16_t m_atim_rx_failure_cnt ;
   uint16_t m_bcn_rx_failure_cnt ;
} ;

struct CservStats
{
   uint32_t m_cs_bmiss_cnt ;
   uint32_t m_cs_low_rssi_cnt ;
   uint16_t m_cs_connect_cnt ;
   uint16_t m_cs_disconnect_cnt ;
   uint16_t m_cs_aveBeacon_rssi ;
   uint16_t m_cs_roam_count ;
   int16_t m_cs_rssi ;
   uint8_t m_cs_snr ;
   uint8_t m_cs_aveBeacon_snr ;
   uint8_t m_cs_lastRoam_msec ;
} ;

struct WlanNetStats
{
   TxStats m_tx_stats ;
   RxStats m_rx_stats ;
   TkipCcmpStats m_tkipCcmpStats ;
} ;

struct ArpStats
{
   uint32_t m_arp_received ;
   uint32_t m_arp_matched ;
   uint32_t m_arp_replied ;
} ;

struct WlanWowStats
{
   uint32_t m_wow_num_pkts_dropped ;
   uint16_t m_wow_num_events_discarded ;
   uint8_t m_wow_num_host_pkt_wakeups ;
   uint8_t m_wow_num_host_event_wakeups ;
} ;

struct TargetStats
{
   uint32_t m_lq_val ;
   int32_t m_noise_floor_calibation ;
   PmStats m_pm_stats ;
   WlanNetStats m_txrx_stats ;
   WlanWowStats m_wow_stats ;
   ArpStats m_arp_stats ;
   CservStats m_cserv_stats ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

typedef Event<0x1001,ReadyData> Ready ;
typedef Event<0x1002,ConnectData> Connect ;
typedef Event<0x1003,DisconnectData> Disconnect ;
typedef Event<0x1005,CmdErrorData> CmdError ;
typedef Event<0x1006> RegDomain ;
typedef Event<0x100A,ScanCompleteData> ScanComplete ;
typedef Event<0x100B,TargetStats> ReportStatistics ;
typedef Event<0x101E> WlanVer ;
typedef Event<0x9008,EasyConfigData> EasyConfig ;

////////////////////////////////////////////////////////////////////////////////////////////

struct BssInfo : EventImpl<0x1004>
{
   struct Hdr
   {
      uint16_t m_channel ;
      uint8_t m_frameType ;
      uint8_t m_snr ;
      uint8_t m_bssid[6] ;
      uint16_t m_ie_mask ;
   } m_hdr ;

   aux::String m_ssid ;

   CryptoType m_rsn_cipher = CryptoType::_,
              m_wpa_cipher = CryptoType::_ ;

   AuthMode m_rsn_auth = AuthMode::_,
            m_wpa_auth = AuthMode::_ ;

   typedef EventImpl<0x1004> event_base ;

   BssInfo (RxStream& rx) ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

      } // namespace evts
   } // namespace rak439
} // namespace hw
