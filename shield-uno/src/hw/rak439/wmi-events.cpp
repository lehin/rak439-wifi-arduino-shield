////////////////////////////////////////////////////////////////////////////////////////////
//
// wmi-events.cpp
//
// Copyright © 2018 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "wmi-events.h"
#include "rxstream.h"
#include "wmi-commands.h"
#include "ieee80211.h"

namespace hw
{
   namespace rak439
   {
      namespace evts
      {

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static AuthMode _wpa_auth_parse (uint32_t w)
   {
      #define WPA_SEL(x)  (((uint32_t)(x)<<24)|WPA_OUI)

      switch (w)
      {
         case WPA_SEL(WPA_ASE_8021X_UNSPEC):
            return AuthMode::Wpa ;
         case WPA_SEL(WPA_ASE_8021X_PSK):
            return AuthMode::WpaPsk ;
         case WPA_SEL(WPA_ASE_NONE):
            return AuthMode::None ;
      }

      return AuthMode::_ ;
      #undef WPA_SEL
   }

   static CryptoType _wpa_cipher_parse (uint32_t w)
   {
      #define WPA_SEL(x)  (((uint32_t)(x)<<24)|WPA_OUI)

      switch (w)
      {
         case WPA_SEL(WPA_CSE_NULL):
            return CryptoType::None ;

         case WPA_SEL(WPA_CSE_WEP40):
         case WPA_SEL(WPA_CSE_WEP104):
            return CryptoType::Wep ;

         case WPA_SEL(WPA_CSE_TKIP):
            return CryptoType::Tkip ;

         case WPA_SEL(WPA_CSE_CCMP):
            return CryptoType::Aes ;
      }

      return CryptoType::_ ;
      #undef WPA_SEL
   }

   static CryptoType _rsn_cipher_parse (uint32_t w)
   {
      #define RSN_SEL(x)  (((uint32_t)(x)<<24)|RSN_OUI)

      switch (w)
      {
         case RSN_SEL(RSN_CSE_NULL):
            return CryptoType::None ;

         case RSN_SEL(RSN_CSE_WEP40):
         case RSN_SEL(RSN_CSE_WEP104):
            return CryptoType::Wep ;

         case RSN_SEL(RSN_CSE_TKIP):
            return CryptoType::Tkip ;

         case RSN_SEL(RSN_CSE_CCMP):
            return CryptoType::Aes ;
      }

      return CryptoType::_ ;
      #undef RSN_SEL
   }

   static AuthMode _rsn_auth_parse (uint32_t w)
   {
      #define RSN_SEL(x)  (((uint32_t)(x)<<24)|RSN_OUI)

      switch (w)
      {
         case RSN_SEL(RSN_ASE_8021X_UNSPEC):
            return AuthMode::Wpa2 ;

         case RSN_SEL(RSN_ASE_8021X_PSK):
            return AuthMode::Wpa2Psk ;

         case RSN_SEL(RSN_ASE_NONE):
            return AuthMode::None ;
      }

      return AuthMode::_ ;
      #undef RSN_SEL
   }

   static void _security_ie_parse (RxStream& rx, uint8_t& len, CryptoType& cipher, AuthMode& auth, bool wpa)
   {
      if (len > 4) // skip multicast cihper
      {
         rx.skip (4) ;
         len -= 4 ;
      }

      if (len <= 2)
         cipher |= wpa ? CryptoType::Tkip : CryptoType::Aes ;
      else
      {
         uint16_t cnt = rx.read() ;
         len -= 2 ;

         for (uint16_t i = 0; i < cnt && len; ++i)
         {
            uint32_t val = rx.read() ;
            len -= 4 ;
            cipher |= (wpa ? _wpa_cipher_parse : _rsn_cipher_parse) (val) ;
         }
      }

      if (len <= 2)
         auth |= wpa ? AuthMode::Wpa : AuthMode::Wpa2 ;
      else
      {
         uint16_t cnt = rx.read() ;
         len -= 2 ;

         for (uint16_t i = 0; i < cnt && len; ++i)
         {
            uint32_t val = rx.read() ;
            len -= 4 ;
            auth |= (wpa ? _wpa_auth_parse : _rsn_auth_parse) (val) ;
         }
      }
   }
}

BssInfo::BssInfo (RxStream& rx) :
   event_base{ rx }
{
   rx >> m_hdr ;
   rx.skip (12) ;

   while (rx.unread())
   {
      uint8_t code = rx.read(),
              len = rx.read() ;

      switch (code)
      {
         case 0: // IEEE80211_ELEMID_SSID
            if (!len)
               break ;

            m_ssid = aux::String{ len, len } ;
            rx >> m_ssid.as_range() ;
            len = 0 ;

            break ;

         case 48: // IEEE80211_ELEMID_RSN
            len -= 2 ;
            if (rx.read<uint16_t>() != 1) // read and check version
               break ;

            _security_ie_parse (rx, len, m_rsn_cipher, m_rsn_auth, false) ;

            break ;

         case 221: // IEEE80211_ELEMID_VENDOR
            if (len <= 6)
               break ;

            len -= 4 ;
            if (rx.read<uint32_t>() != (((uint32_t)WPA_OUI_TYPE<<24)|WPA_OUI)) // check OUI type
               break ;

            len -= 2 ;
            if (rx.read<uint16_t>() != 1) // check version
               break ;

            _security_ie_parse (rx, len, m_wpa_cipher, m_wpa_auth, true) ;

            break ;

         default: ;
      }

      if (len)
         rx.skip (len) ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

      } // namespace evts
   } // namespace rak439
} // namespace hw
