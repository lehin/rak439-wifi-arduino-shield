////////////////////////////////////////////////////////////////////////////////////////////
//
// device.cpp
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "device.h"
#include "hw/gpio.h"
#include "hw/spi.h"
#include "aux/scheduler.h"
#include "aux/ll-scheduler.h"
#include <util/atomic.h>

ISR (PCINT3_vect)
{
   hw::rak439::Device::_on_wlirq() ;
}

namespace hw
{
   namespace rak439
   {

////////////////////////////////////////////////////////////////////////////////////////////

Device::VoidFn Device::m_completion ;
Device::VoidFn Device::m_interrupt_handler ;
bool Device::m_interrupt_pending ;

SINGLETON_IMPL (Device) ;

Device::Device (void)
{
   PCICR |= _BV(PCIE3) ;
}

void Device::reset (VoidFn completion)
{
   m_completion = completion ;

   gpio::Wlrst::reset() ;

   aux::Scheduler::schedule
      (
         []
         {
            gpio::Wlrst::set() ;

            auto fn = m_completion ;
            m_completion = nullptr ;

            if (fn)
               fn() ;
         },
         2
      ) ;
}

void Device::enable_interrupts (VoidFn interrupt_handler)
{
   m_interrupt_handler = interrupt_handler ;
   PCIFR = _BV(PCIF3) ;
   PCMSK3 |= _BV(PCINT31) ;
}

void Device::disable_interrupts (void)
{
   PCMSK3 &= ~_BV(PCINT31) ;
}

inline __attribute__ ((always_inline))
   void Device::_on_wlirq (void)
   {
      if (gpio::Wirq::read() || m_interrupt_pending)
         return ;

      m_interrupt_pending = true ;

      aux::LowLatencySched::schedule
         (
            []
            {
               m_interrupt_pending = false ;
               m_interrupt_handler() ;
            }
         ) ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

uint8_t Device::Cs::s_locks ;

Device::Cs::Cs (void)
{
   ATOMIC_BLOCK (ATOMIC_FORCEON)
      if (!s_locks++)
         gpio::Wlcs::reset() ;
}

Device::Cs::~Cs (void)
{
   Spi::flush() ;

   ATOMIC_BLOCK (ATOMIC_FORCEON)
      if (!--s_locks)
         gpio::Wlcs::set() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace rak439
} // namespace hw
