////////////////////////////////////////////////////////////////////////////////////////////
//
// hci.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdint.h>
#include "rxstream.h"
#include "txstream.h"

namespace hw
{
   namespace rak439
   {

////////////////////////////////////////////////////////////////////////////////////////////

class Hci
{
   struct FrameHdr
   {
      uint8_t m_ep ;
      uint8_t m_flags ;
      uint16_t m_length ;
      uint8_t m_control[2] ;
   } ;

public:
   static void initialize (void) ;
   static void shutdown (void) ;

   static void set_internal_register (uint16_t addr, uint16_t val) ;
   static uint16_t get_internal_register (uint16_t addr) ;

   static void external_read (uint16_t addr, bool incaddr, uint8_t size, void* buf) ;
   static void external_write (uint16_t addr, bool incaddr, uint8_t size, const void* buf) ;

   static uint32_t read_diag_window (uint32_t addr) ;
   static void write_diag_window (uint32_t addr, uint32_t value) ;

   typedef void (*WmiPktHandler) (RxStream& rx) ;
   static void set_wmi_packet_handler (WmiPktHandler handler)
      { m_wmi_pkt_handler = handler ; }

   static TxStream send_packet (uint16_t size, uint8_t ep) ;

   typedef void (*DataPktHandler) (RxStream& rx) ;
   static void set_data_packet_handler (DataPktHandler handler)
      { m_data_pkt_handler = handler ; }

private:
   static void _init_impl (void) ;
   static bool _target_reset (void) ;
   static void _reset_wrbuf_below_watermark (void) ;
   static void _interrupt_handler (void) ;
   static void _cpu_interrupt (void) ;
   static void _read_packet (void) ;
   static void _read_packet_trailer (RxStream&& rx) ;
   static FrameHdr _query_look_ahead (void) ;
   static void _check_wait_write_buffer_space (uint16_t required) ;
   static void _check_wait_credits (uint8_t ep) ;
   static void _init_credits (void) ;

private:
   static WmiPktHandler m_wmi_pkt_handler ;
   static DataPktHandler m_data_pkt_handler ;

   struct Credit
   {
      uint8_t m_available,
              m_reported,
              m_not_reported ;
   } ;

   static Credit m_credits[] ;
   static uint8_t m_total_credits ;
   static uint16_t m_write_buffer_space ;
   static bool m_request_is_pending ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

template<uint16_t _Addr>
   struct IntReg
   {
      static constexpr uint16_t address = _Addr ;

      IntReg& operator= (uint16_t val) { set (val) ; return *this ; }
      operator uint16_t (void) const { return get() ; }

      static void set (uint16_t val) { Hci::set_internal_register (address, val) ; }
      static uint16_t get (void) { return Hci::get_internal_register (address) ; }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<uint16_t _Addr, typename _Tp>
   struct ExtReg
   {
      static constexpr uint16_t address = _Addr ;
      typedef _Tp value_type ;

      ExtReg& operator= (value_type val) { set (val) ; return *this ; }

      operator value_type (void) const { return get() ; }

      static void set (value_type val, bool incaddr = true)
      {
         Hci::external_write (address, incaddr, sizeof (value_type), &val) ;
      }

      static value_type get (bool incaddr = true)
      {
         value_type result ;
         Hci::external_read (address, incaddr, sizeof (value_type), &result) ;
         return result ;
      }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<uint32_t _Addr, typename _Tp>
   struct DiagVal
   {
      static constexpr uint32_t address = _Addr & 0x001fffff ; // virtual to physical address
      typedef _Tp value_type ;

      DiagVal& operator= (value_type val) { set (val) ; return *this ; }
      operator value_type (void) const { return get() ; }

      static void set (value_type val) { Hci::write_diag_window (address, val) ; }
      static value_type get (void) { return static_cast<value_type> (Hci::read_diag_window (address)) ; }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

typedef IntReg<0x0100> SpiDmaSizeReg ;
typedef IntReg<0x0200> WrBufSpaceReg ;
typedef IntReg<0x0300> RdBufSpaceReg ;
typedef IntReg<0x0400> ConfigReg ;
typedef IntReg<0x0500> StatusReg ;
typedef IntReg<0x0600> HostCtrlSizeReg ;
typedef IntReg<0x0700> HostCtrlCfgReg ;
typedef IntReg<0x0800> HostCtrlRdPortReg ;
typedef IntReg<0x0A00> HostCtrlWrPortReg ;

typedef IntReg<0x0C00> IntCauseReg ;
typedef IntReg<0x0D00> IntEnableReg ;

typedef IntReg<0x1300> WrBufWatermarkReg ;
typedef IntReg<0x1400> RdBufLookahead1Reg ;
typedef IntReg<0x1500> RdBufLookahead2Reg ;

////////////////////////////////////////////////////////////////////////////////////////////

typedef ExtReg<0x401,uint8_t> CpuIntStatusReg ;
typedef ExtReg<0x419,uint8_t> CpuIntStatusEnableReg ;
typedef ExtReg<0x474,uint32_t> WindowDataPort ;
typedef ExtReg<0x478,uint32_t> WindowWrAddrReg ;
typedef ExtReg<0x47C,uint32_t> WindowRdAddrReg ;

struct CreditCounters
{
   union
   {
      uint8_t m_values[4] ;
      uint32_t m_collector ;
   } m_s ;

   uint8_t operator[] (uint8_t i) const { return m_s.m_values[i] ; }
} ;

typedef ExtReg<0x420,CreditCounters> CreditCountersReg ;
typedef ExtReg<0x424,CreditCounters> TotalCreditsReg ;

////////////////////////////////////////////////////////////////////////////////////////////

typedef DiagVal<0x0054070C,uint32_t> DiagSample ;
typedef DiagVal<0x00540720,uint32_t> DiagCfgFound ;
typedef DiagVal<0x00004000,uint32_t> RtcControl ;

////////////////////////////////////////////////////////////////////////////////////////////

class Interrupts
{
public:
   enum Flags : uint16_t
   {
      PktAvail = 1 << 0,

      RdBufError = 1 << 1,
      WrBufError = 1 << 2,
      SpiAddrError = 1 << 3,
      AllErrors = RdBufError | WrBufError | SpiAddrError,

      LocalCpuIntr = 1 << 4,
      CounterIntr = 1 << 5,
      CpuOn = 1 << 6,
      CpuIntr = 1 << 7,

      HostCtrlWrDone = 1 << 8,
      HostCtrlRdDone = 1 << 9,
      HostCtrl = HostCtrlWrDone | HostCtrlRdDone,

      WrBufBelowWatermark = 1 << 10
   } ;

public:
   static void enable (Flags flags) ;
   static void disable (Flags flags) ;

   static void inhibit (void) ;
   static void restore (void) ;

   static Flags current (void) ;
   static void clear (Flags flags) ;

private:
   struct ShadowMask
   {
      uint16_t m_value : 15,
               m_inhibited : 1 ;
   } ;

   static ShadowMask m_shadow_mask ;
} ;

static constexpr Interrupts::Flags operator| (Interrupts::Flags lhs, Interrupts::Flags rhs)
   { return static_cast<Interrupts::Flags> ((uint16_t)lhs | (uint16_t)rhs) ; }

static constexpr Interrupts::Flags operator& (Interrupts::Flags lhs, Interrupts::Flags rhs)
   { return static_cast<Interrupts::Flags> ((uint16_t)lhs & (uint16_t)rhs) ; }

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Lhs, typename _Rhs>
   struct is_same
   {
      static constexpr bool value = false ;
   } ;

template<typename _Tp>
   struct is_same<_Tp,_Tp>
   {
      static constexpr bool value = true ;
   } ;

class HostInterest
{
private:
   struct Data
   {
      /*
      * Pointer to application-defined area, if any.
      * Set by Target application during startup.
      */
      uint32_t app_host_interest ; /* 0x00 */

      /* Pointer to register dump area, valid after Target crash. */
      uint32_t failure_state ; /* 0x04 */

      /* Pointer to debug logging header */
      uint32_t dbglog_hdr ; /* 0x08 */

      /* Indicates whether or not flash is present on Target.
      * NB: flash_is_present indicator is here not just
      * because it might be of interest to the Host; but
      * also because it's set early on by Target's startup
      * asm code and we need it to have a special RAM address
      * so that it doesn't get reinitialized with the rest
      * of data.
      */
      uint32_t flash_is_present ; /* 0x0c */

      /*
      * General-purpose flag bits,
      * Can be used by application rather than by OS.
      */
      uint32_t option_flag ; /* 0x10 */

      /*
      * Boolean that determines whether or not to
      * display messages on the serial port.
      */
      uint32_t serial_enable ; /* 0x14 */

      /* Start address of Flash DataSet index, if any */
      uint32_t dset_list_head ; /* 0x18 */

      /* Override Target application start address */
      uint32_t app_start ; /* 0x1c */

      /* Clock and voltage tuning */
      uint32_t skip_clock_init ; /* 0x20 */
      uint32_t core_clock_setting ; /* 0x24 */
      uint32_t cpu_clock_setting ; /* 0x28 */
      uint32_t system_sleep_setting ; /* 0x2c */
      uint32_t xtal_control_setting ; /* 0x30 */
      uint32_t pll_ctrl_setting_24ghz ; /* 0x34 */
      uint32_t pll_ctrl_setting_5ghz ; /* 0x38 */
      uint32_t ref_voltage_trim_setting ; /* 0x3c */
      uint32_t clock_info ; /* 0x40 */

      /*
      * Flash configuration overrides, used only
      * when firmware is not executing from flash.
      * (When using flash, modify the global variables
      * with equivalent names.)
      */
      uint32_t bank0_addr_value ; /* 0x44 */
      uint32_t bank0_read_value ; /* 0x48 */
      uint32_t bank0_write_value ; /* 0x4c */
      uint32_t bank0_config_value ; /* 0x50 */

      /* Pointer to Board Data  */
      uint32_t board_data ; /* 0x54 */
      uint32_t board_data_initialized ; /* 0x58 */

      uint32_t dset_RAM_index_table ; /* 0x5c */

      uint32_t desired_baud_rate ; /* 0x60 */
      uint32_t dbglog_config ; /* 0x64 */
      uint32_t end_RAM_reserve_sz ; /* 0x68 */
      uint32_t mbox_io_block_sz ; /* 0x6c */

      uint32_t num_bpatch_streams ; /* 0x70 -- unused */
      uint32_t mbox_isr_yield_limit ; /* 0x74 */

      uint32_t refclk_hz ; /* 0x78 */
      uint32_t ext_clk_detected ; /* 0x7c */
      uint32_t dbg_uart_txpin ; /* 0x80 */
      uint32_t dbg_uart_rxpin ; /* 0x84 */
      uint32_t hci_uart_baud ; /* 0x88 */
      uint32_t hci_uart_pin_assignments ; /* 0x8C */
      /* NOTE: byte [0] = tx pin, [1] = rx pin, [2] = rts pin, [3] = cts pin */
      uint32_t hci_uart_baud_scale_val ; /* 0x90 */
      uint32_t hci_uart_baud_step_val ; /* 0x94 */

      uint32_t allocram_start ; /* 0x98 */
      uint32_t allocram_sz ; /* 0x9c */
      uint32_t hci_bridge_flags ; /* 0xa0 */
      uint32_t hci_uart_support_pins ; /* 0xa4 */
      /* NOTE: byte [0] = RESET pin (bit 7 is polarity), bytes[1]..bytes[3] are for future use */
      uint32_t hci_uart_pwr_mgmt_params ; /* 0xa8 */
      /* 0xa8 - [0]: 1 = enable, 0 = disable
      *        [1]: 0 = UART FC active low, 1 = UART FC active high
      * 0xa9 - [7:0]: wakeup timeout in ms
      * 0xaa, 0xab - [15:0]: idle timeout in ms
      */
      /* Pointer to extended board Data  */
      uint32_t board_ext_data ; /* 0xac */
      uint32_t board_ext_data_config ; /* 0xb0 */
      /*
      * Bit [0]  :   valid
      * Bit[31:16:   size
      */
      /*
      * reset_flag is used to do some stuff when target reset.
      * such as restore app_start after warm reset or
      * preserve host Interest area, or preserve ROM data, literals etc.
      */
      uint32_t reset_flag ; /* 0xb4 */
      /* indicate reset_flag is valid */
      uint32_t reset_flag_valid ; /* 0xb8 */
      uint32_t hci_uart_pwr_mgmt_params_ext ; /* 0xbc */
      /* 0xbc - [31:0]: idle timeout in ms
      */
      /* ACS flags */
      uint32_t acs_flags ; /* 0xc0 */
      uint32_t console_flags ; /* 0xc4 */
      uint32_t nvram_state ; /* 0xc8 */
      uint32_t option_flag2 ; /* 0xcc */

      /* If non-zero, override values sent to Host in WMI_READY event. */
      uint32_t sw_version_override ; /* 0xd0 */
      uint32_t abi_version_override ; /* 0xd4 */
   } ;

   struct ProxyAddrBase
   {
      static constexpr uint32_t BASE_ADDR = 0x00540600 ;

      template<typename _Tp>
         static constexpr uint32_t calc_address (_Tp Data::*fld)
            { return BASE_ADDR + (uint32_t)&(((Data*)0)->*fld) ; }
   } ;

   template<typename _Tp, _Tp Data::*_Field>
      struct ProxyAddr : ProxyAddrBase
      {
         static constexpr uint32_t value = calc_address (_Field) ;
      } ;

public:
   template<typename _Tp, _Tp Data::*_Field>
      struct Proxy : DiagVal<(ProxyAddr<_Tp,_Field>::value),_Tp>
      {
         typedef DiagVal<(ProxyAddr<_Tp,_Field>::value),_Tp> base_class ;
         typedef _Tp value_type ;

         Proxy& operator= (value_type val) { base_class::set (val) ; return *this ; }
      } ;

   #define DECLARE_FIELD(field) \
      static Proxy<decltype(Data::field),&Data::field> field (void) { return {} ; }

   DECLARE_FIELD( app_host_interest )
   DECLARE_FIELD( failure_state )
   DECLARE_FIELD( dbglog_hdr )
   DECLARE_FIELD( flash_is_present )
   DECLARE_FIELD( option_flag )
   DECLARE_FIELD( serial_enable )
   DECLARE_FIELD( dset_list_head )
   DECLARE_FIELD( app_start )
   DECLARE_FIELD( skip_clock_init )
   DECLARE_FIELD( core_clock_setting )
   DECLARE_FIELD( cpu_clock_setting )
   DECLARE_FIELD( system_sleep_setting )
   DECLARE_FIELD( xtal_control_setting )
   DECLARE_FIELD( pll_ctrl_setting_24ghz )
   DECLARE_FIELD( pll_ctrl_setting_5ghz )
   DECLARE_FIELD( ref_voltage_trim_setting )
   DECLARE_FIELD( clock_info )
   DECLARE_FIELD( bank0_addr_value )
   DECLARE_FIELD( bank0_read_value )
   DECLARE_FIELD( bank0_write_value )
   DECLARE_FIELD( bank0_config_value )
   DECLARE_FIELD( board_data )
   DECLARE_FIELD( board_data_initialized )
   DECLARE_FIELD( dset_RAM_index_table )
   DECLARE_FIELD( desired_baud_rate )
   DECLARE_FIELD( dbglog_config )
   DECLARE_FIELD( end_RAM_reserve_sz )
   DECLARE_FIELD( mbox_io_block_sz )
   DECLARE_FIELD( num_bpatch_streams )
   DECLARE_FIELD( mbox_isr_yield_limit )
   DECLARE_FIELD( refclk_hz )
   DECLARE_FIELD( ext_clk_detected )
   DECLARE_FIELD( dbg_uart_txpin )
   DECLARE_FIELD( dbg_uart_rxpin )
   DECLARE_FIELD( hci_uart_baud )
   DECLARE_FIELD( hci_uart_pin_assignments )
   DECLARE_FIELD( hci_uart_baud_scale_val )
   DECLARE_FIELD( hci_uart_baud_step_val )
   DECLARE_FIELD( allocram_start )
   DECLARE_FIELD( allocram_sz )
   DECLARE_FIELD( hci_bridge_flags )
   DECLARE_FIELD( hci_uart_support_pins )
   DECLARE_FIELD( hci_uart_pwr_mgmt_params )
   DECLARE_FIELD( board_ext_data )
   DECLARE_FIELD( board_ext_data_config )
   DECLARE_FIELD( reset_flag )
   DECLARE_FIELD( reset_flag_valid )
   DECLARE_FIELD( hci_uart_pwr_mgmt_params_ext )
   DECLARE_FIELD( acs_flags )
   DECLARE_FIELD( console_flags )
   DECLARE_FIELD( nvram_state )
   DECLARE_FIELD( option_flag2 )
   DECLARE_FIELD( sw_version_override )
   DECLARE_FIELD( abi_version_override )

   #undef DECLARE_FIELD
} ;

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace rak439
} // namespace hw
