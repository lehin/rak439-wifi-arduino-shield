////////////////////////////////////////////////////////////////////////////////////////////
//
// sock-commands.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "sock.h"

////////////////////////////////////////////////////////////////////////////////////////////

namespace hw
{
   namespace rak439
   {
      namespace sock
      {

////////////////////////////////////////////////////////////////////////////////////////////

struct SocketCmdHeader
{
   const uint32_t m_type ;
   const uint32_t m_length ;

   constexpr SocketCmdHeader (SockCmdType type, uint16_t len) :
      m_type{ static_cast<uint32_t> (type) }, m_length{ len } {}
} ;

template<SockCmdType _Cmd, typename _CmdCore, bool _CustomSerializer>
   struct SocketCmdCoreImpl
   {
      typedef impl::CommandSerializer<_CmdCore> serializer ;

      static constexpr SockCmdType CMDTYPE = _Cmd ;

      SocketCmdHeader m_header ;
      _CmdCore m_core ;

      template<typename ... _Args>
         constexpr SocketCmdCoreImpl (_Args ... args) :
            m_header{ _Cmd, serializer::size (m_core) },
            m_core{ args... }
         {
         }
   } ;

template<SockCmdType _Cmd, typename _CmdCore>
   struct SocketCmdCoreImpl<_Cmd,_CmdCore,true>
   {
      typedef impl::CommandSerializer<_CmdCore> serializer ;

      static constexpr SockCmdType CMDTYPE = _Cmd ;

      _CmdCore m_core ;

      template<typename ... _Args>
         constexpr SocketCmdCoreImpl (_Args ... args) :
            m_core{ args... }
         {
         }

      uint16_t size (void) const { return sizeof (SocketCmdHeader) + serializer::size (m_core) ; }

      void send (TxStream& tx) const
      {
         tx << SocketCmdHeader{ _Cmd, serializer::size (m_core) } ;
         serializer::send (m_core, tx) ;
      }
   } ;

template<SockCmdType _Cmd, typename _CmdCore>
   using SocketCmdCore =
      SocketCmdCoreImpl<_Cmd,_CmdCore,impl::CommandSerializer<_CmdCore>::is_custom> ;

template<SockCmdType _Cmd, typename _SockCmdCore>
   using SocketCmd = Command< 0xF055, SocketCmdCore<_Cmd,_SockCmdCore> > ;

////////////////////////////////////////////////////////////////////////////////////////////

struct StackInitParms
{
   uint8_t m_stack_enabled = 1 ;
   uint8_t m_num_sockets = 8 ;
   uint8_t m_num_buffers = 2 ;
   uint8_t m_reserved = 0 ;
} ;

struct SockAddrParms
{
   uint32_t m_sock ;
   SockAddr m_addr ;
   uint8_t m_padding[20] ; // it should be used for IPv6
   uint16_t m_addrlen = sizeof (m_addr) ;

   SockAddrParms (uint32_t sock, const SockAddr& addr = {}) : m_sock{ sock }, m_addr{ addr } {}
} ;

struct OpenParms
{
   uint32_t m_domain = 2 ;
   uint32_t m_type ;
   uint32_t m_protocol = 0 ;

   OpenParms (SockType type) : m_type{ static_cast<uint32_t> (type) } {}
} ;

struct ListenParms
{
   uint32_t m_sock ;
   uint32_t m_backlog ;

   ListenParms (uint32_t sock, int backlog) : m_sock{ sock }, m_backlog{ (uint32_t)backlog } {}
} ;

struct CloseParms
{
   uint32_t m_sock ;
   CloseParms (uint32_t sock) : m_sock{ sock } {}
} ;

struct IpConfigParms : public IpConfigResult
{
   struct Ip6Addr { uint8_t m_data[16] ; } ;

   char m_hostname[33] ;
   Ip6Addr m_ipv6_link_addr ;
   Ip6Addr m_ipv6_global_addr ;
   Ip6Addr m_ipv6_def_gw ;
   Ip6Addr m_ipv6_link_addr_extd ;
   int32_t m_link_prefix ;
   int32_t m_glb_prefix ;
   int32_t m_def_gw_prefix ;
   int32_t m_glb_prefix_extd ;

   IpConfigParms (void) { memset (this, 0, sizeof (*this)) ; }
} ;

struct DhcpIpConfigParms
{
   IpConfigParms m_ipconfig ;

   DhcpIpConfigParms (const char *host)
   {
      m_ipconfig.m_mode = 2 ;

      if (host)
         strncpy (m_ipconfig.m_hostname, host, sizeof (m_ipconfig.m_hostname) - 1) ;
   }
} ;

struct StaticIpConfigParms
{
   IpConfigParms m_ipconfig ;

   StaticIpConfigParms (uint32_t ip, uint32_t mask, uint32_t gw)
   {
      m_ipconfig.m_mode = 1 ;
      m_ipconfig.m_ipv4 = ip ;
      m_ipconfig.m_subnet_mask = mask ;
      m_ipconfig.m_gateway4 = mask ;
   }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

typedef SocketCmd< SockCmdType::StackInit, StackInitParms > StackInit ;
typedef SocketCmd< SockCmdType::IpConfig, DhcpIpConfigParms > DhcpIpConfig ;
typedef SocketCmd< SockCmdType::IpConfig, StaticIpConfigParms > StaticIpConfig ;
typedef SocketCmd< SockCmdType::IpConfig, IpConfigParms > IpConfig ;
typedef SocketCmd< SockCmdType::Open, OpenParms > Open ;
typedef SocketCmd< SockCmdType::Bind, SockAddrParms > Bind ;
typedef SocketCmd< SockCmdType::Listen, ListenParms > Listen ;
typedef SocketCmd< SockCmdType::Accept, SockAddrParms > Accept ;
typedef SocketCmd< SockCmdType::Connect, SockAddrParms > Connect ;
typedef SocketCmd< SockCmdType::Close, CloseParms > Close ;

////////////////////////////////////////////////////////////////////////////////////////////

      } // namespace sock
   } // namespace rak439
} // namespace hw
