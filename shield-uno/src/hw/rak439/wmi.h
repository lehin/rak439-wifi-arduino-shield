////////////////////////////////////////////////////////////////////////////////////////////
//
// wmi.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdint.h>
#include <string.h>
#include "txstream.h"

namespace hw
{
   namespace rak439
   {

////////////////////////////////////////////////////////////////////////////////////////////

class RxStream ;
class TxStream ;

////////////////////////////////////////////////////////////////////////////////////////////

class EventBase
{
public:
   EventBase (RxStream& rx) : m_rx{ rx } {}

   RxStream& rx (void) const { return m_rx ; }

private:
   RxStream& m_rx ;
} ;

template<uint16_t _ID>
   struct EventImpl : EventBase
   {
      static constexpr auto ID = _ID ;

      EventImpl (RxStream& rx) : EventBase{ rx } {}
   } ;

struct EventDummy
{
} ;

template<uint16_t _ID, typename _Core = EventDummy>
   struct Event : EventImpl<_ID>, _Core
   {
      typedef EventImpl<_ID> event_base ;

      static constexpr auto ID = _ID ;

      Event (RxStream& rx) :
         event_base{ rx }
      {
         rx >> static_cast<_Core&> (*this) ;
      }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

namespace impl
{
   struct _Yes { char c[256] ; } ;
   struct _No { char c ; } ;

   template<typename _Tp, void (_Tp::*_SendFn) (TxStream&) const>
      struct _HasSendFn : _Yes
      {
      } ;

   template<typename _Tp>
      _HasSendFn<_Tp, &_Tp::send> has_send_fn (int) ;

   template<typename _Tp>
      _No has_send_fn (...) ;

   template<uint16_t _ID, typename _CmdCore>
      struct CommandBase
      {
         static constexpr auto ID = _ID ;
         const _CmdCore m_core ;

         template<typename ... _Args>
            constexpr CommandBase (_Args ... args) : m_core{ args... } {}
      } ;

   template<typename _CmdCore, bool _HasOwnSend>
      struct CommandSerializerImpl
      {
         static constexpr bool is_custom = false ;
         static constexpr uint16_t size (const _CmdCore&) { return sizeof (_CmdCore) ; }
         static void send (const _CmdCore& core, TxStream& tx) { tx.write (core) ; }
      } ;

   template<typename _CmdCore>
      struct CommandSerializerImpl<_CmdCore,true>
      {
         static constexpr bool is_custom = true ;
         static uint16_t size (const _CmdCore& core) { return core.size() ; }
         static void send (const _CmdCore& core, TxStream& tx) { core.send (tx) ; }
      } ;

   template<typename _CmdCore>
      using CommandSerializer = CommandSerializerImpl<_CmdCore, (sizeof (has_send_fn<_CmdCore> (0)) == sizeof (_Yes))> ;
} ;

template<uint16_t _ID, typename _CmdCore = void>
   struct Command : _CmdCore
   {
      typedef impl::CommandSerializer<_CmdCore> serializer ;

      static constexpr bool custom_serializer = serializer::is_custom ;

      static constexpr auto ID = _ID ;

      template<typename ... _Args>
         constexpr Command (_Args ... args) : _CmdCore{ args... } {}

      uint16_t size (void) const { return serializer::size (*this) ; }
      void send (TxStream& tx) const { serializer::send (*this, tx) ; }
   } ;

template<uint16_t _ID>
   struct Command<_ID,void>
   {
      static constexpr auto ID = _ID ;

      uint16_t size (void) const { return 0 ; }
      void send (TxStream& tx) const { }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Cmd>
   struct PgmSpaceWrapper
   {
      static_assert( !_Cmd::custom_serializer, "PROGMEM command MUST have NOT customized serializer!") ;

      static constexpr auto ID = _Cmd::ID ;
      static uint16_t size (void) { return s_cmd.size() ; }

      static constexpr _Cmd s_cmd PROGMEM = {} ;

      void send (TxStream& tx) const { tx.write_p (s_cmd) ; }
   } ;

template<typename _Cmd>
   constexpr _Cmd PgmSpaceWrapper<_Cmd>::s_cmd PROGMEM ;

template<typename _Cmd> inline
   constexpr PgmSpaceWrapper<_Cmd> pgmcmd (const _Cmd c) { return {} ; }

template<typename _Cmd> inline
   constexpr PgmSpaceWrapper<_Cmd> pgmcmd (void) { return {} ; }

////////////////////////////////////////////////////////////////////////////////////////////

class Wmi
{
   struct CtrlHeader ;
   struct DataHeader ;

   class EvtHandler ;

   template<typename _Event>
      class EvtHandlerImpl ;

   template<typename _Event>
      using Callback = void (*) (const _Event&) ;

public:
   template<typename _Event>
      static void on_event (Callback<_Event> cb)
      {
         _check_initialized() ;

         static EvtHandlerImpl<_Event> s_handler ;
         s_handler.set (cb) ;
      }

   template<typename _Cmd>
      static void command (const _Cmd& cmd)
      {
         auto tx = _send_header (_Cmd::ID, cmd.size()) ;
         cmd.send (tx) ;
      }

   enum class MsgType : uint8_t { Data, Ctrl, Sync, Opt } ;
   enum class HdrType : uint8_t { Dot3, Dot11, Acl } ;

   static TxStream send (MsgType msgtype, HdrType datatype, uint16_t msgsize) ;

   typedef void (*ReceiveHandler) (RxStream& rx) ;
   static void on_receive (ReceiveHandler handler) { m_receive_handler = handler ; }

   static uint8_t get_rssi (void) ;

private:
   static void _check_initialized (void) ;
   static void _handle_wmi_packet (RxStream& rx) ;
   static void _handle_data_packet (RxStream& rx) ;
   static TxStream _send_header (uint16_t cmdid, uint16_t cmdsize) ;

private:
   static bool m_initialized ;
   static ReceiveHandler m_receive_handler ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class Wmi::EvtHandler
{
public:
   EvtHandler (const uint16_t event_id) :
      m_event_id{ event_id },
      m_next{ s_head }
   {
      s_head = this ;
   }

   static bool empty (void) { return !s_head ; }
   static bool handle (uint16_t event_id, RxStream& rx) ;

protected:
   virtual void handle_event (RxStream& rx) const = 0 ;

private:
   const uint16_t m_event_id ;
   const EvtHandler *const m_next ;
   static const EvtHandler *s_head ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Event>
   class Wmi::EvtHandlerImpl : EvtHandler
   {
   public:
      EvtHandlerImpl (void) : EvtHandler{ _Event::ID } {}

      typedef void (*HandlerFn) (const _Event&) ;
      void set (HandlerFn fn) { m_fn = fn ; }

   protected:
      // еще бы m_fn проверить...
      virtual void handle_event (RxStream& rx) const { m_fn (_Event{ rx }) ; }

   private:
      HandlerFn m_fn ;
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

enum class Dot11AuthMode : uint8_t { Open = 0x01, Shared = 0x02, Leap = 0x04 } ;
enum class AuthMode : uint8_t { _ = 0, None = 0x01, Wpa = 0x02, Wpa2 = 0x04, WpaPsk = 0x08, Wpa2Psk = 0x10, WpaCckm = 0x20, Wpa2Cckm = 0x40 } ;
enum class CryptoType : uint8_t { _ = 0, None = 0x01, Wep = 0x02, Tkip = 0x04, Aes = 0x08 } ;

enum class ConnectCtrlFlags : uint32_t
{
   None = 0x0000,
   AssocPolicyUser = 0x0001,
   SendReassoc = 0x0002,
   IgnoreWpaxGroupCipher = 0x0004,
   ProfileMatchDone = 0x0008,
   IgnoreAacBeacon = 0x0010,
   CsaFollowBss = 0x0020,
   DoWpaOffload = 0x0040,
   DoNotDeauth = 0x0080,
   WpsF1lag = 0x0100
} ;

inline Dot11AuthMode operator| (Dot11AuthMode lhs, Dot11AuthMode rhs)
   { return (Dot11AuthMode) ((uint8_t)lhs | (uint8_t)rhs) ; }

inline ConnectCtrlFlags operator| (ConnectCtrlFlags lhs, ConnectCtrlFlags rhs)
   { return (ConnectCtrlFlags) ((uint32_t)lhs | (uint32_t)rhs) ; }

inline AuthMode operator| (AuthMode lhs, AuthMode rhs)
   { return (AuthMode) ((uint8_t)lhs | (uint8_t)rhs) ; }

inline AuthMode& operator|= (AuthMode& lhs, AuthMode rhs)
   { return lhs = lhs | rhs ; }

inline bool operator& (AuthMode lhs, AuthMode rhs)
   { return ((uint8_t)lhs & (uint8_t)rhs) != 0 ; }

inline CryptoType operator| (CryptoType lhs, CryptoType rhs)
   { return (CryptoType) ((uint8_t)lhs | (uint8_t)rhs) ; }

inline CryptoType& operator|= (CryptoType& lhs, CryptoType rhs)
   { return lhs = lhs | rhs ; }

inline bool operator& (CryptoType lhs, CryptoType rhs)
   { return ((uint8_t)lhs & (uint8_t)rhs) != 0 ; }

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace rak439
} // namespace hw
