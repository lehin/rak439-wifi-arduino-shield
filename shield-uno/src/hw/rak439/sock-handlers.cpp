////////////////////////////////////////////////////////////////////////////////////////////
//
// sock-handlers.cpp
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "sock-handlers.h"
#include "rxstream.h"
#include "aux/ll-scheduler.h"

namespace hw
{
   namespace rak439
   {

////////////////////////////////////////////////////////////////////////////////////////////

static constexpr uint32_t TCP_CONNECTION_AVAILABLE = 99 ;

////////////////////////////////////////////////////////////////////////////////////////////

SockHandlerBase *SockHandlerBase::s_head ;

void SockHandlerBase::handle_response (const SockResponse& event)
{
   for (auto resp = s_head; resp; resp = resp->m_next)
      if (resp->is_waiting (event))
      {
         resp->process (event) ;
         return ;
      }
}

SockHandlerBase::SockHandlerBase (SockCmdType type) :
   m_type{ type },
   m_prev{ nullptr },
   m_next{ s_head }
{
   s_head = this ;
   if (m_next)
      m_next->m_prev = this ;
}

SockHandlerBase::~SockHandlerBase (void)
{
   if (m_prev)
      m_prev->m_next = m_next ;
   else
      s_head = m_next ;

   if (m_next)
      m_next->m_prev = m_prev ;
}

bool SockHandlerBase::is_waiting (const SockResponse& resp) const
{
   return static_cast<SockCmdType> (resp.m_type) == m_type ;
}

////////////////////////////////////////////////////////////////////////////////////////////

uint32_t SockHandler::retcode (void) const
{
   aux::wait ([this] { return m_ready ; }) ;
   return m_error ;
}

bool SockHandler::is_waiting (const SockResponse& resp) const
{
   return
      SockHandlerBase::is_waiting (resp) &&
      resp.m_sock == m_sock &&
      resp.m_error != TCP_CONNECTION_AVAILABLE ;
}

void SockHandler::process (const SockResponse& resp)
{
   m_error = resp.m_error ;
   m_ready = true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

uint32_t SockOpenHandler::sock (void) const
{
   aux::wait ([this] { return m_ready ; }) ;
   return m_sock ;
}

void SockOpenHandler::process (const SockResponse& resp)
{
   m_sock = resp.m_sock ;
   m_ready = true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

SockListenerHandler::Callback SockListenerHandler::m_callback ;

bool SockListenerHandler::is_waiting (const SockResponse& resp) const
{
   return SockHandlerBase::is_waiting (resp) && resp.m_error == TCP_CONNECTION_AVAILABLE ;
}

void SockListenerHandler::process (const SockResponse& resp)
{
   if (m_callback)
      m_callback (resp.m_sock) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

SockCallbackHandler::Callback SockCallbackHandler::m_callback ;

void SockCallbackHandler::process (const SockResponse& resp)
{
   if (m_callback)
      m_callback (resp.m_sock) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

SockSetIpHandler::Callback SockSetIpHandler::m_callback ;

bool SockSetIpHandler::is_waiting (const SockResponse& resp) const
{
   return SockHandlerBase::is_waiting (resp) && m_callback ;
}

void SockSetIpHandler::process (const SockResponse& resp)
{
   if (m_callback)
      m_callback (resp.m_error != -1) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

uint32_t SockQueryIpHandler::retcode (void) const
{
   aux::wait ([this] { return m_ready ; }) ;
   return m_error ;
}

void SockQueryIpHandler::process (const SockResponse& resp)
{
   m_error = resp.m_error ;
   m_ready = true ;

   if (m_error != -1)
      resp.rx() >> m_result ;
//      m_result.m_dnsrv1 = resp.rx().unread() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace rak439
} // namespace hw
