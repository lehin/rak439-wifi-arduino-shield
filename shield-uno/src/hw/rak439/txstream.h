////////////////////////////////////////////////////////////////////////////////////////////
//
// txstream.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/debug-assert.h"
#include "aux/range.h"
#include "hw/spi.h"
#include "device.h"

namespace hw
{
   namespace rak439
   {

////////////////////////////////////////////////////////////////////////////////////////////

class TxStream
{
   TxStream (const TxStream&) = delete ;
   TxStream& operator= (const TxStream&) = delete ;

public:
   TxStream (uint16_t unsent) : m_unsent{ unsent } {} ;
   TxStream (TxStream&& org) :
      m_unsent{ org.m_unsent }
   {
      org.m_unsent = 0 ;
   }

   ~TxStream (void) ;

   template<typename  _Tp>
      TxStream& operator<< (const _Tp& p) { write (p) ; return *this ; }

   template<typename  _Tp>
      TxStream& operator<< (const aux::Range<_Tp*>& r) { write (r) ; return *this ; }

   template<typename  _Tp>
      void write (const _Tp& p) ;

   template<typename  _Tp>
      void write (const aux::Range<_Tp*>& r) ;

   template<typename  _Tp>
      void write_p (const _Tp& p) ;

   void fill (uint16_t size, uint8_t val = 0) ;

private:
   void _generic_write (const void *buf, uint16_t size) ;
   void _generic_write_p (const void *buf, uint16_t size) ;

private:
   uint16_t m_unsent ;
   Device::Cs m_lock ;
} ;


template<typename  _Tp> inline
   void TxStream::write (const _Tp& p)
   {
      _generic_write (&p, sizeof (p)) ;
   }

template<typename  _Tp> inline
   void TxStream::write (const aux::Range<_Tp*>& r)
   {
      typedef aux::Range<_Tp*> Range ;
      _generic_write (&*r.begin(), sizeof (typename Range::value_type) * r.size()) ;
   }

template<typename  _Tp> inline
   void TxStream::write_p (const _Tp& p)
   {
      _generic_write_p (&p, sizeof (p)) ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace rak439
} // namespace hw
