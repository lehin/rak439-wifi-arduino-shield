////////////////////////////////////////////////////////////////////////////////////////////
//
// client-sink.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/array.h"
#include "aux/range.h"
#include "aux/string.h"
#include "util/strconv.h"

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

struct endl{} ;
struct divider{} ;
struct pstr{} ;
struct hex{} ;

////////////////////////////////////////////////////////////////////////////////////////////

namespace impl
{
   template<size_t _TpSz>
      struct IntFormatterBase
      {
         static constexpr size_t BUF_SIZES[] { 4, 6, 0, 11 } ;
         static_assert( _TpSz <= sizeof (BUF_SIZES) / sizeof (*BUF_SIZES), "Type to format is too big" ) ;

         static constexpr size_t BUFSIZE = BUF_SIZES[ _TpSz - 1 ] ;
         static_assert( BUFSIZE, "Type to format is wrong" ) ;

         static char s_buf[BUFSIZE] ;
      } ;

   template<size_t _TpSz>
      char IntFormatterBase<_TpSz>::s_buf[] ;

   template<typename _Tp>
      struct IntFormatter : IntFormatterBase<sizeof(_Tp)>
      {
         typedef IntFormatterBase<sizeof(_Tp)> base_class ;

         static aux::StringRef format (_Tp val)
         {
            auto end = util::itoa (val, base_class::s_buf) ;
            return { base_class::s_buf, end } ;
         }
      } ;

   template<size_t _TpSz>
      struct HexFormatterBase
      {
         static constexpr size_t BUF_SIZES[] { 2, 4, 0, 8 } ;
         static_assert( _TpSz <= sizeof (BUF_SIZES) / sizeof (*BUF_SIZES), "Type to format is too big" ) ;

         static constexpr size_t BUFSIZE = BUF_SIZES[ _TpSz - 1 ] ;
         static_assert( BUFSIZE, "Type to format is wrong" ) ;

         static char s_buf[BUFSIZE] ;
      } ;

   template<size_t _TpSz>
      char HexFormatterBase<_TpSz>::s_buf[] ;

   template<typename _Tp>
      struct HexFormatter : HexFormatterBase<sizeof(_Tp)>
      {
         typedef HexFormatterBase<sizeof(_Tp)> base_class ;

         static aux::StringRef format (_Tp val)
         {
            auto end = util::itoah (val, base_class::s_buf) ;
            return { base_class::s_buf, end } ;
         }
      } ;
}

////////////////////////////////////////////////////////////////////////////////////////////

class TxtStream
{
public:
   enum class Result : uint8_t { Ok, Unknown, Error } ;

protected:
   virtual ~TxtStream (void) {}

public:
   TxtStream (void) ;

   template<typename _Head, typename ... _Args>
      TxtStream& write (const _Head& head, const _Args& ... args)
         { _write (head) ; write (args...) ; return *this ; }

   void write (void) {}

   template<typename _Arg>
      TxtStream& operator<< (const _Arg& arg)
         { _write (arg) ; return *this ; }

   virtual void commit (void) {} ;

protected:
   virtual void push_char (char ch) = 0 ;
   virtual void push_data (const char *data, size_t sz) = 0 ;

private:
   template<typename _Tp>
      void _write (const _Tp& p)
      {
         if (!m_flags.m_hex)
            _write (impl::IntFormatter<_Tp>::format (p)) ;
         else
         {
            _write (impl::HexFormatter<_Tp>::format (p)) ;
            m_flags.m_hex = false ;
         }
      }

   template<typename _It>
      void _write (const aux::Range<_It>& r)
         { for (auto& a : r) _write (a) ; }

   void _write (char ch) { push_char (ch) ; }
   void _write (bool b) ;
   void _write (const aux::String& str) { push_data (str.begin(), str.size()) ; }
   void _write (const aux::StringRef& str) { push_data (str.begin(), str.size()) ; }
   void _write (const char *str) ;
   void _write (pstr) { m_flags.m_pstr = true ; }
   void _write (hex) { m_flags.m_hex = true ; }
   void _write (endl) { write ('\r','\n') ; }
   void _write (divider) ;
   void _write (Result p) ;

   void _write_data (const char *data, size_t sz) ;

private:
   void _reset_flags (void) { m_flags = { false, false } ; }

private:
   struct Flags
   {
      bool m_hex : 1,
           m_pstr : 1 ;
   } m_flags ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util
