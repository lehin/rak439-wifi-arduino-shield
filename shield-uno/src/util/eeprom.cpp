////////////////////////////////////////////////////////////////////////////////////////////
//
// eeprom.cpp
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "eeprom.h"

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

uint8_t EepromStrPool::add (const aux::StringRef& s)
{
   uint8_t idx = _find_free() ;
   _add_impl (idx, s) ;
   return idx ;
}

void EepromStrPool::remove (uint8_t idx)
{
   if (idx == (uint8_t)-1)
      return ;

   uint16_t addr = _address (idx) ;
   if (addr == (uint16_t)-1)
      return ;

   _address (idx, (uint16_t)-1) ;

   uint8_t size ;
   EepromExchange::read (addr, size) ;

   uint16_t last_addr = addr ;
   uint8_t last_size = size ;
   uint8_t cnt = _count() ;

   for (uint8_t n = 0; n < cnt; ++n)
   {
      if (n == idx)
         continue ;

      uint16_t a = _address (n) ;
      if (a == (uint16_t)-1 || a <= addr)
         continue ;

      if (a > last_addr)
      {
         last_addr = a ;
         EepromExchange::read (a, last_size) ;
      }

      a -= size + 1 ;
      _address (n, a) ;
   }

   for (uint16_t a = addr + size + 1; a < last_addr + last_size + 1; ++a, ++addr)
   {
      uint8_t byte ;
      EepromExchange::read (a, byte) ;
      EepromExchange::write (addr, byte) ;
   }
}

void EepromStrPool::update (uint8_t idx, const aux::StringRef& s)
{
   remove (idx) ;
   _add_impl (idx, s) ;
}

aux::String EepromStrPool::get (uint8_t idx)
{
   if (idx == (uint8_t)-1)
      return {} ;

   uint16_t addr = _address (idx) ;
   if (addr == (uint16_t)-1)
      return {} ;

   uint8_t size ;
   EepromExchange::read (addr, size) ;

   if (size == (uint8_t)-1)
      return {} ;

   aux::String res{ size, size } ;
   eeprom_read_block (res.begin(), (const void*)(addr + 1), size) ;

   return res ;
}

uint8_t EepromStrPool::size (uint8_t idx)
{
   if (idx == (uint8_t)-1)
      return 0 ;

   uint16_t addr = _address (idx) ;
   if (addr == (uint16_t)-1)
      return 0 ;

   uint8_t size ;
   EepromExchange::read (addr, size) ;

   return size != (uint8_t)-1 ? size : 0 ;
}

char* EepromStrPool::fill (uint8_t idx, char *buf)
{
   if (idx == (uint8_t)-1)
      return buf ;

   uint16_t addr = _address (idx) ;
   if (addr == (uint16_t)-1)
      return buf ;

   uint8_t size ;
   EepromExchange::read (addr, size) ;

   if (size == (uint8_t)-1)
      return buf ;

   eeprom_read_block (buf, (const void*)(addr + 1), size) ;

   return buf + size ;
}

uint8_t EepromStrPool::_count (void)
{
   uint8_t res ;
   EepromExchange::read (Eeprom::data_size() + POOL_SIZE - 1, res) ;

   if (res == (uint8_t)-1)
      res = 0 ;

   return res ;
}

void EepromStrPool::_count (uint8_t cnt)
{
   EepromExchange::write (Eeprom::data_size() + POOL_SIZE - 1, cnt) ;
}

uint16_t EepromStrPool::_index_item (uint8_t idx)
{
   return Eeprom::data_size() + POOL_SIZE - 1 - (idx + 1) * sizeof (uint16_t) ;
}

uint16_t EepromStrPool::_address (uint8_t idx)
{
   uint16_t res ;
   EepromExchange::read (_index_item (idx), res) ;
   return res ;
}

void EepromStrPool::_address (uint8_t idx, uint16_t addr)
{
   EepromExchange::write (_index_item (idx), addr) ;
}

uint8_t EepromStrPool::_find_free (void)
{
   for (uint8_t idx = 0; ; ++idx)
      if (_address (idx) == (uint16_t)-1)
         return idx ;
}

void EepromStrPool::_add_impl (uint8_t idx, const aux::StringRef& s)
{
   uint16_t last_addr = 0,
            freeblk = Eeprom::data_size() ;

   uint8_t cnt = _count() ;

   for (uint8_t n = 0; n < cnt; ++n)
   {
      uint16_t a = _address (n) ;
      if (a == (uint16_t)-1 || a <= last_addr)
         continue ;

      last_addr = a ;

      uint8_t size ;
      EepromExchange::read (a, size) ;

      freeblk = a + size + 1 ;
   }

   _address (idx, freeblk) ;

   EepromExchange::write (freeblk, (uint8_t)s.size()) ;
   eeprom_update_block (s.begin(), (void*)(freeblk + 1), s.size()) ;

   if (idx >= _count())
      _count (idx + 1) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

aux::String EepromString::get (void) const
{
   return EepromStrPool::get (m_id) ;
}

void EepromString::set (const aux::StringRef& s)
{
   if (m_id != (uint8_t)-1)
      EepromStrPool::update (m_id, s) ;
   else
      m_id = EepromStrPool::add (s) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util
