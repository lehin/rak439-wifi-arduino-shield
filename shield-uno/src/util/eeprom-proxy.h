////////////////////////////////////////////////////////////////////////////////////////////
//
// eeprom-proxy.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdint.h>
#include <avr/eeprom.h>

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

struct EepromExchange
{
   template<typename _Tp>
      static void read (uint16_t addr, _Tp& val)
      {
         eeprom_read_block (&val, (const void*)addr, sizeof (_Tp)) ;
      }

   template<typename _Tp>
      static void write (uint16_t addr, const _Tp& val)
      {
         eeprom_update_block (&val, (void*)addr, sizeof (_Tp)) ;
      }

   static void read (uint16_t addr, uint8_t& val)
   {
      val = eeprom_read_byte ((const uint8_t*)addr) ;
   }

   static void write (uint16_t addr, uint8_t val)
   {
      eeprom_update_byte ((uint8_t*)addr, val) ;
   }

   static void read (uint16_t addr, uint16_t& val)
   {
      val = eeprom_read_word ((const uint16_t*)addr) ;
   }

   static void write (uint16_t addr, uint16_t val)
   {
      eeprom_update_word ((uint16_t*)addr, val) ;
   }

   static void read (uint16_t addr, uint32_t& val)
   {
      val = eeprom_read_dword ((const uint32_t*)addr) ;
   }

   static void write (uint16_t addr, uint32_t val)
   {
      eeprom_update_dword ((uint32_t*)addr, val) ;
   }

   static void reset (uint16_t addr, size_t size)
   {
      while (size--)
         eeprom_write_byte ((uint8_t*)addr++, 255) ;
   }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class EepromProxyBase
{
   friend class Eeprom ;
   static void init (uint8_t data_size) ;

protected:
   template<typename _Tp>
      static void eep_read (uint16_t addr, _Tp& data)
      {
         EepromExchange::read (addr, data) ;
      }

   template<typename _Tp>
      static void eep_write (uint16_t addr, const _Tp& data)
      {
         EepromExchange::write (addr, data) ;
      }

   template<typename _Tp>
      static void eep_reset (uint16_t addr)
      {
         EepromExchange::reset (addr, sizeof (_Tp)) ;
      }
} ;

template<typename _Tp>
   class ValueProxy : EepromProxyBase
   {
      union Convertor
      {
         _Tp m_val ;
         uint8_t m_bytes[sizeof (_Tp)] ;
      } ;

   public:
      ValueProxy (uint16_t addr) : m_addr { addr } {}

      operator _Tp (void) const { return operator*() ; }

      _Tp operator* (void) const
      {
         _Tp result ;
         eep_read (m_addr, result) ;
         return result ;
      }

      void operator= (const _Tp& val)
      {
         eep_write (m_addr, val) ;
      }

      constexpr uint16_t address (void) const { return m_addr ; }

      void reset (void) { eep_reset<_Tp> (m_addr) ; }

   private:
      const uint16_t m_addr ;
   } ;

template<typename _Tp>
   class ArrayProxy
   {
   public:
      ArrayProxy (uint16_t addr) : m_addr { addr } {}

      ValueProxy<_Tp> operator[] (uint8_t idx) const { return m_addr + idx * sizeof (_Tp) ; }
      ValueProxy<_Tp> operator* (void) const { return operator[](0) ; }

   private:
      const uint16_t m_addr ;
   } ;

template<typename _Tp>
   class EepromProxy : public ValueProxy<_Tp>
   {
      typedef ValueProxy<_Tp> base_class ;

   public:
      EepromProxy (uint16_t addr) : base_class { addr } {}

      using base_class::operator= ;
   } ;

template<typename _Tp, unsigned sz>
   class EepromProxy<_Tp[sz]> : public ArrayProxy<_Tp>
   {
      typedef ArrayProxy<_Tp> base_class ;

   public:
      EepromProxy (uint16_t addr) : base_class { addr } {}
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util
