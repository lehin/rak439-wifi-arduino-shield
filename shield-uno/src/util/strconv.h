////////////////////////////////////////////////////////////////////////////////////////////
//
// strconv.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/string.h"
#include <stdint.h>

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

char *itoa (uint8_t n, char *dst, bool leadings = false, uint8_t div = 100) ;
char *itoa (int8_t n, char *dst, bool leadings = false, uint8_t div = 100) ;
char *itoa (uint16_t n, char *dst, bool leadings = false, uint16_t div = 10000) ;
char *itoa (int16_t n, char *dst, bool leadings = false, uint16_t div = 10000) ;
char* itoa (uint32_t n, char *dst, bool leadings = false, uint32_t div = 1000000000ul) ;
char* itoa (int32_t n, char *dst, bool leadings = false, uint32_t div = 1000000000ul) ;
char* itoah (uint8_t n, char *dst) ;
char* itoah (uint16_t n, char *dst) ;
char* itoah (uint32_t n, char *dst) ;

inline char* itoah (int8_t n, char *dst) { return itoah ((uint8_t)n, dst) ; }
inline char* itoah (int16_t n, char *dst) { return itoah ((uint16_t)n, dst) ; }
inline char* itoah (int32_t n, char *dst) { return itoah ((uint32_t)n, dst) ; }

uint8_t atoib (const char *s, const char *end, const char **err = nullptr) ;
uint16_t atoiw (const char *s, const char *end, const char **err = nullptr) ;
int32_t atol (const char *s, const char *end, const char **err = nullptr) ;
uint32_t atoul (const char *s, const char *end, const char **err = nullptr) ;
uint8_t ahtoib (const char *s, const char *end, const char **err = nullptr) ;
uint16_t ahtoiw (const char *s, const char *end, const char **err = nullptr) ;
float atof (const char *s, const char *end) ;

////////////////////////////////////////////////////////////////////////////////////////////

char* iptostr (uint32_t ip, char *dst) ;
char* mactostr (const uint8_t* mac, char *dst) ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util
