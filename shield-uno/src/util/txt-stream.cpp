////////////////////////////////////////////////////////////////////////////////////////////
//
// client-sink.cpp
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "txt-stream.h"
#include <avr/pgmspace.h>
#include <string.h>

#define DECLARE_PGM_STR(a,s) \
   static const char a[] PROGMEM = s

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

TxtStream::TxtStream (void)
{
   _reset_flags() ;
}

void TxtStream::_write (bool b)
{
   *this << pstr{} << (b ? PSTR("true") : PSTR("false")) ;
}

void TxtStream::_write (divider)
{
   for (int8_t n = 0; n < 10; ++n)
      _write ('-') ;

   _write (endl{}) ;
}

void TxtStream::_write (Result p)
{
   DECLARE_PGM_STR (_ok, "ok") ;
   DECLARE_PGM_STR (_unknown, "unknown") ;
   DECLARE_PGM_STR (_error, "error") ;

   static const char* const PROGMEM _responses[] PROGMEM { _ok, _unknown, _error } ;

   uint8_t code = static_cast<uint8_t> (p) ;
   *this << static_cast<char>('0' + code) << ' ' ;

   auto str = reinterpret_cast<const char*> (pgm_read_word (_responses + code)) ;
   operator<< (pstr{}) << str << endl{} ;
}

void TxtStream::_write (const char *str)
{
   if (!m_flags.m_pstr)
      while (*str)
         push_char (*str++) ;
   else
   {
      while (char ch = pgm_read_byte (str++))
         push_char (ch) ;

      m_flags.m_pstr = false ;
   }
}

void TxtStream::_write_data (const char *data, size_t sz)
{
   push_data (data, sz) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util
