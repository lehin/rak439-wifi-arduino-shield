////////////////////////////////////////////////////////////////////////////////////////////
//
// eeprom.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "eeprom-proxy.h"
#include "aux/string.h"
#include <avr/eeprom.h>

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

class EepromStrPool
{
   static constexpr uint16_t POOL_SIZE = 512 ;

public:
   static uint8_t add (const aux::StringRef& s) ;
   static void remove (uint8_t idx) ;
   static void update (uint8_t idx, const aux::StringRef& s) ;
   static aux::String get (uint8_t idx) ;

   static uint8_t size (uint8_t idx) ;
   static char* fill (uint8_t idx, char *buf) ;

private:
   static uint8_t _count (void) ;
   static void _count (uint8_t cnt) ;
   static uint16_t _index_item (uint8_t idx) ;
   static uint16_t _address (uint8_t idx) ;
   static void _address (uint8_t idx, uint16_t addr) ;
   static uint8_t _find_free (void) ;
   static void _add_impl (uint8_t idx, const aux::StringRef& s) ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class EepromString
{
public:
   EepromString (void) : m_id{ (uint8_t)-1 } {}

   operator aux::String (void) const { return get() ; }

   template<typename _Tp>
      EepromString& operator= (const _Tp& s) { set (s) ; return *this ; }

   aux::String get (void) const ;

   uint8_t size (void) const { return EepromStrPool::size (m_id) ; }
   char* fill (char *buf) { return EepromStrPool::fill (m_id, buf) ; }


   void set (const aux::String& s) { set ({ s.begin(), s.end() }) ; }
   void set (const aux::StringRef& s) ;

private:
   uint8_t m_id ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Struct, typename _MemType>
   EepromProxy<_MemType> eeprom_proxy (_MemType _Struct::*_Member)
   {
      return EepromProxy<_MemType> { (uint16_t) & (((_Struct*)0) ->* _Member) } ;
   }

#define EEPROM_MEMBER(s,m) \
   static auto m (void) -> decltype (eeprom_proxy (&s::m)) { return eeprom_proxy (&s::m) ; }

////////////////////////////////////////////////////////////////////////////////////////////

class Eeprom
{
private:
   struct Data
   {
      EepromString host_name ;
      uint8_t uart_debug ;
   } ;

public:
   EEPROM_MEMBER (Data, host_name) ;
   EEPROM_MEMBER (Data, uart_debug) ;

   static constexpr uint16_t data_size (void) { return sizeof (Data) ; }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util
