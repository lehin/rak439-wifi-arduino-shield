#include "aux/ll-scheduler.h"
#include "aux/scheduler.h"
#include "hw/watchdog.h"
#include "lan/lan-service.h"
#include "controller/serial-master.h"
#include "util/uart-dbg.h"
#include "hw/gpio.h"
#include "host/comm.h"

extern "C"
{
   void __cxa_pure_virtual(void)
   {
      abort() ;
   }

   void __cxa_deleted_virtual(void)
   {
      abort() ;
   }
}

static void _initialize (void)
{
   hw::gpio::Pwr::set() ;

   if (util::UartDbg::enabled())
      util::UartDbg{} << util::pstr{} << PSTR("Starting...") << util::endl{} ;

   lan::Service::initialize() ;
   host::Comm::initialize() ;

   controller::SerialMaster::initialize() ;
}

int main (int srgc, const char *argv[])
{
   hw::Wdt::enable() ;
   sei() ;

   aux::LowLatencySched::schedule (_initialize) ;

   while (true)
   {
      hw::Wdt::reset() ;
      aux::LowLatencySched::process_no_sleep() ;
   }
}
