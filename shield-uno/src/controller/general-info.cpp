////////////////////////////////////////////////////////////////////////////////////////////
//
// general-info.cpp
//
// Copyright © 2018 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "general-info.h"
#include "util/strconv.h"

extern "C" { extern uint16_t __brkval ; }

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

static const char _ver[] PROGMEM = "Arduino RAK439 WiFi shield ver 1.0" ;
static const char _built[] PROGMEM = __DATE__ " " __TIME__ ;

////////////////////////////////////////////////////////////////////////////////////////////

void GeneralInfo::ver (util::TxtStream& sink)
{
   sink << util::pstr{} << _ver ;
}

void GeneralInfo::built (util::TxtStream& sink)
{
   sink << util::pstr{} << _built ;
}

void GeneralInfo::mem (util::TxtStream& sink)
{
   size_t start = (size_t)__malloc_heap_start ;
   size_t end = (size_t)__brkval ;
   size_t sp = SP ;

   char res[] = "    -     SP=        " ;

   char *it = util::itoah (start, res) ;
   it = util::itoah (end, it + 1) ;
   util::itoah (sp, it + 4) ;

   sink << res  ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
