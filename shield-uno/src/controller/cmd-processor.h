////////////////////////////////////////////////////////////////////////////////////////////
//
// cmd-processor.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "util/strconv.h"
#include "util/txt-stream.h"
#include "declare-pgm-str.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

class CmdProcessor
{
public:
   template<bool (*_Handler) (const aux::StringRef& parms, util::TxtStream& client)>
      struct Static
      {
         static bool call (const CmdProcessor*, const aux::StringRef& parms, util::TxtStream& client)
         {
            return _Handler (parms, client) ;
         }
      } ;


   template<typename _Class, bool (_Class::*_MemFn) (const aux::StringRef&, util::TxtStream&) const>
      struct Member
      {
         static bool call (const CmdProcessor *this_, const aux::StringRef& parms, util::TxtStream& client)
         {
            return (this_->*_MemFn) (parms, client) ;
         }
      } ;

   class WrapHandler
   {
   public:
      template<bool (*_Handler) (const aux::StringRef& parms, util::TxtStream& client)>
         constexpr WrapHandler (Static<_Handler>) :
            m_proc{ Static<_Handler>::call } {}

      template<typename _Class, bool (_Class::*_MemFn) (const aux::StringRef&, util::TxtStream&) const>
         constexpr WrapHandler (Member<_Class,_MemFn>) :
            m_proc{ Member<_Class,_MemFn>::call } {}

      bool operator() (const CmdProcessor *this_, const aux::StringRef& parms, util::TxtStream& client) const
      {
         return m_proc (this_, parms, client) ;
      }

   private:
      typedef bool (*UnifiedProc) (const CmdProcessor *this_, const aux::StringRef& parms, util::TxtStream& client) ;
      const UnifiedProc m_proc ;
   } ;

   struct Command
   {
      const char* m_str ;
      WrapHandler m_handler ;

      static_assert
         (
            sizeof(m_str) == 2,
            "Appropriate pgm_read_... algorithms must be used"
         ) ;

   } ;

   typedef aux::Range<const Command*> Commands ;
   typedef util::TxtStream::Result Result ;

public:
   template<typename _Table>
      CmdProcessor (const _Table& cmds) : m_commands{ aux::range (cmds) } {}

   bool on_command (const aux::StringRef& cmd, util::TxtStream& client) const ;

private:
   bool _cmd_help (const aux::StringRef&, util::TxtStream& client) const ;
   bool _on_command_impl (const Commands& cmdtbl, const aux::StringRef& cmd, util::TxtStream& client, bool& result) const ;

private:
   const Commands m_commands ;

   static const Command s_cmn_cmds[] PROGMEM ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
