////////////////////////////////////////////////////////////////////////////////////////////
//
// system-master.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "system-master.h"
#include "master-commands.h"
#include "aux/array.h"
#include "aux/debug-assert.h"
#include "aux/ll-scheduler.h"
#include "aux/shared-ptr.h"

static constexpr aux::String::size_type RECEIVE_BUFFER_SIZE = 62 ;

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

MasterCommands SystemMaster::s_cmd_processor ;
SystemMaster::Pendings SystemMaster::m_pendings ;

void SystemMaster::on_receive (Request& current, char ch)
{
   switch (ch)
   {
      case '\n': break ;

      case '\r':
      {
         bool empty = m_pendings.empty() ;

         m_pendings.push_back (current.clone()) ;
         current.m_command.reset() ;

         if (empty)
            aux::LowLatencySched::schedule (_handle_pendings) ;

         break ;
      }

      case 0x08: // BS
         if (!current.m_command.empty())
            current.m_command.pop_back() ;
         break ;

      default:
         if (!current.m_command.capacity())
            current.m_command = aux::String{ RECEIVE_BUFFER_SIZE } ;

         current.m_command.push_back (ch) ;
   }
}

void SystemMaster::invalidate_requests (const void *cookie)
{
   for (auto& item : m_pendings)
      item->invalidate (cookie) ;
}

void SystemMaster::_handle_pendings (void)
{
   while (!m_pendings.empty())
   {
      auto top = m_pendings.pop_front() ;

      auto sink = top->sink() ;
      if (!sink || top->m_command.empty())
         continue ;

      bool need_close = !s_cmd_processor.on_command (aux::range (top->m_command), *sink) ;
      sink->commit() ;

      if (need_close)
         top->close() ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
