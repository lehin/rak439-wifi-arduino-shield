////////////////////////////////////////////////////////////////////////////////////////////
//
// serial-master.cpp
//
// Copyright © 2018 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "serial-master.h"
#include "util/uart-stream.h"
#include "hw/uart.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

struct SerialMaster::Request : public SystemMaster::Request
{
   virtual aux::shared_ptr<SystemMaster::Request> clone (void) const
   {
      return aux::shared_ptr<Request>::create (aux::ref (*this)) ;
   }

   virtual aux::shared_ptr<util::TxtStream> sink (void) const
   {
      return aux::shared_ptr<util::UartStream>::create() ;
   }

   virtual void close (void) const {}
} ;

////////////////////////////////////////////////////////////////////////////////////////////

void SerialMaster::initialize (void)
{
   hw::Uart::receive_handler (_on_receive) ;
}

void SerialMaster::_on_receive (void)
{
   static Request s_current ;

   while (!hw::Uart::buffer()->empty())
   {
      auto ch = hw::Uart::buffer()->pop_front() ;

      hw::Uart::send (ch) ;

      if (ch == '\r')
         hw::Uart::send ('\n') ;

      if (ch == 0x08)
         hw::Uart::send_multiple (' ', '\x08') ;

      SystemMaster::on_receive (s_current, ch) ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
