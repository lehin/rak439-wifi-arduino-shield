////////////////////////////////////////////////////////////////////////////////////////////
//
// system-master.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/string.h"
#include "aux/ring-buffer.h"
#include "aux/shared-ptr.h"
#include "util/txt-stream.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

class SystemMaster
{
public:
   struct Request
   {
      virtual ~Request (void) {}
      virtual aux::shared_ptr<Request> clone (void) const = 0 ;
      virtual aux::shared_ptr<util::TxtStream> sink (void) const = 0 ;
      virtual void close (void) const = 0 ;

      virtual void invalidate (const void *cookie) {}

      aux::String m_command ;
   } ;

public:
   static void on_receive (Request& current, char ch) ;
   static void invalidate_requests (const void *cookie) ;

private:
   static void _handle_pendings (void) ;

private:
   static class MasterCommands s_cmd_processor ;

   typedef aux::RingBuffer< 4, aux::shared_ptr<Request> > Pendings ;
   static Pendings m_pendings ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
