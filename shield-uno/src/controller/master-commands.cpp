////////////////////////////////////////////////////////////////////////////////////////////
//
// master-commands.cpp
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "master-commands.h"
#include "master-cmds-tags.h"
#include "system-master.h"
#include "general-info.h"
#include "util/eeprom.h"
#include "util/uart-dbg.h"
#include "lan/lan-service.h"
#include "hw/watchdog.h"
#include "aux/scheduler.h"
#include "aux/ll-scheduler.h"
#include <ctype.h>

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace tags
{
   DECLARE_PGM_STR (test, "test") ;
}

////////////////////////////////////////////////////////////////////////////////////////////

const CmdProcessor::Command MasterCommands::s_commands[] PROGMEM =
{
   { tags::version, Static<_cmd_version>{} },
   { tags::reboot, Static<_cmd_reboot>{} },
   { tags::mem, Static<_cmd_mem>{} },
   { tags::hostname, Static<_cmd_hostname>{} },
   { tags::uart_debug, Static<_cmd_uart_debug>{} },

#ifdef DEBUG_ASSERT
   { tags::debug_assert, Static<_debug_assert>{} },
#endif

   { tags::test, Static<_cmd_test>{} }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

MasterCommands::MasterCommands (void) :
   base_class (s_commands)
{
}

bool MasterCommands::_cmd_version (const aux::StringRef&, util::TxtStream& client)
{
   GeneralInfo::ver (client) ;
   client << util::endl{} ;
   GeneralInfo::built (client) ;
   client << util::endl{} << Result::Ok ;
   return true ;
}

bool MasterCommands::_cmd_reboot (const aux::StringRef& parms, util::TxtStream& client)
{
   _reboot_impl (client) ;
   return false ;
}

bool MasterCommands::_cmd_mem (const aux::StringRef&, util::TxtStream& client)
{
   GeneralInfo::mem (client) ;
   client << util::endl{} << Result::Ok ;

   return true ;
}

bool MasterCommands::_cmd_hostname (const aux::StringRef& parms, util::TxtStream& client)
{
   auto str = *util::Eeprom::host_name() ;

   if (parms.empty())
      client << str.get() << util::endl{} ;
   else
   {
      auto end = parms.begin() ;
      for (; end != parms.end() && !isspace (*end); ++end) ;

      str = aux::range (parms.begin(), end) ;
      util::Eeprom::host_name() = str ;
   }

   client << Result::Ok ;
   return true ;
}

bool MasterCommands::_cmd_uart_debug (const aux::StringRef& parms, util::TxtStream& client)
{
   static const char _off[] PROGMEM = "off" ;
   static const char _on[] PROGMEM = "on" ;
   static const char* const _off_on[] PROGMEM = { _off, _on } ;
   static constexpr uint8_t _off_on_cnt = sizeof (_off_on) / sizeof (*_off_on) ;

   if (parms.empty())
   {
      auto str = (const char*)pgm_read_ptr (_off_on + util::UartDbg::enabled()) ;
      client << util::pstr{} << str << util::endl{} ;
   }
   else
   {
      auto end = parms.begin() ;
      for (; end != parms.end() && !isspace (*end); ++end) ;

      aux::StringRef s{ parms.begin(), end } ;

      uint8_t res = _off_on_cnt ;

      for (uint8_t n = 0; n < _off_on_cnt; ++n)
         if (!strncmp_P (s.begin(), (const char*)pgm_read_ptr (_off_on + n), s.size()))
         {
            res = n ;
            break ;
         }

      if (res == _off_on_cnt)
      {
         client << Result::Error ;
         return true ;
      }

      util::UartDbg::enable (res != 0) ;
   }

   client << Result::Ok ;
   return true ;
}

#ifdef DEBUG_ASSERT
bool MasterCommands::_debug_assert (const aux::StringRef&, util::TxtStream& client)
{
   auto info = aux::read_debug_assert_info() ;

   char *end = info.m_ctx ;
   for (int8_t n = 0; n < sizeof (info.m_ctx) && *end && *end != -1; ++n, ++end) ;

   client << aux::range (info.m_ctx + 0, end) << util::endl{} << Result::Ok ;
   return true ;
}
#endif

bool MasterCommands::_cmd_test (const aux::StringRef& parms, util::TxtStream& client)
{
   client << Result::Ok ;
   return true ;
}

void MasterCommands::_reboot_impl (util::TxtStream& client)
{
   client << Result::Ok ;

   aux::LowLatencySched::schedule
      (
         []
         {
            lan::Service::shutdown() ;
            aux::Scheduler::schedule (hw::reboot, 100) ;
         }
      ) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
