////////////////////////////////////////////////////////////////////////////////////////////
//
// cmd-processor.cpp
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "cmd-processor.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace tags
{
   DECLARE_PGM_STR (help, "help") ;
}

////////////////////////////////////////////////////////////////////////////////////////////

const CmdProcessor::Command CmdProcessor::s_cmn_cmds[] PROGMEM =
{
   { tags::help, Member<CmdProcessor,&CmdProcessor::_cmd_help>{} }
} ;

bool CmdProcessor::on_command (const aux::StringRef& cmd, util::TxtStream& client) const
{
   bool result ;

   if
      (
         _on_command_impl (m_commands, cmd, client, result) ||
         _on_command_impl (aux::range (s_cmn_cmds), cmd, client, result)
      )
      return result ;

   client << Result::Unknown ;

   return true ;
}

bool CmdProcessor::_cmd_help (const aux::StringRef&, util::TxtStream& client) const
{
   client << util::divider{} ;

   for (auto& cmd : m_commands)
      client << util::pstr{} << (const char*)pgm_read_ptr (&cmd.m_str) << util::endl{} ;

   client << util::divider{} << Result::Ok ;

   return true ;
}

bool CmdProcessor::_on_command_impl (const Commands& cmdtbl, const aux::StringRef& cmd, util::TxtStream& client, bool& result) const
{
   auto parms = cmd.find (' ') ;
   uint8_t cmdlen = parms - cmd.begin() ;

   while (parms != cmd.end() && *parms == ' ')
      ++parms ;

   for (auto& c_pgm : cmdtbl)
   {
      auto s = (const char*)pgm_read_word (&c_pgm.m_str) ;

      if
         (
            strlen_P (s) == cmdlen &&
            !strncmp_P (cmd.begin(), s, cmdlen)
         )
      {
         union
         {
            uint16_t word ;
            WrapHandler handler ;
         } cvt = { pgm_read_word (&c_pgm.m_handler) } ;

         static_assert( sizeof (cvt.word) == sizeof (cvt.handler), "Wrong prg_... reading member pointer algorithm" ) ;

         result = cvt.handler (this, aux::range (parms, cmd.end()), client) ;
         return true ;
      }
   }

   return false ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
