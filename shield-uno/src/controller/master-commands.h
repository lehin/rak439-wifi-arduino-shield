////////////////////////////////////////////////////////////////////////////////////////////
//
// master-commands.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "cmd-processor.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

class MasterCommands : public CmdProcessor
{
   typedef CmdProcessor base_class ;

public:
   MasterCommands (void) ;

private:
   static bool _cmd_help (const aux::StringRef&, util::TxtStream& client) ;
   static bool _cmd_version (const aux::StringRef&, util::TxtStream& client) ;
   static bool _cmd_reboot (const aux::StringRef&, util::TxtStream& client) ;
   static bool _cmd_mem (const aux::StringRef&, util::TxtStream& client) ;
   static bool _cmd_hostname (const aux::StringRef&, util::TxtStream& client) ;
   static bool _cmd_uart_debug (const aux::StringRef&, util::TxtStream& client) ;

#ifdef DEBUG_ASSERT
   static bool _debug_assert (const aux::StringRef&, util::TxtStream& client) ;
#endif

   static bool _cmd_test (const aux::StringRef&, util::TxtStream& client) ;

   static void _reboot_impl (util::TxtStream& client) ;

private:
   static const Command s_commands[] PROGMEM ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
