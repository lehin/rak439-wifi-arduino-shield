#!/bin/sh

basedir=`dirname $0`
builddir=$basedir/build

mkdir -p $builddir
cd $builddir
cmake $basedir
make