# RAK439 WiFi Arduino shield

This is RAK439 WiFi Arduino shield project.

The shield is based onto RAK439 WiFi module. The projects goal is implementing lighweigth 
WiFi shield so users can use "standard" (Adafruit's) WiFi shield library.

At the moment everything that was used by Adafruit's WiFi shield library is implemented in
this Arduino RAK439 WiFi shield project. The one exclusion: there is no WEP encryption is 
implemented since the encryption is also not implemeneted in native RAK349 library.

The shield's hardware design has been done in GEDA programs (gscheme, pcb). The sources 
(with all necessary symbols) is included. There is also gerber files in the directories.

CMake project is used to build firmware for the shield. build.sh script is included that's
indended to run the build.

It's extremely desirable to use ATmega1284p as shield's MCU because of its RAM size. But 
it's also possible to solder ATmega644p MCU. The exact MCU type must be specified in 
CMakeLists.txt file before the firmware will be built. 

Two HEX files have already been included in the project: the shield's firmware for both 
ATmega644p and ATmega1284p MCUs.
